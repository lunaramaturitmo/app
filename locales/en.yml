exit: 'Tap back again to exit'
moment:
  date: 'M/D/YYYY'
  short_date: 'MM/DD'
cajon:
  notas: Notes
  calendario: Calendar
  tribe: Community
  tienda: Store
  biblioteca: Library
  glosario: Glossary
  ajustes: Settings
  acerca: About
  donaciones: Collaborate
  privacidad: Your privacy
  backup: Backup
  reporte: Report an error
espiral:
  se_me_fue_toast: Data saved. Your spiral will change at midnight.
  te_pasaste: "Hi! It's been over ten days from your estimated menstrual date. Touch the drop button when you initiate a new cycle."
  exceso: 'Your current cycle lasts longer than the variability we consider for your average. This exceeds the outreach of our estimations.'
  que_hacer: What are you up to?
  escribir_nota: Write a free note
  registrar_estados: Register my sensations
  modificar_estados: Update my sensations
  modificar_nota: Update my note
  fase: phase
  me_vino: my period arrived
  cancelar: Cancel
  se_me_fue: my period is over
modal:
  cerrar: close
calendario:
  titulo: Calendar
  meses:
    - JAN
    - FEB
    - MAR
    - APR
    - MAY
    - JUN
    - JUL
    - AUG
    - SEP
    - OCT
    - NOV
    - DEC
  dias:
    - S
    - M
    - T
    - W
    - T
    - F
    - S
  estados: sensations
  nota: note
acerca:
  - 'Each menstrual cycle gives us the chance to **develop ourselves** on multiple aspects, it is a **personal journey** to gain deeper understanding of who we are. Painful or irritating symptoms generally associated to the menstrual cycle derive from our denial or lack of attention to the messages our body is sending us. A culture in which "success" can only be achieved through permanent progress, frantic speeds and self-exploitation turns us away from our own individual **rhythm**, which is as **authentic, creative and wise** as Nature is.'
  - 'Judging by the approach taken by the mass media, Biology lessons and even running family jokes, menstruation is a "condition" to be concealed and numbed; and premenstrual syndrome or PMS is the peak-period of lady madness (or the perfect excuse for women to "derail"). However, to us and to an **ancient legacy of women** from all continents, menstrual cycle is a four-phase wheel, where our body, heart, mind and spirit are involved. Each of these _four phases_ has its own talents, weaknesses, timings and signs.'
  - 'In this app we gathered ancient and current knowledge so that you can **tune in with your own pace**. You can get a handy reminder of what phase are you in and what benefits entails, try out new ideas to make the most of its potential and, of course, enjoy it! ...Knowing that that "it will pass, too". You are invited to keep track of your findings on self-observation and **combine** the information we provide with your singularities. You will gain a _cyclic view of yourself_ which integrates this unique, waving rhythm your life is based upon.'
  - '## Tips'
  - 'In most women, the whole cycle varies from 21 to 35 days.'
  - 'Each woman has her own fluctuating cadence, and some variation is perfectly normal and healthy, since we are under the influence of multiple internal and external factors which have an impact on our chemical balance.'
  - 'Lunar encourages you to take advantage from the notes section, writing down whatever **vision, feeling, doubt or certainty** comes to your mind. Use the app as an ally for your personal observation process. The information provided for each phase is based on general trends; select what better resonates with you at the moment and look for your own patterns. We would like to remind you as well that phases are based on the increase and decrease of hormone levels: they are transitions, not abrupt overnight changes. Pay close attention to them as days go by, and factor in other influences (from your environment, activities and relationships) along with those of the menstrual cycle.'
  - 'It is not our goal to be a family planning or predictive tool. Keep up with your sexual health care through the course of medical care or health practices you have selected as an adult.
If you need additional information, ask your family physician, specialist or health professional for guidance, or share your questions with our online community. Our information **does not replace** medical advice, diagnosis and treatment.'
  - '## App navigation'
  - 'After entering some basic data in the Home section, the app wil estimate your current cycle, phases length and approximate date of your next menstruation. '
  - 'The "Day 1" allows you to state the exact date your new cycle begins. If your cycle has already begun but you forgot to input it, touch the drop button to indicate the day it began.'
  - 'The length of your menstrual cycle varies from woman to woman and from month to month, so you can mark the day it ended by touching that same button.'
  - 'How do we arrange the information in Lunar?'
  - 'We are complex living beings.  We chose four key aspects, in order to organize a little bit this sea of information.'
  - '## Earth aspects'
  - 'These are the **objective, concrete aspects of life**, as perceived by our senses. Do we sleep, eat, smell, feel physical pleasure, ache? These aspects involve everything that can be measured or tested, they reflect our unbiased and pragmatic take on stuff.'
  - '## Fire aspects'
  - 'Creative aspects of life, what we want to **crystallize, create, transform or reveal**. Fire burns, gives voice, it embodies what we yearn for. Amount and quality of energy. Beliefs arising from instinct or heart, faith, spirituality.'
  - '## Air aspects'
  - 'The social links and nets we create, the relationships we build; the topics we think of and the way we think of them; the way we communicate with each other. Our **mind and the social tissue**.'
  - '## Water aspects'
  - 'Emotional aspects of life, dynamic feelings that can be described as love, rejection, empathy, fear, connection, sympathy... How do you **feel** today? Those feelings beyond words.'
ajustes:
  ciclo_actual: 'Current cycle'
  proximo_ciclo: 'Next cycle'
  proximo_estimado: 'Estimated start date: '
  proximo_estimado_format: 'MM/DD'
  tu_usuaria: 'Your account'
  sin_registrarse: "You're not registered with Lunar!"
  registrarme: 'I want to sign up'
  hola: 'Hello, '
  backup: 'Your recovery code is:'
  reprogramando: Arranging notifications
  cambios_guardados: Changes saved
  frecuencias:
    diaria: Daily
    dia_por_medio: Every other day
    cada_3_dias: Every 3 days
    solo_en_cambio: When I move to a next phase
  dias: days
  ultima_menstruacion: Last period date
  me_vino: First day of bleeding
  cancelar: Cancel
  se_me_fue: Last day of bleeding
  se_me_fue_ayuda: You can edit your previously entered dates here, **not** the beggining of your new cycle. If a new period has started, use the drop-shaped button in the main screen.
  duracion_promedio: Average cycle length
  notificaciones: Receive notifications and tips
  empezar: Let's begin
  retirar_consentimiento: "I don't want to send my data"
  estas_segura: 'This will remove your account and all your data from our servers.  Do you want to continue?'
  registrarse: My LUNAR account
  yep: Yes, remove everything
backup:
  titulo: Your data
  texto:
    - 'You are logged in with: %{email}'
    - 'Latest backup:'
    - '%{ultimo}'
    - 'In order to retrieve your data for any re-installation, LUNAR needs to combine the email you informed when registering along with this recovery code:'
    - '_%{password_para_backup}_'
    - 'Please write down this code and keep it safe!'
  texto_sin_registro:
    - "You haven't registered with LUNAR APP."
    - "Thus, we do not have your consent to save your information."
    - "The data you are writing down currently is saved only in your local device."
  retirar_consentimiento: "I don't want to send my data"
  sin_backups: There're no backups yet
  manual: Backup now
  iniciando: Creating backup...
  exito: 'Backup ready!'
  error: There was an error creating the backup
biblioteca:
  fase: fase
configuracion_inicial:
  bienvenida: Welcome!
  saber_mas: Let us know your dates and preferences for your menstrual journey.
  primera_vez: Initial configuration
  backup: Recover from backup
donaciones:
  texto:
    - 'Lunar is an independent start up, without publicity or data-selling incomes.'
    - 'When you collaborate with a one-time donative, you are encouraging new developments and investigations. ;)'
  donar: Donate
  paypal: Donate with Paypal
glosario:
  title: Glossary
  texto:
    - 'Get to know some keywords and processes that define our health.'
    - '## Phase 1: menstrual'
    - "While you release endometrial tissue (menstruation), in your ovaries the follicles will start maturing. Since day 1, the hypothalamus (a structure in your brain that regulates hunger, thirst, libido) produces gonadotropin-releasing hormone (GnRH). GnRH will trigger your pituitary gland to release follicle stimulating hormone (FSH). This hormone's job travels through your bloodstream to your ovaries, where it will stimulate the growth and development of your eggs."
    - '## Phase 2: pre-ovulatory'
    - 'The maturing follicle sets off a surge in estrogen that thickens the lining of your uterus. Estrogen enhances insulin sensitivity and boosts serotonin, which have influence in our mood and sleep.'
    - '## Phase 3: ovulatory'
    - 'When estrogen levels peak, this informs the brain that the egg has matured and triggers the pituitary gland to release a surge of luteinizing hormone (LH). About 24-36 hours after the LH surge, a mature egg breaks out of the ovary and is released into the fallopian tube: this is ovulation. The egg leaves behind the corpus luteum (the empty follicle) which will produce progesterone.'
    - '## Phase 4: premenstrual'
    - 'Through this phase you experience important hormonal variations. The corpus luteum will continue to make progesterone during the this phase, around 11-13 days if the egg is not fertilized. Without fertilization, the corpus luteum will shrink away and be resorbed, leading to decreased levels of estrogen and progesterone. In the final days of this phase we are at the minimum hormone levels and the endometrial lining stops receiving hormonal support. The low levels of estrogen and progesterone will then signal the hypothalamus to start the whole menstrual cycle process over again.'
    - '**Estrogen:** sexual hormone produced in the ovary. Collaborates in the preparation of the endometrial lining.'
    - 'Increased and reaching its peak near the ovulation, it is also present in variable amounts in the pre-mesntrual phase.'
    - 'Due to its action, the uterus grows twice or three times its size. It builds bone, strengthens muscle, slows aging, raises libido, enhances insulin sensitivity, and boosts serotonin (which is why estrogen is so important for mood and sleep). Estrogen also triggers pubertal changes.'
    - '**Progesteron:** sexual hormone made by the corpus luteum. Prepares the uterus lining for implantation in case of pregnancy.'
    - 'Progesterone changes the cervical mucus, making it scarce and thick, preventing sperm to access the uterus. Its peak produces a temperature rise on a basal body temperature. Progesterone is also a calming, soothing hormone, that lightens periods and reduces anxiety.'
    - '**FSH:** FSH is secreted from cells in the anterior pituitary called gonadotrophs. This hormone stimulates the maturation of ovarian follicles. Each mont, many follicles develop but only one (exceptionally two) reach the maduration point correspondant to a mature egg, that will be ovulated.'
    - '**LH:** luteinizing hormone is produced in the pituitary gland. Its surge at midcycle triggers ovulation, and luteinizing hormone turns the follicle into the corpeus luteum. This structure produces progesterone, nourishing the uterus lining.'
    - '## Phases length'
    - '**Phase 1:** From the first day of bleeding, until the last one. We consider the menstrual phase to last up to 7 days, overlaping with the pre-ovulatory or follicular phase.'
    - '**Fase 2:** From the last day o final days of bleeding until two days prior to ovulation. It is the most variable phase of our cycle, and the length of the whole cycle depends of it.'
    - '**Fase 3:** Three days prior to ovulation, ovulation day and the following one.'
    - '**Fase 4:** From the enf od ovullatory phase to the following menstruation, it usually lasts 11 to 13 days.'
helper:
  cambios_guardados: 'Changes saved'
nota:
  guardada: 'Saved'
  placeholder: "Write here and press **save**"
  guardar: Save
  fase: phase
  luna: moon archetype
notas:
  agregar: Add note
  fase: phase
  dia: day
  lineas: "(%{lineas} more lines)"
notificaciones:
  te_olvidaste: 'Did you forget to input your period?'
  no_me_vino: "It's been over 10 days from your average cycle length. You can inform when your period arrives by pressing the drop button."
  actualizacion_disponible: "There's an app upgrade available, touch to upgrade Lunar"
  espera: "Please wait while upgrade is downloaded, don't close the app"
reporte:
  enviado: 'Thank you!'
  error: "There's been an error, please try again later."
  razones:
    sugerencias_no_llegan: 'Suggestions and tips are showing up in an unexpected frequency'
    gotita: 'The drop button does not work to indicate the beginning or end of my period'
    calendario: 'The calendar is not properly colored'
    olvidaste: 'I got the notification "Did you forget to input your period?" in the middle of my cycle'
    no_entiendo: "I don't understand the app"
  texto:
    - 'In order to test the errors, we need to understand the status of your app when it failed. The error report contains a description of the problem you are having and a restricted version of your data, without any of your notes.'
    - 'We process the dates only, not the content you created.'
    - 'Lunar does not ask for nor use any personal information in this error report.'
  contanos: "Tell us what's the issue you're having"
  cancelar: Cancel
  enviar: I agree, send
slide:
  luna: moon archetype
como_te_sentis:
  estados:
    - key: feliz
      name: 'Happy'
      color: '#00E3AC'
    - key: triste
      name: 'Sad'
      color: '#00B386'
    - key: enojada
      name: 'Angry'
      color: '#8500FF'
    - key: preocupada
      name: 'Worried'
      color: '#B566FF'
    - key: bien
      name: 'Alright'
      color: '#00D0FF'
    - key: cansada
      name: 'Tired'
      color: '#66E3FF'
    - key: hambrienta
      name: 'Hungry'
      color: '#FF00FF'
    - key: elevada
      name: 'Frisky'
      color: '#FF99FF'
  title: 'Register my sensations on '
  senales_fisicas: Physical signs
consentimiento:
  title: Do you want to register in Lunar Community?
  texto:
    - "We provide data backup, by saving a safety copy in our server."
    - "You will receive our new contents and updates"
    - "You will be able to collaborate with cervix cancer investigations, through At Your Cervix project with volunteer doctors."
    - "We take care of your intimacy and privacy with a responsible Privacy Policy"
    - "Join us!"
  aceptar: Yes, I want to receive updates
  rechazar: No, thanks
  privacidad: Privacy policy
dolor:
  title: Where is the pain located?
  pechos: Breasts
  panza: Belly
  pelvis: Pelvis
  cabeza: Head
  cuello: Neck
  lumbares: Lumbar
  cuanto: How much pain are you in?
  promedio: Your average
  continuar: Continue
flujo_vaginal:
  color: Color of your discharge
  textura: Texture of your discharge
  transparent: Transparent
  cantidad: How heavy was the discharge?
  promedio: Your average
  olor: Check if the smell was intense and unusual
  continuar: Continue
  texturas:
  - key: seco
    name: Dry / No discharge
  - key: pegajoso
    name: Sticky
  - key: cremoso
    name: Creamy
  - key: elastico
    name: 'Elastic / "Egg white"'
gracias:
  title: Done!
  descripcion: Your sensations were saved
  volver: Back to your spiral
perdidas:
  title: Spotting
  durante_o_despues: Did the spotting occur during/after sex?
  si: 'Yes'
  nope: 'No'
  duracion: How many hours after sex did the bleeding last?
  continuar: Continue
sangrado:
  title: How heavy was your bleeding?
  promedio: Your average
  continuar: Continue
  productos: Which products did you use today?
  productos_utilizados:
    - key: copa_menstrual
      name: Menstrual cup
    - key: toallita_de_tela
      name: Reusable pads
    - key: toallitas_descartables
      name: Disposable pads
    - key: tampones
      name: Tampons
    - key: ropa_interior_absorbente
      name: Period underwear
    - key: ninguno
      name: none
vacunacion:
  title: 'I was vaccinated against COVID-19'
  marca_vacuna: 'Vaccine Brand Name'
  dosis_a_registrar: 'Dose'
  pais_de_vacunacion: 'Country of application'
  nota_sobre_vacunacion: "HI! Make sure to load this data on the exact date of the vaccine's application. You can go to the previous screen to check if the title's date is accurate. If it isn't, touch Menu / Calendar and search for the proper date through the calendar."
  continuar: Continue
  incompleto: Complete the options to continue
  paises:
    - code: ''
      name: ''
    - code: AR
      name: Argentina
    - code: BR
      name: Brazil
    - code: CL
      name: Chile
    - code: CO
      name: Colombia
    - code: ES
      name: Spain
    - code: US
      name: United States of America
    - code: MX
      name: Mexico
    - code: UY
      name: Uruguay
    - code: 'none'
      name: '***************'
    - name: Albania
      code: AL
    - name: Algeria
      code: DZ
    - name: American Samoa
      code: AS
    - name: Andorra
      code: AD
    - name: Angola
      code: AO
    - name: Anguilla
      code: AI
    - name: Antarctica
      code: AQ
    - name: Antigua and Barbuda
      code: AG
    - name: Argentina
      code: AR
    - name: Armenia
      code: AM
    - name: Aruba
      code: AW
    - name: Australia
      code: AU
    - name: Austria
      code: AT
    - name: Azerbaijan
      code: AZ
    - name: 'Bahamas (the)'
      code: BS
    - name: Bahrain
      code: BH
    - name: Bangladesh
      code: BD
    - name: Barbados
      code: BB
    - name: Belarus
      code: BY
    - name: Belgium
      code: BE
    - name: Belize
      code: BZ
    - name: Benin
      code: BJ
    - name: Bermuda
      code: BM
    - name: Bhutan
      code: BT
    - name: Bolivia (Plurinational State of)
      code: BO
    - name: Bonaire, Sint Eustatius and Saba
      code: BQ
    - name: Bosnia and Herzegovina
      code: BA
    - name: Botswana
      code: BW
    - name: Bouvet Island
      code: BV
    - name: Brazil
      code: BR
    - name: British Indian Ocean Territory (the)
      code: IO
    - name: Brunei Darussalam
      code: BN
    - name: Bulgaria
      code: BG
    - name: Burkina Faso
      code: BF
    - name: Burundi
      code: BI
    - name: Cabo Verde
      code: CV
    - name: Cambodia
      code: KH
    - name: Cameroon
      code: CM
    - name: Canada
      code: CA
    - name: Cayman Islands (the)
      code: KY
    - name: Central African Republic (the)
      code: CF
    - name: Chad
      code: TD
    - name: Chile
      code: CL
    - name: China
      code: CN
    - name: Christmas Island
      code: CX
    - name: Cocos (Keeling) Islands (the)
      code: CC
    - name: Colombia
      code: CO
    - name: Comoros (the)
      code: KM
    - name: Congo (the Democratic Republic of the)
      code: CD
    - name: Congo (the)
      code: CG
    - name: Cook Islands (the)
      code: CK
    - name: Costa Rica
      code: CR
    - name: Croatia
      code: HR
    - name: Cuba
      code: CU
    - name: Curaçao
      code: CW
    - name: Cyprus
      code: CY
    - name: Czechia
      code: CZ
    - name: Côte d'Ivoire
      code: CI
    - name: Denmark
      code: DK
    - name: Djibouti
      code: DJ
    - name: Dominica
      code: DM
    - name: Dominican Republic (the)
      code: DO
    - name: Ecuador
      code: EC
    - name: Egypt
      code: EG
    - name: El Salvador
      code: SV
    - name: Equatorial Guinea
      code: GQ
    - name: Eritrea
      code: ER
    - name: Estonia
      code: EE
    - name: Eswatini
      code: SZ
    - name: Ethiopia
      code: ET
    - name: Falkland Islands (the) [Malvinas]
      code: FK
    - name: Faroe Islands (the)
      code: FO
    - name: Fiji
      code: FJ
    - name: Finland
      code: FI
    - name: France
      code: FR
    - name: French Guiana
      code: GF
    - name: French Polynesia
      code: PF
    - name: French Southern Territories (the)
      code: TF
    - name: Gabon
      code: GA
    - name: Gambia (the)
      code: GM
    - name: Georgia
      code: GE
    - name: Germany
      code: DE
    - name: Ghana
      code: GH
    - name: Gibraltar
      code: GI
    - name: Greece
      code: GR
    - name: Greenland
      code: GL
    - name: Grenada
      code: GD
    - name: Guadeloupe
      code: GP
    - name: Guam
      code: GU
    - name: Guatemala
      code: GT
    - name: Guernsey
      code: GG
    - name: Guinea
      code: GN
    - name: Guinea-Bissau
      code: GW
    - name: Guyana
      code: GY
    - name: Haiti
      code: HT
    - name: Heard Island and McDonald Islands
      code: HM
    - name: Holy See (the)
      code: VA
    - name: Honduras
      code: HN
    - name: Hong Kong
      code: HK
    - name: Hungary
      code: HU
    - name: Iceland
      code: IS
    - name: India
      code: IN
    - name: Indonesia
      code: ID
    - name: Iran (Islamic Republic of)
      code: IR
    - name: Iraq
      code: IQ
    - name: Ireland
      code: IE
    - name: Isle of Man
      code: IM
    - name: Israel
      code: IL
    - name: Italy
      code: IT
    - name: Jamaica
      code: JM
    - name: Japan
      code: JP
    - name: Jersey
      code: JE
    - name: Jordan
      code: JO
    - name: Kazakhstan
      code: KZ
    - name: Kenya
      code: KE
    - name: Kiribati
      code: KI
    - name: Korea (the Democratic People's Republic of)
      code: KP
    - name: Korea (the Republic of)
      code: KR
    - name: Kuwait
      code: KW
    - name: Kyrgyzstan
      code: KG
    - name: Lao People's Democratic Republic (the)
      code: LA
    - name: Latvia
      code: LV
    - name: Lebanon
      code: LB
    - name: Lesotho
      code: LS
    - name: Liberia
      code: LR
    - name: Libya
      code: LY
    - name: Liechtenstein
      code: LI
    - name: Lithuania
      code: LT
    - name: Luxembourg
      code: LU
    - name: Macao
      code: MO
    - name: Madagascar
      code: MG
    - name: Malawi
      code: MW
    - name: Malaysia
      code: MY
    - name: Maldives
      code: MV
    - name: Mali
      code: ML
    - name: Malta
      code: MT
    - name: Marshall Islands (the)
      code: MH
    - name: Martinique
      code: MQ
    - name: Mauritania
      code: MR
    - name: Mauritius
      code: MU
    - name: Mayotte
      code: YT
    - name: Mexico
      code: MX
    - name: Micronesia (Federated States of)
      code: FM
    - name: Moldova (the Republic of)
      code: MD
    - name: Monaco
      code: MC
    - name: Mongolia
      code: MN
    - name: Montenegro
      code: ME
    - name: Montserrat
      code: MS
    - name: Morocco
      code: MA
    - name: Mozambique
      code: MZ
    - name: Myanmar
      code: MM
    - name: Namibia
      code: NA
    - name: Nauru
      code: NR
    - name: Nepal
      code: NP
    - name: Netherlands (the)
      code: NL
    - name: New Caledonia
      code: NC
    - name: New Zealand
      code: NZ
    - name: Nicaragua
      code: NI
    - name: Niger (the)
      code: NE
    - name: Nigeria
      code: NG
    - name: Niue
      code: NU
    - name: Norfolk Island
      code: NF
    - name: Northern Mariana Islands (the)
      code: MP
    - name: Norway
      code: 'NO'
    - name: Oman
      code: OM
    - name: Pakistan
      code: PK
    - name: Palau
      code: PW
    - name: Palestine, State of
      code: PS
    - name: Panama
      code: PA
    - name: Papua New Guinea
      code: PG
    - name: Paraguay
      code: PY
    - name: Peru
      code: PE
    - name: Philippines (the)
      code: PH
    - name: Pitcairn
      code: PN
    - name: Poland
      code: PL
    - name: Portugal
      code: PT
    - name: Puerto Rico
      code: PR
    - name: Qatar
      code: QA
    - name: Republic of North Macedonia
      code: MK
    - name: Romania
      code: RO
    - name: Russian Federation (the)
      code: RU
    - name: Rwanda
      code: RW
    - name: Réunion
      code: RE
    - name: Saint Barthélemy
      code: BL
    - name: Saint Helena, Ascension and Tristan da Cunha
      code: SH
    - name: Saint Kitts and Nevis
      code: KN
    - name: Saint Lucia
      code: LC
    - name: Saint Martin (French part)
      code: MF
    - name: Saint Pierre and Miquelon
      code: PM
    - name: Saint Vincent and the Grenadines
      code: VC
    - name: Samoa
      code: WS
    - name: San Marino
      code: SM
    - name: Sao Tome and Principe
      code: ST
    - name: Saudi Arabia
      code: SA
    - name: Senegal
      code: SN
    - name: Serbia
      code: RS
    - name: Seychelles
      code: SC
    - name: Sierra Leone
      code: SL
    - name: Singapore
      code: SG
    - name: Sint Maarten (Dutch part)
      code: SX
    - name: Slovakia
      code: SK
    - name: Slovenia
      code: SI
    - name: Solomon Islands
      code: SB
    - name: Somalia
      code: SO
    - name: South Africa
      code: ZA
    - name: South Georgia and the South Sandwich Islands
      code: GS
    - name: South Sudan
      code: SS
    - name: Spain
      code: ES
    - name: Sri Lanka
      code: LK
    - name: Sudan (the)
      code: SD
    - name: Suriname
      code: SR
    - name: Svalbard and Jan Mayen
      code: SJ
    - name: Sweden
      code: SE
    - name: Switzerland
      code: CH
    - name: Syrian Arab Republic
      code: SY
    - name: Taiwan (Province of China)
      code: TW
    - name: Tajikistan
      code: TJ
    - name: Tanzania, United Republic of
      code: TZ
    - name: Thailand
      code: TH
    - name: Timor-Leste
      code: TL
    - name: Togo
      code: TG
    - name: Tokelau
      code: TK
    - name: Tonga
      code: TO
    - name: Trinidad and Tobago
      code: TT
    - name: Tunisia
      code: TN
    - name: Turkey
      code: TR
    - name: Turkmenistan
      code: TM
    - name: Turks and Caicos Islands (the)
      code: TC
    - name: Tuvalu
      code: TV
    - name: Uganda
      code: UG
    - name: Ukraine
      code: UA
    - name: United Arab Emirates (the)
      code: AE
    - name: United Kingdom of Great Britain and Northern Ireland (the)
      code: GB
    - name: United States Minor Outlying Islands (the)
      code: UM
    - name: United States of America (the)
      code: US
    - name: Uruguay
      code: UY
    - name: Uzbekistan
      code: UZ
    - name: Vanuatu
      code: VU
    - name: Venezuela (Bolivarian Republic of)
      code: VE
    - name: Viet Nam
      code: VN
    - name: Virgin Islands (British)
      code: VG
    - name: Virgin Islands (U.S.)
      code: VI
    - name: Wallis and Futuna
      code: WF
    - name: Western Sahara
      code: EH
    - name: Yemen
      code: YE
    - name: Zambia
      code: ZM
    - name: Zimbabwe
      code: ZW
  marcas_vacuna:
    - key: ''
      name: ''
    - key: convidecia
      name: Convidecia (CanSino Biologics)
    - key: coronaVac
      name: CoronaVac (Sinovac Biotech)
    - key: covaxin
      name: Covaxin (Bharat Biotech)
    - key: covishield
      name: Covishield (Oxford–AstraZeneca)
    - key: janssen
      name: Janssen (Johnson & Johnson)
    - key: moderna
      name: Moderna (Moderna)
    - key: comirnaty
      name: Comirnaty (Pfizer-BioNTech)
    - key: sinopharm
      name: Sinopharm (Sinopharm)
    - key: sputnik
      name: Sputnik V (Gamaleya)
  dosis:
    - key: ''
      name: ''
    - key: primera
      name: 'First dose'
    - key: segunda
      name: 'Second dose'
    - key: refuerzo
      name: 'Booster'
    - key: monodosis
      name: 'Single dose'
sintomas:
  sintomas:
    - key: sangrado
      name: 'Bleeding'
      help: 'Shedding of the uteran lining resulting in a loss of blood from the vaginal opening'
    - key: flujo_vaginal
      name: 'Vaginal discharge'
      help: 'Fluid that exits the vaginal opening'
    - key: perdidas
      name: 'Spotting'
      help: 'Any bleeding from the vagina that is not due to the monthly menstrual cycle'
    - key: dolor
      name: 'Pain'
    - key: 'vacunacion'
      name: 'I was vaccinated against COVID-19'
      help: "Application of an authorized vaccine against COVID-19. Make sure to load this data on the exact day of its application, through the app's Calendar. By registering your info here you enrich an anonymized statistical dataset that can better inform other menstruating people"
    - key: hinchazon
      name: 'Bloating'
      help: 'Abdominal swelling due to air or gas'
    - key: sangre_en_orina
      name: 'Blood in urine'
    - key: picazon
      name: 'Burning/itching in pelvic area'
      help: 'Uncomfortable burning sensation in pubic area'
    - key: fiebre
      name: 'Fever'
      help: 'Body temperature above 36C usually accompanied by shivering'
    - key: insomnio
      name: 'Insomnia'
      help: 'Inability to sleep'
    - key: nausea
      name: 'Nausea'
      help: 'Feeling of sickness with an inclination to vomit'
    - key: dolor_durante_el_sexo
      name: 'Pain during sex'
  title: 'Register my sensations on '
  help: Help
  continuar: Continue
recuperar_datos:
  introduccion: To retrieve your data, write down your recovery password and the email you registered with.
  email: "Email adress"
  password: "Recovery password"
  recuperar: Retrieve my data
  obtener: Downloading data...
  no_hay: "There're no backups! :("
  decifrando: Decrypting data...
  obtenidos: Data obtained
  daniados: 'Data is damaged! :('
  notificaciones: Recovering notifications
  ciclos: Recovering cycles
  nota: Recovering notes
  encuestas: Recovering sensations
  consentimiento: Recovering consent
  usuaria: Recovering user
  ajustes: Recovering settings
  listo: "Ready! You can go back to use the app :)"
  no_se_pudo_decifrar: "Couldn't decrypt"
  contrasenia_incorrecta: Wrong password
  no_se_obtuvieron: "Couldn't obtain data"
backend:
  sincronizacion: 'Synchronized!'
aspectos:
  tierra: earth
  aire: air
  fuego: fire
  agua: water
lunas:
  nueva: new
  creciente: crescent
  llena: full
  menguante: waning
