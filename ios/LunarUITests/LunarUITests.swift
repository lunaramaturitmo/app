//
//  LunarUITests.swift
//  LunarUITests
//
//  Created by Silvi on 16/5/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

import XCTest

class LunarUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()

        let app = XCUIApplication()
        setupSnapshot(app)
        app.launch()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
  
  func testScreenshots() {
    let app = XCUIApplication()
    XCUIDevice.shared().orientation = .portrait
    snapshot("0-Espiral")
  }
}
