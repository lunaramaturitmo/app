Creative Commons Legal Code
===========================

Atribución/Reconocimiento-NoComercial-SinDerivados 4.0 Internacional
--------------------------------------------------------------------
Las traducciones oficiales de esta licencia están disponibles [en otros
idiomas](#languages).

Creative Commons Corporation (\"Creative Commons\") no es una firma de
abogados ni ofrece servicios legales ni asesoría legal. La distribución
de las licencias públicas de Creative Commons no genera una relación
abogado-cliente ni de cualquier otro tipo. Creative Commons proporciona
sus licencias y la información relacionada tal como se presenta.
Creative Commons no ofrece ninguna garantía con respecto a sus
licencias, ni sobre el material sujeto a sus términos y condiciones, ni
sobre cualquier información relacionada. Creative Commons queda exenta
de cualquier responsabilidad vinculada a daños que resulten de su uso
conforme al marco legal aplicable.

**Uso de las Licencias Públicas de Creative Commons**

Las licencias públicas de Creative Commons proporcionan un conjunto
estándar de términos y condiciones que los creadores y otros titulares
de derechos pueden utilizar para compartir obras originales de su
autoría y cualquier otro material sujeto a derechos de autor y a otros
derechos que se especifican en la licencia pública de más abajo. Las
consideraciones siguientes son solo con fines informativos, no son
exhaustivas y no forman parte de nuestras licencias.

**Consideraciones para licenciantes:** Nuestras licencias públicas están
destinadas a ser utilizadas por aquellos autorizados a dar permiso al
público para utilizar material en formas que de otra manera estarían
restringidas por los derechos de autor y por otros derechos. Nuestras
licencias son irrevocables. Los licenciantes deben leer y entender los
términos y las condiciones de la licencia que elijan antes de aplicarla.
Los licenciantes deben también contar con todos los derechos necesarios
antes de aplicar nuestras licencias, de modo que el público pueda
reutilizar el material según lo esperado. Los licenciantes deben
identificar con claridad cualquier material que no esté sujeto a la
licencia. Esto incluye materiales que se encuentren distribuidos con
otra licencia de CC o materiales utilizados en virtud de alguna
excepción o limitación al derecho de autor. [Más consideraciones para
Licenciantes.](https://wiki.creativecommons.org/Considerations_for_licensors_and_licensees#Considerations_for_licensors)

**Consideraciones para el público:** Mediante el uso de una de nuestras
licencias públicas, el licenciante concede el permiso para utilizar el
Material Licenciado bajo los términos y las condiciones que se
especifican. Si producto de la aplicación de una excepción o limitación
al derecho de autor u otra razón, no es necesario aplicar la licencia
para el uso requerido, se entenderá que dicho uso no se encuentra
regulado por la licencia. Nuestras licencias solamente otorgan los
permisos que por derecho de autor y derechos conexos el licenciante
tiene potestad legal de otorgar. El uso del Material Licenciado podría
estar restringido por razones distintas de la licencia, incluyendo el
ejercicio de otros derechos de autor u otros derechos sobre el material.
El licenciante puede solicitar peticiones especiales, como pedir que los
cambios estén marcados o descritos. Aunque nuestras licencias no lo
requieren, se lo alienta a respetar esas solicitudes dentro de lo
razonable. [Más consideraciones para
licenciatarios.](https://wiki.creativecommons.org/Considerations_for_licensors_and_licensees#Considerations_for_licensees)
:::

### Creative Commons Atribución/Reconocimiento-NoComercial-SinDerivados 4.0 Licencia Pública Internacional --- CC BY-NC-ND 4.0

Al ejercer los Derechos Licenciados (definidos a continuación), Usted
acepta y acuerda estar obligado por los términos y condiciones de esta
Licencia Internacional Pública de
Atribución/Reconocimiento-NoComercial-SinDerivados 4.0 de Creative
Commons (\"Licencia Pública\"). En la medida en que esta Licencia
Pública pueda ser interpretada como un contrato, a Usted se le otorgan
los Derechos Licenciados en consideración a su aceptación de estos
términos y condiciones, y el Licenciante le concede a Usted tales
derechos en consideración a los beneficios que el Licenciante recibe por
poner a disposición el Material Licenciado bajo estos términos y
condiciones.

**Sección 1 -- Definiciones.**

a.  [**Material Adaptado** es aquel material protegido por Derechos de
    Autor y Derechos Similares que se deriva o se crea en base al
    Material Licenciado y en el cual el Material Licenciado se traduce,
    altera, arregla, transforma o modifica de manera tal que dicho
    resultado sea de aquellos que requieran autorización de acuerdo con
    los Derechos de Autor y Derechos Similares que ostenta el
    Licenciante. A los efectos de esta Licencia Pública, cuando el
    Material Licenciado se trate de una obra musical, una interpretación
    o una grabación sonora, la sincronización temporal de este material
    con una imagen en movimiento siempre producirá Material
    Adaptado.]{#s1a}
b.  [**Derechos de Autor y Derechos Similares** son todos aquellos
    derechos estrechamente vinculados a los derechos de autor,
    incluidos, de manera enunciativa y no taxativa, los derechos sobre
    las interpretaciones, las emisiones, las grabaciones sonoras y los
    Derechos \"Sui Generis\" sobre Bases de Datos, sin importar cómo
    estos derechos se encuentren enunciados o categorizados. A los
    efectos de esta Licencia Pública, los derechos especificados en las
    secciones [2(b)(1)-(2)](#s2b) no se consideran Derechos de Autor y
    Derechos Similares.]{#s1b}
c.  [**Medidas Tecnológicas Efectivas** son aquellas medidas que, en
    ausencia de la debida autorización, no pueden ser eludidas en virtud
    de las leyes que cumplen las obligaciones del artículo 11 del
    Tratado de la OMPI sobre Derecho de Autor adoptado el 20 de
    diciembre de 1996, y/o acuerdos internacionales similares.]{#s1c}
d.  [**Excepciones y Limitaciones** son el uso justo (fair use), el
    trato justo (fair dealing) y/o cualquier otra excepción o limitación
    a los Derechos de Autor y Derechos Similares que se apliquen al uso
    el Material Licenciado.]{#s1d}
e.  [**Material Licenciado** es obra artística o literaria, base de
    datos o cualquier otro material al cual el Licenciante aplicó esta
    Licencia Pública.]{#s1e}
f.  [**Derechos Licenciados** son derechos otorgados a Usted bajo los
    términos y condiciones de esta Licencia Pública, los cuales se
    limitan a todos los Derechos de Autor y Derechos Similares que
    apliquen al uso del Material Licenciado y que el Licenciante tiene
    potestad legal para licenciar.]{#s1f}
g.  [**Licenciante** es el individuo(s) o la entidad(es) que concede
    derechos bajo esta Licencia Pública.]{#s1g}
h.  [**NoComercial**, a los efectos de esta licencia, es que no se
    encuentre principalmente destinado o dirigido a obtener una ventaja
    comercial o una compensación monetaria. Para los propósitos de esta
    Licencia Pública, el intercambio de Material Licenciado por otro
    material sujeto a Derechos de Autor y Derechos Similares a través de
    mecanismos de intercambio de archivos digitales o de medios
    similares, se considerará como NoComercial siempre que no haya pago
    de una compensación monetaria en relación con el intercambio.]{#s1h}
i.  [**Compartir** significa proporcionar material al público por
    cualquier medio o procedimiento que requiera permiso conforme a los
    Derechos Licenciados, tales como la reproducción, exhibición
    pública, presentación pública, distribución, difusión, comunicación
    o importación, así como también su puesta a disposición, incluyendo
    formas en que el público pueda acceder al material desde un lugar y
    momento elegido individualmente por ellos.]{#s1i}
j.  [**Derechos \"Sui Generis\" sobre Bases de Datos** son aquellos
    derechos diferentes a los derechos de autor, resultantes de la
    Directiva 96/9/EC del Parlamento Europeo y del Consejo, de 11 de
    marzo de 1996 sobre la protección jurídica de las bases de datos, en
    sus versiones modificadas y/o posteriores, así como otros derechos
    esencialmente equivalentes en cualquier otra parte del mundo.]{#s1j}
k.  [**Usted** es el individuo o la entidad que ejerce los Derechos
    Licenciados en esta Licencia Pública. La palabra **Su** tiene un
    significado equivalente.]{#s1k}

**Sección 2 -- Ámbito de Aplicación.**

a.  [**Otorgamiento de la licencia**.]{#s2a}
    1.  [Sujeto a los términos y condiciones de esta Licencia Pública,
        el Licenciante le otorga a Usted una licencia de carácter
        global, gratuita, no transferible a terceros, no exclusiva e
        irrevocable para ejercer los Derechos Licenciados sobre el
        Material Licenciado para:]{#s2a1}
        A.  [reproducir y Compartir el Material Licenciado, en su
            totalidad o en parte, solamente para fines NoComerciales;
            y]{#s2a1A}
        B.  [producir y reproducir, pero no Compartir Material Adaptado,
            para fines NoComerciales.]{#s2a1B}
    2.  [[Excepciones y
        Limitaciones]{style="text-decoration: underline;"}. Para evitar
        cualquier duda, donde se apliquen Excepciones y Limitaciones al
        uso del Material Licenciado, esta Licencia Pública no será
        aplicable, y Usted no tendrá necesidad de cumplir con sus
        términos y condiciones.]{#s2a2}
    3.  [[Vigencia]{style="text-decoration: underline;"}. La vigencia de
        esta Licencia Pública está especificada en la sección
        [6(a)](#s6a).]{#s2a3}
    4.  [[Medios y formatos; modificaciones técnicas
        permitidas]{style="text-decoration: underline;"}. El Licenciante
        le autoriza a Usted a ejercer los Derechos Licenciados en todos
        los medios y formatos, actualmente conocidos o por crearse en el
        futuro, y a realizar las modificaciones técnicas necesarias para
        ello. El Licenciante renuncia y/o se compromete a no hacer valer
        cualquier derecho o potestad para prohibirle a Usted realizar
        las modificaciones técnicas necesarias para ejercer los Derechos
        Licenciados, incluyendo las modificaciones técnicas necesarias
        para eludir las Medidas Tecnológicas Efectivas. A los efectos de
        esta Licencia Pública, la mera realización de modificaciones
        autorizadas por esta sección [2(a)(4)](#s2a4) nunca produce
        Material Adaptado.]{#s2a4}
    5.  [[Receptores
        posteriores]{style="text-decoration: underline;"}.]{#s2a5}
        ::: {.para}
        A.  [[Oferta del Licenciante -- Material
            Licenciado]{style="text-decoration: underline;"}. Cada
            receptor de Material Licenciado recibe automáticamente una
            oferta del Licenciante para ejercer los Derechos Licenciados
            bajo los términos y condiciones de esta Licencia
            Pública.]{#s2a5A}
        B.  [[Sin restricciones a receptores
            posteriores]{style="text-decoration: underline;"}. Usted no
            puede ofrecer o imponer ningún término ni condición
            diferente o adicional, ni puede aplicar ninguna Medida
            Tecnológica Efectiva al Material Licenciado si haciéndolo
            restringe el ejercicio de los Derechos Licenciados a
            cualquier receptor del Material Licenciado.]{#s2a5B}
        :::

    6.  [[Sin endoso]{style="text-decoration: underline;"}. Nada de lo
        contenido en esta Licencia Pública constituye o puede
        interpretarse como un permiso para afirmar o implicar que Usted,
        o que Su uso del Material Licenciado, está conectado,
        patrocinado, respaldado o reconocido con estatus oficial por el
        Licenciante u otros designados para recibir la
        Atribución/Reconocimiento según lo dispuesto en la sección
        [3(a)(1)(A)(i)](#s3a1Ai).]{#s2a6}
b.  ::: {#s2b}
    **Otros derechos**.

    1.  [Los derechos morales, tales como el derecho a la integridad, no
        están comprendidos bajo esta Licencia Pública ni tampoco los
        derechos de publicidad y privacidad ni otros derechos personales
        similares. Sin embargo, en la medida de lo posible, el
        Licenciante renuncia y/o se compromete a no hacer valer ninguno
        de estos derechos que ostenta como Licenciante, limitándose a lo
        necesario para que Usted pueda ejercer los Derechos Licenciados,
        pero no de otra manera.]{#s2b1}
    2.  [Los derechos de patentes y marcas no son objeto de esta
        Licencia Pública.]{#s2b2}
    3.  [En la medida de lo posible, el Licenciante renuncia al derecho
        de cobrarle regalías a Usted por el ejercicio de los Derechos
        Licenciados, ya sea directamente o a través de una entidad de
        gestión colectiva bajo cualquier esquema de licenciamiento
        voluntario, renunciable o no renunciable. En todos los demás
        casos, el Licenciante se reserva expresamente cualquier derecho
        de cobrar esas regalías, incluidos aquellos casos en los que el
        Material Licenciado sea utilizado para fines distintos a los
        NoComerciales.]{#s2b3}
    :::

**Sección 3 -- Condiciones de la Licencia.**

Su ejercicio de los Derechos Licenciados está expresamente sujeto a las
condiciones siguientes.

a.  ::: {#s3a}
    **Atribución/Reconocimiento**.

    1.  ::: {#s3a1}
        Si Usted comparte el Material Licenciado, Usted debe:

        A.  [Conservar lo siguiente si es facilitado por el Licenciante
            con el Material Licenciado:]{#s3a1A}
            i.  [identificación del creador o los creadores del Material
                Licenciado y de cualquier otra persona designada para
                recibir Atribución/Reconocimiento, de cualquier manera
                razonable solicitada por el Licenciante (incluyendo por
                seudónimo si este ha sido designado);]{#s3a1Ai}
            ii. [un aviso sobre derecho de autor;]{#s3a1Aii}
            iii. [un aviso que se refiera a esta Licencia
                Pública;]{#s3a1Aiii}
            iv. [un aviso que se refiera a la limitación de
                garantías;]{#s3a1Aiv}
            v.  [un URI o un hipervínculo al Material Licenciado en la
                medida razonablemente posible;]{#s3a1Av}
        B.  [Indicar si Usted modificó el Material Licenciado y
            conservar una indicación de las modificaciones anteriores;
            e]{#s3a1B}
        C.  [Indicar que el Material Licenciado está bajo esta Licencia
            Pública, e incluir el texto, el URI o el hipervínculo a esta
            Licencia Pública.]{#s3a1C}

        Para evitar dudas, no tiene permiso bajo esta Licencia Pública
        para compartir Material Adaptado.
        :::

    2.  [Usted puede satisfacer las condiciones de la sección
        [3(a)(1)](#s3a1) de cualquier forma razonable según el medio,
        las maneras y el contexto en los cuales Usted Comparta el
        Material Licenciado. Por ejemplo, puede ser razonable satisfacer
        las condiciones facilitando un URI o un hipervínculo a un
        recurso que incluya la información requerida.]{#s3a2}
    3.  [Bajo requerimiento del Licenciante, Usted debe eliminar
        cualquier información requerida por la sección
        [3(a)(1)(A)](#s3a1A) en la medida razonablemente
        posible.]{#s3a3}
    :::

**Sección 4 -- Derechos \"Sui Generis\" sobre Bases de Datos.**

Cuando los Derechos Licenciados incluyan Derechos \"Sui Generis\" sobre
Bases de Datos que apliquen a Su uso del Material Licenciado:

a.  [para evitar cualquier duda, la sección [2(a)(1)](#s2a1) le concede
    a Usted el derecho a extraer, reutilizar, reproducir y Compartir
    todo o una parte sustancial de los contenidos de la base de datos
    solamente para fines NoComerciales, si no compartes Material
    Adaptado;]{#s4a}
b.  [si Usted incluye la totalidad o una parte sustancial del contenido
    de una base de datos en otra sobre la cual Usted ostenta Derecho
    \"Sui Generis\" sobre Bases de Datos, entonces ella (pero no sus
    contenidos individuales) se entenderá como Material Adaptado;
    y]{#s4b}
c.  [Usted debe cumplir con las condiciones de la sección [3(a)](#s3a)
    si Usted Comparte la totalidad o una parte sustancial de los
    contenidos de la base de datos.]{#s4c}

Para evitar dudas, esta sección [4](#s4) complementa y no sustituye Sus
obligaciones bajo esta Licencia Pública cuando los Derechos Licenciados
incluyen otros Derechos de Autor y Derechos Similares.

**Sección 5 -- Exención de Garantías y Limitación de Responsabilidad.**

a.  [**Salvo que el Licenciante se haya comprometido mediante un acuerdo
    por separado, en la medida de lo posible el Licenciante ofrece el
    Material Licenciado tal como es y tal como está disponible y no se
    hace responsable ni ofrece garantías de ningún tipo respecto al
    Material Licenciado, ya sea de manera expresa, implícita, legal u
    otra. Esto incluye, de manera no taxativa, las garantías de título,
    comerciabilidad, idoneidad para un propósito en particular, no
    infracción, ausencia de vicios ocultos u otros defectos, la
    exactitud, la presencia o la ausencia de errores, sean o no
    conocidos o detectables. Cuando no se permita, totalmente o en
    parte, la declaración de ausencia de garantías, a Usted puede no
    aplicársele esta exclusión.**]{#s5a}
b.  [**En la medida de lo posible, en ningún caso el Licenciante será
    responsable ante Usted por ninguna teoría legal (incluyendo, de
    manera no taxativa, la negligencia) o de otra manera por cualquier
    pérdida, coste, gasto o daño directo, especial, indirecto,
    incidental, consecuente, punitivo, ejemplar u otro que surja de esta
    Licencia Pública o del uso del Material Licenciado, incluso cuando
    el Licenciante haya sido advertido de la posibilidad de tales
    pérdidas, costes, gastos o daños. Cuando no se permita la limitación
    de responsabilidad, ya sea totalmente o en parte, a Usted puede no
    aplicársele esta limitación.**]{#s5b}

<!-- -->

c.  [La renuncia de garantías y la limitación de responsabilidad
    descritas anteriormente deberán ser interpretadas, en la medida de
    lo posible, como lo más próximo a una exención y renuncia absoluta a
    todo tipo de responsabilidad.]{#s5c}

**Sección 6 -- Vigencia y Terminación.**

a.  [Esta Licencia Pública tiene una vigencia de aplicación igual al
    plazo de protección de los Derechos de Autor y Derechos Similares
    licenciados aquí. Sin embargo, si Usted incumple las condiciones de
    esta Licencia Pública, los derechos que se le conceden mediante esta
    Licencia Pública terminan automáticamente.]{#s6a}
b.  ::: {#s6b}
    En aquellos casos en que Su derecho a utilizar el Material
    Licenciado se haya terminado conforme a la sección [6(a)](#s6a),
    éste será restablecido:

    1.  [automáticamente a partir de la fecha en que la violación sea
        subsanada, siempre y cuando esta se subsane dentro de los 30
        días siguientes a partir de Su descubrimiento de la violación;
        o]{#s6b1}
    2.  [tras el restablecimiento expreso por parte del
        Licenciante.]{#s6b2}

    Para evitar dudas, esta sección [6(b)](#s6b) no afecta ningún
    derecho que pueda tener el Licenciante a buscar resarcimiento por
    Sus violaciones de esta Licencia Pública.
    :::

c.  [Para evitar dudas, el Licenciante también puede ofrecer el Material
    Licenciado bajo términos o condiciones diferentes, o dejar de
    distribuir el Material Licenciado en cualquier momento; sin embargo,
    hacer esto no pondrá fin a esta Licencia Pública.]{#s6c}
d.  [Las secciones [1](#s1), [5](#s5), [6](#s6), [7](#s7), y [8](#s8)
    permanecerán vigentes a la terminación de esta Licencia
    Pública.]{#s6d}

**Sección 7 -- Otros Términos y Condiciones.**

a.  [El Licenciante no estará obligado por ningún término o condición
    adicional o diferente que Usted le comunique a menos que se acuerde
    expresamente.]{#s7a}
b.  [Cualquier arreglo, convenio o acuerdo en relación con el Material
    Licenciado que no se indique en este documento se considera separado
    e independiente de los términos y condiciones de esta Licencia
    Pública.]{#s7b}

**Sección 8 -- Interpretación.**

a.  [Para evitar dudas, esta Licencia Pública no es ni deberá
    interpretarse como una reducción, limitación, restricción, o una
    imposición de condiciones al uso de Material Licenciado que
    legalmente pueda realizarse sin permiso del titular, más allá de lo
    contemplado en esta Licencia Pública.]{#s8a}
b.  [En la medida de lo posible, si alguna disposición de esta Licencia
    Pública se considera inaplicable, esta será automáticamente
    modificada en la medida mínima necesaria para hacerla aplicable. Si
    la disposición no puede ser reformada, deberá ser eliminada de esta
    Licencia Pública sin afectar la exigibilidad de los términos y
    condiciones restantes.]{#s8b}
c.  [No se podrá renunciar a ningún término o condición de esta Licencia
    Pública, ni se consentirá ningún incumplimiento, a menos que se
    acuerde expresamente con el Licenciante.]{#s8c}
d.  [Nada en esta Licencia Pública constituye ni puede ser interpretado
    como una limitación o una renuncia a los privilegios e inmunidades
    que aplican al Licenciante o a Usted, incluyendo aquellos surgidos a
    partir de procesos legales de cualquier jurisdicción o
    autoridad.]{#s8d}

Creative Commons no es una parte en sus licencias públicas. No obstante,
Creative Commons puede optar por aplicar una de sus licencias públicas
al material que publica y en estos casos debe ser considerado como el
"Licenciante". El texto de las licencias públicas de Creative Commons
está dedicado al dominio público bajo una licencia [CC0 de Dedicación al
Dominio Público](https://creativecommons.org/publicdomain/zero/1.0/legalcode).
Excepto con el propósito limitado de indicar que el material se comparte
bajo una licencia pública de Creative Commons o según lo permitido por
las políticas de Creative Commons publicadas en
[creativecommons.org/policies](https://creativecommons.org/policies), Creative
Commons no autoriza el uso de la marca "Creative Commons" o cualquier
otra marca o logotipo de Creative Commons sin su consentimiento previo
por escrito, incluso, de manera enunciativa y no taxativa, en relación
con modificaciones no autorizadas de cualquiera de sus licencias
públicas o de cualquier otro acuerdo, arreglo o convenio relativos al
uso de Material Licenciado. Para evitar cualquier duda, este párrafo no
forma parte de las licencias públicas.\
\
Puede contactarse con Creative Commons en
[creativecommons.org](https://creativecommons.org/).

[Otros idiomas disponibles]{#languages}: [Bahasa
Indonesia](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.id),
[euskara](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.eu),
[English](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode),
[Deutsch](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.de),
[français](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.fr),
[hrvatski](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.hr),
[italiano](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.it),
[latviski](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.lv),
[Lietuvių](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.lt),
[Nederlands](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.nl),
[norsk](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.no),
[polski](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.pl),
[português](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.pt),
[suomeksi](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.fi),
[svenska](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.sv), [te
reo Māori](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.mi),
[Türkçe](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.tr),
[Ελληνικά](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.el),
[русский](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.ru),
[українська](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.uk),
[العربية](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.ar),
[日本語](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.ja). Por
favor lea las [Preguntas
Frecuentes](https://wiki.creativecommons.org/FAQ#officialtranslations) para
obtener más información acerca de las traducciones oficiales.
