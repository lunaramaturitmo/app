/**
 * @format
 */
import 'expo-asset'
import * as Sentry from '@sentry/react-native'
import { SENTRY, VERSION } from './lib/const'

Sentry.init({ dsn: SENTRY, release: VERSION, sampleRate: .1 })

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Icon from 'react-native-vector-icons/FontAwesome'
import BackgroundFetch from 'react-native-background-fetch'

import { AppRegistry, BackHandler, Platform } from 'react-native'
import { Scene, Router, Actions } from 'react-native-router-flux'
import Toast from 'react-native-root-toast'
import analytics from '@react-native-firebase/analytics'

import { Logger } from './lib/logger'

Logger.delete_if_too_big()

import { I18n } from './lib/i18n'

I18n.load()

import { STYLES } from './lib/styles'
import { Aspecto } from './lib/aspecto'
import { Acerca } from './lib/acerca'
import { Biblioteca } from './lib/biblioteca'
import { Glosario } from './lib/glosario'
import { Ajustes } from './lib/ajustes'
import { Espiral } from './lib/espiral'
import { Notas } from './lib/notas'
import { Nota } from './lib/nota'
import { Calendario } from './lib/calendario'
import { Reporte } from './lib/reporte'
import { ComoTeSentis } from './lib/ayc/como_te_sentis'
import { Sintomas } from './lib/ayc/sintomas'
import { Sangrado } from './lib/ayc/sangrado'
import { DondeEstaElDolor } from './lib/ayc/donde_esta_el_dolor'
import { FlujoVaginal } from './lib/ayc/flujo_vaginal'
import { Perdidas } from './lib/ayc/perdidas'
import { Vacunacion } from './lib/ayc/vacunacion'
import { PedirConsentimiento } from './lib/ayc/consentimiento'
import { Gracias } from './lib/ayc/gracias'
import { Backup } from './lib/backup'
import { RecuperarDatos } from './lib/recuperar_datos'
import { Registro } from './lib/registro'
import { Tienda } from './lib/tienda'
import { Backend } from './lib/backend'
import { Tribe } from './lib/tribe'

class Lunar extends Component {
  constructor (props) {
    super(props)

    this.tap = true
  }

  componentDidMount () {
    Backend.backup(false);
    Backend.sync();

    /*
     * Sincronizar en segundo plano
     */
    BackgroundFetch.configure(
      {
        periodic: true,
        stopOnTerminate: false,
        startOnBoot: true,
        enableHeadless: true,
        requiredNetworkType: BackgroundFetch.NETWORK_TYPE_ANY,
        requiresBatteryNotLow: true
      },
      async (taskId) => {
        analytics().logEvent('lunar_background_start', {});

        await Backend.backup(false);
        await Backend.sync();

        analytics().logEvent('lunar_background_end', {});

        BackgroundFetch.finish(taskId) // Obligatorio
      },
      async (taskId) => {
        analytics().logEvent('lunar_background_timeout', {});
        BackgroundFetch.finish(taskId);
      }
    )
  }

  /*
   * Vuelve para atrás a menos que estemos en la primera escena
   */
  backHandler () {
    if (Actions.state.index !== 0) {
      this.tap = true
      Actions.pop()

      return true
    }

    if (this.tap) {
      Toast.show(I18n.t('exit'))
      this.tap = false
    } else {
      BackHandler.exitApp()
    }

    return true
  }

  render() {
    return (
      <Router backAndroidHandler={this.backHandler.bind(this)}>
        <Scene key="root" backTitleEnabled={false} headerMode="none">
          <Scene key="espiral" component={Espiral} title="Lunar" hideNavBar={true} initial={true}/>
          <Scene key="acerca" component={Acerca} title="Acerca" />
          <Scene key="biblioteca" component={Biblioteca} title="Biblioteca" />
          <Scene key="glosario" component={Glosario} title="Glosario" />
          <Scene key="ajustes" component={Ajustes} title="Ajustes" />
          <Scene key="nota" component={Nota} title="Nota" />
          <Scene key="notas" component={Notas} title="Notas" />
          <Scene key="calendario" component={Calendario} title="Calendario" />
          <Scene key="reporte" component={Reporte} title="Reporte" />
          <Scene key="aspecto" component={Aspecto} title="Aspecto" />
          <Scene key="tienda" component={Tienda} title="Tienda" />
          <Scene key="tribe" component={Tribe} title="Tribe" />

          <Scene key="como_te_sentis"
            component={ComoTeSentis}
            title="AYC - ¿Cómo te sentís?"/>
          <Scene key="sintomas"
            component={Sintomas}
            title="AYC - Síntomas" />
          <Scene key="sangrado"
            component={Sangrado}
            title="AYC - Sangrado" />
          <Scene key="dolor"
            component={DondeEstaElDolor}
            title="AYC - DondeEstaElDolor" />
          <Scene key="flujo_vaginal"
            component={FlujoVaginal}
            title="AYC - Flujo Vaginal" />
          <Scene key="perdidas"
            component={Perdidas}
            title="AYC - Pérdidas" />
          {Platform.OS !== 'ios' && <Scene key="vacunacion"
            component={Vacunacion}
            title="AYC - Vacunación" />}
          <Scene key="pedir_consentimiento"
            component={PedirConsentimiento}
            title="AYC - Consentimiento" />
          <Scene key="gracias"
            component={Gracias}
            title="AYC - Gracias" />

          <Scene key="backup"
            component={Backup}
            title="AYC - Backup" />
          <Scene key="recuperar_datos"
            component={RecuperarDatos}
            title="AYC - Recuperar datos" />
          <Scene key="registro"
            component={Registro}
            title="AYC - Registro" />
        </Scene>
      </Router>
    )
  }
}

AppRegistry.registerComponent('Lunar', () => Lunar)
