/*
 * Devuelve la posición de un bug a partir de un stacktrace.  Útil para
 * los reportes de bloqueos de Google Play
 */
const [line, col] = process.argv[2].split(':').map(x => parseInt(x))

const sm = require('source-map'),
      fs = require('fs')

sm.SourceMapConsumer.with(fs.readFileSync('./android-sourcemap.json', 'utf8'), null, c => {
  console.log(c.originalPositionFor({
    line: line,
    column: col
  }))
})
