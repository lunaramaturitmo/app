import {PANEL} from './const';

import PropTypes from 'prop-types';
import React, {Component} from 'react';

import {
  Platform,
  SafeAreaView,
  TouchableOpacity,
  Text,
  BackHandler,
} from 'react-native';
import {WebView} from 'react-native-webview';
import {Actions} from 'react-native-router-flux';

import DeviceInfo from 'react-native-device-info';
import Toast from 'react-native-root-toast';

import realm from './models';
import {Helper} from './helper';
import {Logger} from './logger';
import {I18n} from './i18n';
import {Loader} from './loader'
import {STYLES} from './styles'

/*
 * Muestra una tienda a pantalla completa
 */
export class Tienda extends Component {
  constructor(props) {
    super(props);
    this.WEBVIEW_REF = React.createRef();

    this.state = {
      cargando: true,
    }
  }
  /*
   * XXX: Copiado de lib/registro.js
   */
  user_agent() {
    let ua = DeviceInfo.getUserAgentSync().replace(/; *wv/, '');

    if (Platform.OS === 'ios') ua = ua + ' Safari/604';

    return ua;
  }

  /*
   * Controlar loader
   */
  cambiarEstadoLoader() {
    this.setState({cargando: false})
  }

  /*
   * Mostrar un código al entrar
   */
  componentDidMount() {
    Toast.show(I18n.t('tienda.codigo'));
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  /*
   * Almacena datos de navegacion en el webview
   */

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton = () => {
    if (this.state.canGoBack) {
      this.WEBVIEW_REF.current.goBack()
    } else {
      Actions.pop()
    }

    return true;
  };

  handleWebViewNavigationStateChange(navState) {
    this.setState({ canGoBack: navState.canGoBack });
  }

  render() {
    return (
      <SafeAreaView style={[STYLES.android_safe_area, {flex: 1}]}>
        <TouchableOpacity
          style={{ padding: 10}}
          onPress={() => Actions.pop()}>
          <Text style={{fontSize: 18}}>◀ {I18n.t('gracias.volver')}</Text>
        </TouchableOpacity>

        <Loader cargando={this.state.cargando}/>

        <WebView
          onMessage={(event) => {}}
          onError={(error) => Logger.log('Tienda', error)}
          cacheEnabled={false}
          userAgent={this.user_agent()}
          useWebKit={true}
          allowsLinkPreview={false}
          javaScriptEnabled={true}
          source={{
            uri: 'https://www.copamenstrual.com.ar/?utm_source=LunarApp',
          }}
          ref={this.WEBVIEW_REF}
          onNavigationStateChange={n => this.handleWebViewNavigationStateChange(n)}
          onLoad={() => this.cambiarEstadoLoader()}
        />
      </SafeAreaView>
    );
  }
}
