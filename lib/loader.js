/* eslint-disable prettier/prettier */
import React, { Component } from 'react'

import {View, Modal, ActivityIndicator} from 'react-native'

export class Loader extends Component {
    constructor(props) {
        super(props)
        this.state = {
            cargando: this.props.cargando
        }
    }

    render() {
        return (
            <Modal transparent={true} visible={this.props.cargando}>
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#00000040' }}>
                    <ActivityIndicator color = '#999999' size="large" animating={this.props.cargando} style={{flex: 1, justifyContent: 'center', alignItems: 'center', height: 80}}/>
                </View>
             </Modal>
        )
    }
}