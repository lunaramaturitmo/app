import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { SafeAreaView, Text, ScrollView, View, TouchableOpacity, Linking, Platform, Image } from 'react-native'

import { Actions } from 'react-native-router-flux'

import Svg, { Line } from 'react-native-svg'
import Icon from 'react-native-vector-icons/FontAwesome'
import Drawer from 'react-native-drawer'

import realm from './models'
import { Backend } from './backend'
import { TEST } from './const'
import { STYLES } from './styles'
import { Helper } from './helper'
import { I18n } from './i18n'
import { Notificaciones } from './notificaciones'
import { Color } from './pseudo_html'

/*
 * Menú de la app, es el contenedor de todas las vistas.
 */
export class Cajon extends Component {
  static propTypes = {
    open: PropTypes.bool,
    back: PropTypes.bool
  }

  static defaultTypes = {
    open: false,
    back: true
  }

  constructor (props) {
    super(props)

    this.state = {
      open: this.props.open,
      back: this.props.back,
      recomendados: Helper.recomendados()
    }
  }

  componentDidMount () {
    realm.objects('Recomendado').addListener((recomendados, cambios) => {
      if (cambios.deletions.length === 0 &&
          cambios.insertions.length === 0 &&
          cambios.modifications.length === 0) return

      this.setState({ recomendados: Helper.recomendados() })
    })
  }

  UNSAFE_componentWillReceiveProps (next) {
    this.setState({ open: next.open, back: next.back })
  }

  /*
   * Cerrar el cajón
   */
  close () {
    this.setState({ open: false })
  }

  /*
   * Abrir el cajón
   */
  open () {
    this.setState({ open: true })
  }

  /*
   * El cajón no se autooculta cuando tocamos un elemento que cambia
   * la vista actual
   */
  ir_y_ocultar (a_donde) {
    this.close()

    if (a_donde.startsWith('http://')) {
      Linking.openURL(a_donde)
    } else if (a_donde.startsWith('https://')) {
      Linking.openURL(a_donde)
    } else {
      const ciclo = Helper.ciclo_actual()

      const props = {
        ciclo: ciclo,
        fase: Helper.fase_actual(ciclo)
      }

      if (a_donde === 'tribe' && Helper.pedir_consentimiento()) {
        props.volver_a = a_donde
        a_donde = 'pedir_consentimiento'
      }

      Actions[a_donde](props)
    }
  }

  render () {
    let cajonContents = (
      <View style={{ flex: 1, backgroundColor: 'lightgrey' }}>
      <ScrollView>
        {TEST ?
          <Icon.Button
            name="cloud-download"
            borderRadius={0}
            backgroundColor="lightgrey"
            color="black"
            onPress={async () => await Backend.sync()}>
            <Text style={STYLES.menu_item}>
              Sincronizar
            </Text>
          </Icon.Button> : null}

        <Icon.Button
          name="book"
          borderRadius={0}
          backgroundColor="lightgrey"
          color="black"
          onPress={() => this.ir_y_ocultar('notas')}
        >
          <Text style={STYLES.menu_item}>
            {I18n.t('cajon.notas')} ({realm.objects('Nota').length.toString()})
          </Text>
        </Icon.Button>

        <Icon.Button
          name="calendar"
          borderRadius={0}
          backgroundColor="lightgrey"
          color="black"
          onPress={() => this.ir_y_ocultar('calendario')}>
          <Text style={STYLES.menu_item}>{I18n.t('cajon.calendario')}</Text>
        </Icon.Button>

        {I18n.current_short_locale === 'es' || TEST
          ? <Icon.Button
            name="users"
            borderRadius={0}
            backgroundColor="lightgrey"
            color="black"
            onPress={() => this.ir_y_ocultar('https://tribe.applun.ar')}>
            <Text style={STYLES.menu_item}>
              {I18n.t('cajon.tribe')}
              <Color color="fuchsia">&nbsp;&#9679;</Color>
            </Text>
          </Icon.Button>
          : null}

        {I18n.current_short_locale === 'es'
          ? <Icon.Button
            name="shopping-basket"
            borderRadius={0}
            backgroundColor="lightgrey"
            color="black"
            onPress={() => this.ir_y_ocultar('tienda')}>
            <Text style={STYLES.menu_item}>
              {I18n.t('cajon.tienda')}
              <Color color="fuchsia">&nbsp;&#9679;</Color>
            </Text>
          </Icon.Button>
          : null}

        <Icon.Button
          name="book"
          borderRadius={0}
          backgroundColor="lightgrey"
          color="black"
          onPress={() => this.ir_y_ocultar('biblioteca')}>
          <Text style={STYLES.menu_item}>{I18n.t('cajon.biblioteca')}</Text>
        </Icon.Button>

        <Icon.Button
          name="question-circle-o"
          borderRadius={0}
          backgroundColor="lightgrey"
          color="black"
          onPress={() => this.ir_y_ocultar('glosario')}>
          <Text style={STYLES.menu_item}>{I18n.t('cajon.glosario')}</Text>
        </Icon.Button>

        <Icon.Button
          name="cog"
          borderRadius={0}
          backgroundColor="lightgrey"
          color="black"
          onPress={() => this.ir_y_ocultar('ajustes')}>
          <Text style={STYLES.menu_item}>{I18n.t('cajon.ajustes')}</Text>
        </Icon.Button>

        <Icon.Button
          name="exclamation-circle"
          borderRadius={0}
          backgroundColor="lightgrey"
          color="black"
          onPress={() => this.ir_y_ocultar('acerca')}>
          <Text style={STYLES.menu_item}>{I18n.t('cajon.acerca')}</Text>
        </Icon.Button>

        <Icon.Button
          name="user-secret"
          borderRadius={0}
          backgroundColor="lightgrey"
          color="black"
          onPress={() => this.ir_y_ocultar('http://www.lunarcomunidad.com/politica-de-privacidad-de-la-app')}>
          <Text style={STYLES.menu_item}>{I18n.t('cajon.privacidad')}</Text>
        </Icon.Button>

        <Icon.Button
          name="upload"
          borderRadius={0}
          backgroundColor="lightgrey"
          color="black"
          onPress={() => this.ir_y_ocultar('backup')}>
          <Text style={STYLES.menu_item}>{I18n.t('cajon.backup')}</Text>
        </Icon.Button>

        <Icon.Button
          name="bug"
          borderRadius={0}
          backgroundColor="lightgrey"
          color="black"
          onPress={() => this.ir_y_ocultar('reporte')}>
          <Text style={STYLES.menu_item}>{I18n.t('cajon.reporte')}</Text>
        </Icon.Button>

        <Icon.Button
          name="instagram"
          borderRadius={0}
          backgroundColor="lightgrey"
          color="black"
          onPress={() => this.ir_y_ocultar('https://instagram.com/applunar')}>
          <Text style={STYLES.menu_item}>Instagram</Text>
        </Icon.Button>

        {
          this.state.recomendados.map((recomendado, i) => {
            return (<TouchableOpacity style={{}} key={`recomendado-${i}`} onPress={() => this.ir_y_ocultar(recomendado.url)}>
              <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10}}>
                <Image
                  style={{ width: 16, height: 16, marginRight: 10 }}
                  resizeMode="contain"
                  source={{ uri: recomendado.favicon_url }}/>

                <Text style={STYLES.menu_item}>{recomendado.title}</Text>
              </View>
            </TouchableOpacity>)
          })
        }
      </ScrollView></View>
    )

    // SafeAreaView respeta los márgenes y dock de cada celular.
    return (
      <SafeAreaView style={[STYLES.android_safe_area,{ flex: 1, overflow: 'hidden' }]}>
        <Drawer type="overlay"
          open={this.state.open}
          openDrawerOffset={100}
          content={cajonContents} >

          {this.props.children}

          <View style={{ position: 'absolute', top: 0, left: 0, flexDirection: 'row' }}>
            <TouchableOpacity
              style={{ padding: 10 }}
              onPress={() => this.open()}>
              <Svg height="20" width="35">
                <Line x1="0" y1="2" x2="35" y2="2" stroke="#9b9b9b" strokeWidth="2" strokeLinecap="round" />
                <Line x1="0" y1="10" x2="35" y2="10" stroke="#9b9b9b" strokeWidth="2" strokeLinecap="round" />
                <Line x1="0" y1="18" x2="35" y2="18" stroke="#9b9b9b" strokeWidth="2" strokeLinecap="round" />
              </Svg>
            </TouchableOpacity>

            {Platform.OS === 'ios' && this.state.back
              ? <TouchableOpacity
                style={{ padding: 10, paddingTop: 5 }}
                onPress={() => Actions.pop()}>
                <Text style={{ fontSize: 18 }}>◀</Text>
              </TouchableOpacity>
              : null}
          </View>
        </Drawer>
      </SafeAreaView>
    )
  }
}
