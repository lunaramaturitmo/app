'use strict'

import React, { Component } from 'react'
import { View } from 'react-native'
import { Actions } from 'react-native-router-flux'
import { Calendar, LocaleConfig } from 'react-native-calendars'
import Icon from 'react-native-vector-icons/FontAwesome'

import m from 'moment'

import realm from './models'
import { STYLES, WIDTH } from './styles'
import { Helper } from './helper'
import { Cajon } from './cajon'
import { H1, Paragraph, Resaltado, BotonText } from './pseudo_html'
import { Logger } from './logger'
import { LunarModal } from './modal'
import { I18n } from './i18n'

const NOTA_COLOR = 'black'
const ESTADO_COLOR = '#999999'

/*
 * El calendario muestra una vista de mes marcando dónde hay notas y
 * estados con colores.
 */
export class Calendario extends Component {
  constructor () {
    super()

    Logger.log('Calendario.constructor', 'Entrando al calendario')

    this.state = {
      nota_o_encuesta: false,
      fases: realm.objects('Fase').sorted('id')
    }

    // Traer todos los eventos de este mes
    this.state.events = this.events()

    LocaleConfig.locales[I18n.current_short_locale] = {
      monthNames: I18n.t('calendario.meses'),
      dayNamesShort: I18n.t('calendario.dias')
    }

    LocaleConfig.defaultLocale = I18n.current_short_locale
  }

  /*
   * Mostrar el selector de nota o encuesta, indicando que se va a
   * modificar si existe alguna.
   */
  nota_o_encuesta (date) {
    const hoy = m().startOf('day').toDate().getTime()
    const cuando = m(date.dateString).startOf('day')

    // No permitir notas y encuestas en días posteriores
    if (cuando.toDate().getTime() > hoy) return

    // No permitir notas y encuestas fuera de un ciclo
    const ciclos = realm.objects('Ciclo')
      .filtered('me_vino <= $0 && fin_del_ciclo >= $0', cuando.toDate())

    if (ciclos.length === 0) return

    this.setState({ dia_presionado: cuando })
    this.toggle('nota_o_encuesta')
  }

  /*
   * Ir a una nota.  Las notas se pueden encontrar por ID o por ciclo y
   * fecha (?)
   *
   * TODO: Buscar notas solo por ID!
   */
  ir_a_nota () {
    const notas = realm.objects('Nota').filtered('created_at == $0', this.state.dia_presionado.toDate())
    const props = {}

    if (notas.length > 0) {
      props.id = notas[0].id
    } else {
      // props.ciclo = realm.objects('Ciclo').filtered('me_vino <= $0 && fin_del_ciclo >= $0', this.state.dia_presionado.toDate())[0]
      props.ciclo = Helper.ciclo_para_fecha(this.state.dia_presionado.toDate())
      props.date = this.state.dia_presionado.format('YYYY-MM-DD')
    }

    this.toggle('nota_o_encuesta')
    Actions.nota(props)
  }

  // XXX: Similar a Espiral.como_te_sentis() pero especifica la fecha
  ir_a_encuesta () {
    const desde = m(this.state.dia_presionado).startOf('day').toDate()
    const hasta = m(this.state.dia_presionado).endOf('day').toDate()
    const date = this.state.dia_presionado.toDate()

    this.toggle('nota_o_encuesta', false)

    const props = { ciclo: Helper.ciclo_para_fecha(date), fecha: date }
    props.fase = this.state.fases[props.ciclo.calcular_fase_segun_fecha(date) - 1]

    const encuestas = realm.objects('Encuesta')
      .filtered('created_at >= $0 && created_at <= $1', desde, hasta)

    Logger.log('Encuestas de hoy', encuestas.length)

    // Pasar la encuesta como un objeto para poder modificarla
    if (encuestas.length > 0) {
      const e = encuestas[encuestas.length - 1]

      // XXX: Convertir sintomas y dolores a arrays
      props.encuesta = {
        id: e.id,
        como_te_sentis: e.como_te_sentis.slice(),
        productos_utilizados: e.productos_utilizados.slice(),
        dolores: e.dolores.map(d => d.parte_del_cuerpo),
        sintomas: e.sintomas.map(s => s.nombre),
        nivel_de_sangrado: e.nivel_de_sangrado,
        sangrado_durante_o_despues_del_sexo: e.sangrado_durante_o_despues_del_sexo,
        duracion_del_sangrado: e.duracion_del_sangrado,
        intensidad_del_dolor: e.intensidad_del_dolor,
        color_del_flujo_vaginal: e.color_del_flujo_vaginal,
        cantidad_del_flujo_vaginal: e.cantidad_del_flujo_vaginal,
        olor_del_flujo_vaginal: e.olor_del_flujo_vaginal,
        textura_del_flujo: e.textura_del_flujo,
        marca_vacuna: e.marca_vacuna,
        pais: e.pais,
        dosis: e.dosis,
      }
    } else {
      props.encuesta = { created_at: date }
    }

    Logger.log('Espiral', props.encuesta)

    // Si la usuaria no está registrada y no dio consentimiento aún,
    // lo pedimos, sino no volvemos a molestar
    if (Helper.pedir_consentimiento()) {
      Actions.pedir_consentimiento(props)
    } else {
      Actions.como_te_sentis(props)
    }
  }

  /*
   * Intercambiar el valor de una vista
   */
  toggle (cosa) {
    const s = {}
    s[cosa] = !this.state[cosa]

    this.setState(s)
  }

  /*
   * Cambia los eventos según el mes
   */
  set_events (month) {
    this.setState({ events: this.events(month.dateString) })
  }

  /*
   * Trae todos los eventos en el formato del calendario
   */
  events (fecha = null) {
    const mes_base = m(fecha || new Date())
    const f = 'YYYY-MM-DD'
    const events = {}

    // XXX: trabajamos con una copia porque moment modifica el original
    const mes = mes_base.clone().startOf('month')
    const fin_de_mes = mes_base.clone().endOf('month')

    // Les restamos la mayor duración promedio del ciclo para poder
    // agarrar ciclos enteros
    const mes_anterior = mes.clone().subtract(Helper.duracion_maxima(), 'days')
    const mes_siguiente = fin_de_mes.clone().add(Helper.duracion_maxima(), 'days')

    // Todos los ciclos en el rango
    const ciclos = realm.objects('Ciclo').filtered('me_vino >= $0 && fin_del_ciclo <= $1', mes_anterior.toDate(), mes_siguiente.toDate())

    Logger.log('Calendario.events', {
      mes_base: mes_base.toDate(),
      mes: mes.toDate(),
      fin_de_mes: fin_de_mes.toDate(),
      mes_anterior: mes_anterior.toDate(),
      mes_siguiente: mes_siguiente.toDate(),
      ciclos_del_mes: ciclos.map(c => ({ p: c.me_vino, f: c.fin_del_ciclo, m: c.se_me_fue, d: c.duracion }))
    })

    // Recorrer todos los ciclos encontrados
    ciclos.forEach(ciclo => {
      for (let i = 1; i <= ciclo.duracion; i++) {
        // Obtener la fecha del día
        const date = ciclo.date_para_dia(i)
        const fase = this.state.fases[ciclo.calcular_fase_segun_fecha(date) - 1]

        events[m(date).format(f)] = {
          periods: [{
            startingDay: true,
            endingDay: true,
            color: fase.color
          }]
        }
      }

      // Los dias con notas tienen un tono distinto
      ciclo.notas.forEach(nota => {
        const created_at = m(nota.created_at).format(f)

        // Definir si no existen
        if (events[created_at] === undefined) { events[created_at] = {} }

        if (events[created_at].periods === undefined) { events[created_at].periods = [] }

        events[created_at].periods.push({
          startingDay: true,
          endingDay: true,
          color: NOTA_COLOR
        })
      })

      ciclo.encuestas.forEach(encuesta => {
        const created_at = m(encuesta.created_at).format(f)

        // Definir si no existen
        if (events[created_at] === undefined) { events[created_at] = {} }

        if (events[created_at].periods === undefined) { events[created_at].periods = [] }

        events[created_at].periods.push({
          startingDay: true,
          endingDay: true,
          color: ESTADO_COLOR
        })
      })
    })

    Logger.log('Calendario.events', events)
    return events
  }

  /*
   * Detecta si un día tiene nota, buscándolo en los eventos
   */
  tiene_nota () {
    if (!this.state.dia_presionado) return false

    const fecha = this.state.dia_presionado.format('YYYY-MM-DD')
    const _event = this.state.events[fecha]

    return (_event && _event.periods.find(x => x.color === NOTA_COLOR))
  }

  /*
   * Detecta si hay encuesta
   *
   * XXX: Copia de tiene_nota()
   */
  tiene_encuesta () {
    if (!this.state.dia_presionado) return false

    const fecha = this.state.dia_presionado.format('YYYY-MM-DD')
    const _event = this.state.events[fecha]

    return (_event && _event.periods.find(x => x.color === ESTADO_COLOR))
  }

  render () {
    const fase = realm.objectForPrimaryKey('Fase', 1)

    return (
      <Cajon back={true}>
        <View style={STYLES.container}>

          <LunarModal
            show={this.state.nota_o_encuesta}
            fase={fase}
            onPress={() => this.toggle('nota_o_encuesta', false)}>

            <View style={{ marginBottom: 30 }}>
              <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                <Paragraph center>{I18n.t('espiral.que_hacer')}</Paragraph>
              </View>
              <View style={{ margin: 5 }}>
                <Icon.Button
                  name="book"
                  style={{ backgroundColor: ESTADO_COLOR }}
                  onPress={() => this.ir_a_nota()}>
                  <BotonText>
                    {this.tiene_nota() ? I18n.t('espiral.modificar_nota') : I18n.t('espiral.escribir_nota')}
                  </BotonText>
                </Icon.Button>
              </View>

              <View style={{ margin: 5 }}>
                <Icon.Button
                  name="smile-o"
                  style={{ backgroundColor: ESTADO_COLOR }}
                  onPress={() => this.ir_a_encuesta()}>
                  <BotonText>
                    {this.tiene_encuesta() ? I18n.t('espiral.modificar_estados') : I18n.t('espiral.registrar_estados')}
                  </BotonText>
                </Icon.Button>
              </View>
            </View>
          </LunarModal>

          <H1>{I18n.t('calendario.titulo')}</H1>

          <Calendar
            ref="calendar"
            onDayPress={date => this.nota_o_encuesta(date)}
            markingType={'multi-period'}
            markedDates={this.state.events}
            onMonthChange={month => this.set_events(month)}
            style={{ width: WIDTH - 20 }}
            theme={{ calendarBackground: 'transparent' }} />

          <View style={{ flexGrow: 1, flexDirection: 'row', alignItems: 'flex-end' }}>
            <View style={{ flexGrow: 1, flexDirection: 'column', alignItems: 'center' }}>
              <Paragraph><Resaltado color={NOTA_COLOR} foreground>&nbsp;{I18n.t('calendario.nota')}&nbsp;</Resaltado></Paragraph>
              <Paragraph><Resaltado color={ESTADO_COLOR}>&nbsp;{I18n.t('calendario.estados')}&nbsp;</Resaltado></Paragraph>
            </View>

            {[-2, 0].map(slice => {
              return (
                <View key={`slice_${slice}`} style={{ flexGrow: 1, flexDirection: 'column', alignItems: 'center' }}>
                  {this.state.fases.slice(slice + 2, slice + 4).map(fase => {
                    return (<Paragraph key={fase.etapa}>
                      <Resaltado color={fase.color}>
                      &nbsp;{fase.etapa}&nbsp;
                      </Resaltado>
                    </Paragraph>)
                  })}
                </View>)
            })}
          </View>
        </View>
      </Cajon>
    )
  }
}
