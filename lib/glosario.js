import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { ScrollView, View, Image } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import { STYLES, WIDTH, HEIGHT } from './styles'
import { H1, Paragraph } from './pseudo_html'
import { Cajon } from './cajon'
import { I18n } from './i18n'
import { PseudoMarkdown } from './pseudo_markdown'

/*
 * El glosario de la aplicación, muestra un gráfico zoomeable y un texto
 * explicativo.
 */
export class Glosario extends Component {
  static propTypes = { fase: PropTypes.object.isRequired };

  render () {
    const width = WIDTH * 0.9
    const height = HEIGHT * 0.9 * 0.59

    return (
      <Cajon back={true}>
        <ScrollView contentContainerStyle={STYLES.non_view_container}>
          <View style={STYLES.aspecto_body}>
            <View style={{ marginBottom: 10 }}>
              <H1>{I18n.t('glosario.title')}</H1>
            </View>

            <Image
              enableHorizontalBounce={true}
              style={{ width: width, height: width * 0.59 }}
              source={{uri: 'https://panel.lunarcomunidad.com/ios/grafico_glosario.png'}}/>

            {I18n.t('glosario.texto').map((parrafo, i) => {
              const render = PseudoMarkdown.render(this.props.fase, parrafo)
              return (<Paragraph key={`parrafo_${i}`}>{render}</Paragraph>)
            })}

          </View>
        </ScrollView>
      </Cajon>
    )
  }
}
