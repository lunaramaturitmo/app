import React, { Component } from 'react'
import { Bold, Resaltado, H1, H2 } from './pseudo_html'

/*
 * Procesa estilos del texto y devuelve elementos
 */
export class PseudoMarkdown {
  static randomId () {
    return parseInt(Math.random() * 100000)
  }

  static resaltado (part, fase) {
    if (typeof part !== 'string') return part

    if (part.startsWith('_')) {
      return (<Resaltado
          key={`resaltado-${this.randomId()}`}
          foreground={fase.id > 3}
          color={fase.color}>
          {part.replace(/^_/, '').replace(/_$/, '')}
        </Resaltado>)
    }

    return part
  }

  static enfasis (part) {
    if (typeof part !== 'string') return part

    if (part.startsWith('**'))
      return (<Bold key={`bold-${this.randomId()}`}>{part.replace(/^\*\*/, '').replace(/\*\*$/, '')}</Bold>)

    return part
  }

  static h1 (part) {
    if (typeof part !== 'string') return part

    if (part.startsWith('# '))
      return (<H1 key={`h1-${this.randomId()}`}>{part.replace(/^# /, '')}</H1>)

    return part
  }

  static h2 (part) {
    if (typeof part !== 'string') return part

    if (part.startsWith('## '))
      return (<H2 key={`h2-${this.randomId()}`}>{part.replace(/^## /, '')}</H2>)

    return part
  }

  static render (fase, text) {
    return text
      .split(/(_[^_]+_)/g)
      .map(part => this.resaltado(part, fase))
      .map(part => {
        if (typeof part !== 'string') return part

        return part.split(/(\*\*[^\*]+\*\*)/g).map(part => this.enfasis(part))
      }).flat()
      .map(part => this.h1(part))
      .map(part => this.h2(part))
  }
}
