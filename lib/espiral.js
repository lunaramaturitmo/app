'use strict';

import {SINCRONIZACION, VERSION, PANEL} from './const';

import * as Sentry from '@sentry/react-native';

import m from 'moment';

import React, {Component} from 'react';
import {View, Image, TouchableOpacity, Platform} from 'react-native';
import Svg, {
  G,
  Line,
  Circle,
  Text,
  Polygon,
  Path,
  Rect,
} from 'react-native-svg';
import DatePicker from 'react-native-datepicker';
import {Actions} from 'react-native-router-flux';
import Toast from 'react-native-root-toast';
import Icon from 'react-native-vector-icons/FontAwesome';
import DeviceInfo from 'react-native-device-info';
import analytics from '@react-native-firebase/analytics';

import EStyleSheet from 'react-native-extended-stylesheet';
import {STYLES, WIDTH, HEIGHT} from './styles';

import realm from './models';
import {Logger} from './logger';
import {Notificaciones} from './notificaciones';
import {LunarModal} from './modal';
import {FaseH1, FaseH2, FaseH3, Paragraph, BotonText} from './pseudo_html';
import {Aspecto} from './aspecto';
import {Nota} from './nota';
import {Notas} from './notas';
import {Cajon} from './cajon';
import {ConfiguracionInicial} from './configuracion_inicial';
import {MeVino, SeMeFue, BarraDeAspectos} from './svg_elements';
import {Helper} from './helper';
import {Backend} from './backend';
import {I18n} from './i18n';
import {AdhocStorage} from './adhoc_storage';

EStyleSheet.build({$rem: WIDTH > 340 ? 18 : 16});

/*
 * La Espiral es la pantalla principal de la app
 */
export class Espiral extends Component {
  constructor(props) {
    super(props);

    analytics().logAppOpen();

    Logger.log(
      'Espiral',
      `Iniciando Lunar ${VERSION} ${Platform.OS} ${Platform.Version} ${WIDTH}x${HEIGHT} ${I18n.current_locale} (${I18n.current_short_locale})`,
    );

    // Detectar si cambiamos el idioma
    try {
      const lunas = realm
        .objects('Fase')
        .map((f) => f.luna)
        .sort();
      const lunas_i18n = Object.values(I18n.t('lunas')).sort();
      const lunas_en_otro_idioma = !lunas.every((v, i) => v === lunas_i18n[i]);

      // Creamos todas las notificaciones si no hay ninguna o estamos
      // cambiando de idioma
      if (realm.objects('Notificacion').length === 0 || lunas_en_otro_idioma) {
        Logger.log(
          'Notificaciones.crear',
          'Creando notificaciones en la base de datos',
        );

        switch (I18n.current_short_locale) {
          case 'pt':
            Notificaciones.crear(require('../assets/notificaciones.pt.json'));
            break;
          case 'es':
            Notificaciones.crear(require('../assets/notificaciones.es.json'));
            break;
          default:
            Notificaciones.crear(require('../assets/notificaciones.en.json'));
        }
      }

      // Crear todas las fases si no hay ninguna o estamos en otro idioma
      if (lunas.length === 0 || lunas_en_otro_idioma) {
        Logger.log('Espiral.Fases', 'Cambiando el idioma');

        switch (I18n.current_short_locale) {
          case 'pt':
            Backend.actualizar_fases(require('../assets/fases.pt.json'));
            break;
          case 'es':
            Backend.actualizar_fases(require('../assets/fases.es.json'));
            break;
          default:
            Backend.actualizar_fases(require('../assets/fases.en.json'));
        }
      }
    } catch (e) {
      Sentry.captureException(e);
    }

    Logger.log('Fases', realm.objects('Fase').length);
    Logger.log('Aspectos', realm.objects('Aspecto').length);
    Logger.log('Slides', realm.objects('Slide').length);

    // Cargar el estado inicial
    this.state = this.obtener_estado();

    // La notificación se configura una sola vez para que no sea
    // cambiada accidentalmente por el reset_state().
    this.state.modal_notificacion = false;
    this.state.notificacion = '';

    Logger.log('Espiral.state', this.state);

    // Registramos el toque de las notificaciones para poder recibir el
    // evento aunque la app esté cerrada
    Notificaciones.configurar((n, component) => {
      component.setState({
        modal_notificacion: true,
        notificacion:
          n.userInfo && n.userInfo.contenido ? n.userInfo.contenido : n.message,
      });

      analytics().logEvent('lunar_notification_open', {id: n.id});

      Notificaciones.cancelar(n.id);
    }, this);
  }

  /*
   * Acciones a realizar cuando la vista aparece en pantalla
   */
  componentDidMount() {
    /*
     * Cuando cambian los ciclos, recargar el estado para redibujar la
     * espiral.
     *
     * XXX: Revisar porque esto esta sucediendo varias veces al mismo
     * tiempo
     */
    realm.objects('Ciclo').addListener((ciclos, cambios) => {
      // Si no hubo ningún cambio, salir
      if (
        cambios.deletions.length === 0 &&
        cambios.insertions.length === 0 &&
        cambios.modifications.length === 0
      )
        return;

      Logger.log(
        'Espiral.componentDidMount',
        'Hubo un cambio en Ciclos, reseteando el estado',
      );

      this.reset_state();
    });

    realm.objects('Ajustes').addListener((ciclos, cambios) => {
      // Si no hubo ningún cambio, salir
      if (
        cambios.deletions.length === 0 &&
        cambios.insertions.length === 0 &&
        cambios.modifications.length === 0
      )
        return;

      Logger.log('Espiral.componentDidMount', 'Hubo un cambio en Ajustes');

      this.setState({ajustes: Helper.ajustes()});
    });

    // Realizar acciones en cambios de versión específicos
    try {
      switch (VERSION) {
        case '1.9.0':
        case '1.9.2':
        case '1.9.3':
        case '1.9.4':
        case '1.9.5':
        case '1.9.6':
        case '1.9.7':
        case '1.9.8':
          AdhocStorage.getset(VERSION, () => {
            if (Helper.ajustes().primera_vez) return;

            // Resincronizar los ciclos!
            realm.write(() =>
              realm
                .objects('Ciclo')
                .forEach((c) => (c.updated_at = new Date())),
            );
            // Nos habíamos olvidado de reprogramarlas en 1.9.3
            this.programar_notificaciones();
          });
          break;
        case '1.9.9':
        case '1.9.10':
          AdhocStorage.getset(VERSION, () => {
            if (!Helper.ajustes().primera_vez) return;

            setTimeout(() => this.programar_notificaciones(), 5000);
          });
          break;
        case '1.9.14':
          Notificaciones.cancelar('1000');
          break;
        case '1.10.04':
          AdhocStorage.getset(VERSION, () => {
            setTimeout(() => this.programar_notificaciones(), 5000);
          });
          break;
      }
    } catch (e) {
      Sentry.captureException(e);
    }

    // XXX: Por alguna razón algunas usuarias sincronizaron que la
    // imagen de fase era la imagen en texto plano.
    realm
      .objects('Fase')
      .filtered('imagen != ""')
      .map((x) => x.imagen)
      .filter((x) => x.startsWith(PANEL))
      .forEach((uri) =>
        Image.prefetch(uri)
          .then((e) => e)
          .catch((e) => Sentry.captureException(e)),
      );
    realm
      .objects('Slide')
      .filtered('imagen != ""')
      .forEach((slide) =>
        Image.prefetch(slide.imagen)
          .then((e) => e)
          .catch((e) => Sentry.captureException(e)),
      );
    Helper.recomendados().forEach((r) =>
      Image.prefetch(r.favicon_url)
        .then((e) => e)
        .catch((e) => Sentry.captureException(e)),
    );
  }

  /*
   * Volver al estado inicial
   */
  reset_state() {
    this.setState(this.obtener_estado());
  }

  /*
   * Obtener el ángulo de cada día
   */
  paso(ciclo) {
    return 360 / (ciclo.duracion + 1);
  }

  /*
   * Devuelve el ángulo de rotación del día en la espiral
   */
  rotacion_actual(ciclo) {
    // Ajustar los ángulos para que empecemos siempre desde el mismo
    // punto
    if (ciclo.dia > ciclo.duracion) return 0;

    return this.paso(ciclo) * (ciclo.dia - 1);
  }

  /*
   * Cambiar visibilidad de un elemento
   */
  toggle(key) {
    const s = {};

    s[key] = !this.state[key];

    this.setState(s);
  }

  /*
   * La usuaria presionó el botón me vino, informando a la app que
   * empieza un ciclo nuevo.
   */
  me_vino(fecha) {
    Logger.log('Espiral.me_vino', 'Gota presionada');

    const me_vino = m(fecha).startOf('day').toDate();
    const ciclo_pasado = this.state.ciclo;
    const ciclo = Helper.crear_ciclo(me_vino);

    this.toggle('boton_me_vino', false);
    this.toggle('boton_se_me_fue', true);

    realm.write(() => {
      ciclo_pasado.activo = false;
      // El ciclo anterior se terminó ayer a menos que el ciclo se haya
      // pasado de fecha, en ese caso dejamos la fecha estimada
      if (ciclo_pasado.dia <= Helper.duracion_maxima()) {
        ciclo_pasado.fin_del_ciclo = m(ciclo.me_vino).add(-1, 'days').toDate();
        ciclo_pasado.updated_at = new Date();
      }
    });

    this.programar_notificaciones();
  }

  /*
   * La usuaria tocó el botón se me fue, informando a la app que terminó
   * el ciclo menstrual.
   */
  se_me_fue(fecha) {
    Logger.log('Espiral.se_me_fue', 'Gota presionada');

    const se_me_fue = m(fecha).startOf('day').toDate();
    const ciclo = this.state.ciclo;

    Logger.log('Espiral.se_me_fue.ciclo_original', ciclo);

    realm.write(() => {
      ciclo.se_me_fue = se_me_fue;
      ciclo.se_me_fue_temporal = false;
      ciclo.updated_at = new Date();
    });

    Logger.log('Espiral.se_me_fue.ciclo_nuevo', ciclo);

    if (se_me_fue.getTime() === Helper.today().getTime())
      Toast.show(I18n.t('espiral.se_me_fue_toast'));

    this.programar_notificaciones();
  }

  /*
   * Obtiene el estado actual de la app
   */
  obtener_estado() {
    const ajustes = Helper.ajustes();
    const ciclo = Helper.ciclo_actual();
    const fase = Helper.fase_actual(ciclo);

    return {
      ciclo: ciclo,
      ajustes: ajustes,
      fase: fase,
      rotation: this.rotacion_actual(ciclo),
      paso: this.paso(ciclo),
      boton_me_vino: fase.id !== 1,
      boton_se_me_fue: fase.id === 1,
      reconfiguracion: Helper.mostrar_reconfiguracion(ciclo),
      exceso: Helper.mostrar_exceso(ciclo),
      nota_o_encuesta: false,
      espiral: this.calcular_espiral(),
      me_vino: {maxDate: Helper.today()},
      se_me_fue: { minDate: ciclo.me_vino },
    };
  }

  /*
   * Reconfigura las notificaciones
   */
  programar_notificaciones() {
    Logger.log(
      'Espiral.programar_notificaciones',
      'Programando notificaciones',
    );

    Notificaciones.vaciar();

    Notificaciones.no_me_vino(this.state.ciclo.fin_del_ciclo);
    Notificaciones.periodicas(
      this.state.ajustes.frecuencia_notificaciones,
      this.state.ciclo,
    );
  }

  /*
   * Oculta el modal de reconfiguración
   */
  ocultar_reconfiguracion() {
    this.toggle('reconfiguracion', false);

    // Guardar la fecha sin mostrar toast
    Helper.guardar_ajuste('reconfiguracion', Helper.today(), false);
  }

  /*
   * Detecta si el día tiene una nota asociada
   */
  tiene_nota() {
    const hoy = Helper.today();
    const notas = realm.objects('Nota').filtered('created_at == $0', hoy);

    return notas.length > 0;
  }

  /*
   * Detecta el día tiene una encuesta asociada
   */
  tiene_encuesta() {
    const encuestas = realm
      .objects('Encuesta')
      .filtered(
        'created_at >= $0 && created_at <= $1',
        Helper.today(),
        Helper.end_of_today(),
      );

    return encuestas.length > 0;
  }

  /*
   * Ir a la vista de encuestas, pasando por la pantalla de registro si
   * es necesario.
   */
  como_te_sentis() {
    this.toggle('nota_o_encuesta', false);

    const props = {fase: this.state.fase, ciclo: this.state.ciclo, fecha: m(Helper.today()).format('YYYY-MM-DD')};

    const encuestas = realm
      .objects('Encuesta')
      .filtered(
        'created_at >= $0 && created_at <= $1',
        Helper.today(),
        Helper.end_of_today(),
      );

    Logger.log('Encuestas de hoy', encuestas.length);

    // Pasar la encuesta como un objeto para poder modificarla
    if (encuestas.length > 0) {
      const e = encuestas[encuestas.length - 1];

      // XXX: Convertir sintomas y dolores a arrays
      props.encuesta = {
        id: e.id,
        como_te_sentis: e.como_te_sentis.slice(),
        productos_utilizados: e.productos_utilizados.slice(),
        dolores: e.dolores.map((d) => d.parte_del_cuerpo),
        sintomas: e.sintomas.map((s) => s.nombre),
        nivel_de_sangrado: e.nivel_de_sangrado,
        sangrado_durante_o_despues_del_sexo:
          e.sangrado_durante_o_despues_del_sexo,
        duracion_del_sangrado: e.duracion_del_sangrado,
        intensidad_del_dolor: e.intensidad_del_dolor,
        color_del_flujo_vaginal: e.color_del_flujo_vaginal,
        cantidad_del_flujo_vaginal: e.cantidad_del_flujo_vaginal,
        olor_del_flujo_vaginal: e.olor_del_flujo_vaginal,
        textura_del_flujo: e.textura_del_flujo,
        marca_vacuna: e.marca_vacuna,
        pais: e.pais,
        dosis: e.dosis,
      };
    }

    Logger.log('Espiral', props.encuesta);

    // Si la usuaria no está registrada y no dio consentimiento aún,
    // lo pedimos, sino no volvemos a molestar
    if (Helper.pedir_consentimiento()) {
      Actions.pedir_consentimiento(props);
    } else {
      Actions.como_te_sentis(props);
    }
  }

  /*
   * Calcula los valores de la espiral
   */
  calcular_espiral() {
    const espiral = {
      medidas: {
        view: {h: Platform.OS === 'ios' ? WIDTH : WIDTH * 0.9, w: WIDTH * 0.9},
        image: {h: WIDTH * 0.7, w: WIDTH * 0.7},
        dia: {r: 15},
        text: {s: 12},
        circle: {},
        pencil: {s: 0.6},
        line: {w: 40, h: 1, y1: 1, y2: 1},
      },
      pos: {
        image: {},
        dia: {},
        text: {},
        circle: {},
        pencil: {},
      },
      offset: {
        pencil: {x: -30, y: -30},
      },
    };

    // El centro de la espiral
    espiral.medidas.center = {
      x: espiral.medidas.view.w / 2,
      y: espiral.medidas.view.h / 2,
    };

    espiral.medidas.circle.r = espiral.medidas.image.h / 2;
    espiral.medidas.line.x1 =
      (espiral.medidas.view.w - espiral.medidas.line.w) / 2;
    espiral.medidas.line.x2 =
      (espiral.medidas.view.w + espiral.medidas.line.w) / 2;

    // La imagen principal va en el centro pero puede ser más chica
    espiral.pos.image = {
      y: (espiral.medidas.view.h - espiral.medidas.image.h) / 2,
      x: (espiral.medidas.view.w - espiral.medidas.image.w) / 2,
    };

    // El día va al centro horizontal y al comienzo del borde de la
    // espiral
    espiral.pos.dia = {
      x: espiral.medidas.view.w / 2,
      y: espiral.pos.image.y,
    };

    // El texto va en el centro del día
    espiral.pos.text = {
      x: espiral.medidas.view.w / 2,
      y: Platform.OS === 'ios' ? 66 : 38,
    };

    // El lápiz de editar notas
    espiral.pos.pencil = {
      x: espiral.pos.dia.x,
      y: espiral.pos.dia.y + espiral.offset.pencil.y,
    };

    // El círculo que nos ayuda a llevar la rotación
    espiral.pos.circle = {
      x: espiral.medidas.center.x,
      y: espiral.medidas.center.y,
    };

    return espiral;
  }

  /*
   * Dibujar la vista
   */
  render() {
    return (
      <Cajon back={false}>
        <View style={[STYLES.container, STYLES.main]}>
          <ConfiguracionInicial mostrar={this.state.ajustes.primera_vez} />

          <LunarModal
            show={this.state.modal_notificacion}
            fase={this.state.fase}
            onPress={() => this.toggle('modal_notificacion', false)}>
            <Paragraph center>{this.state.notificacion}</Paragraph>
          </LunarModal>

          <LunarModal
            show={this.state.reconfiguracion}
            fase={this.state.fase}
            onPress={() => this.ocultar_reconfiguracion()}>
            <Paragraph center>{I18n.t('espiral.te_pasaste')}</Paragraph>
          </LunarModal>

          <LunarModal
            show={this.state.exceso}
            fase={this.state.fase}
            onPress={() => this.toggle('exceso', false)}>
            <Paragraph center>{I18n.t('espiral.exceso')}</Paragraph>
          </LunarModal>

          <LunarModal
            show={this.state.nota_o_encuesta}
            fase={this.state.fase}
            onPress={() => this.toggle('nota_o_encuesta', false)}>
            <View style={{marginBottom: 30}}>
              <View style={{alignItems: 'center', justifyContent: 'center'}}>
                <Paragraph center>{I18n.t('espiral.que_hacer')}</Paragraph>
              </View>

              <View style={{margin: 5}}>
                <Icon.Button
                  name="book"
                  style={{backgroundColor: '#999999'}}
                  onPress={() => {
                    this.toggle('nota_o_encuesta', false);
                    Actions.nota({ciclo: this.state.ciclo});
                  }}>
                  <BotonText>
                    {this.tiene_nota()
                      ? I18n.t('espiral.modificar_nota')
                      : I18n.t('espiral.escribir_nota')}
                  </BotonText>
                </Icon.Button>
              </View>

              <View style={{margin: 5}}>
                <Icon.Button
                  name="smile-o"
                  style={{backgroundColor: '#999999'}}
                  onPress={() => this.como_te_sentis()}>
                  <BotonText>
                    {this.tiene_encuesta()
                      ? I18n.t('espiral.modificar_estados')
                      : I18n.t('espiral.registrar_estados')}
                  </BotonText>
                </Icon.Button>
              </View>
            </View>
          </LunarModal>

          <View style={{flex: 1, alignItems: 'center'}}>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <FaseH1>
                {I18n.t('espiral.fase').toUpperCase()} {this.state.fase.id}.{' '}
              </FaseH1>
              <FaseH2>{this.state.fase.nombre}</FaseH2>
            </View>

            <FaseH3>{this.state.fase.etapa.toUpperCase()}</FaseH3>
          </View>

          <TouchableOpacity
            onPress={() => this.toggle('nota_o_encuesta', true)}>
            <View
              style={{
                height: this.state.espiral.medidas.view.h,
                width: this.state.espiral.medidas.view.w,
              }}>
              <Image
                resizeMode="contain"
                source={{uri: 'https://panel.lunarcomunidad.com/ios/espiral.png'}}
                style={{
                  position: 'absolute',
                  top: this.state.espiral.pos.image.y,
                  left: this.state.espiral.pos.image.x,
                  width: this.state.espiral.medidas.image.h,
                  height: this.state.espiral.medidas.image.w,
                }}
              />

              <Svg
                height={this.state.espiral.medidas.view.h}
                width={this.state.espiral.medidas.view.w}>
                <Line
                  x1={this.state.espiral.medidas.line.x1}
                  x2={this.state.espiral.medidas.line.x2}
                  y1={this.state.espiral.medidas.line.y1}
                  y2={this.state.espiral.medidas.line.y2}
                  strokeWidth={this.state.espiral.medidas.line.h}
                  stroke="black"
                />

                <G
                  rotation={this.state.rotation}
                  originX={this.state.espiral.medidas.center.x}
                  originY={this.state.espiral.medidas.center.y}>
                  <G>
                    <G
                      x={this.state.espiral.pos.pencil.x}
                      y={this.state.espiral.pos.pencil.y}
                      rotation="90"
                      scale={this.state.espiral.medidas.pencil.s}>
                      <G
                        x={this.state.espiral.offset.pencil.x}
                        y={this.state.espiral.offset.pencil.y}
                        fill={this.state.fase.color}>
                        <Polygon points="49.938,31.599 51.48,30.722 49.949,29.826" />
                        <Path d="m 44.514,25.386 -0.061,10.57 9.184,-5.236 -9.123,-5.334 z m 0.699,1.227 7.012,4.1 -7.059,4.025 0.047,-8.125 z" />
                        <Rect
                          x="11.363"
                          y="25.33"
                          width="32.308998"
                          height="10.627"
                        />
                      </G>
                    </G>

                    <G
                      rotation={this.state.rotation * -1}
                      originX={this.state.espiral.medidas.center.x}
                      originY={this.state.espiral.pos.dia.y}>
                      <Circle
                        r={this.state.espiral.medidas.dia.r}
                        cx={this.state.espiral.pos.dia.x}
                        cy={this.state.espiral.pos.dia.y}
                        fill={this.state.fase.color}
                      />

                      <Text
                        fill="white"
                        fontSize={this.state.espiral.medidas.text.s}
                        x={this.state.espiral.pos.text.x}
                        y={this.state.espiral.pos.text.y}
                        fontWeight="bold"
                        textAnchor="middle">
                        {this.state.ciclo.dia.toString()}
                      </Text>
                    </G>

                    <Circle
                      cx={this.state.espiral.pos.circle.x}
                      cy={this.state.espiral.pos.circle.y}
                      r={this.state.espiral.medidas.circle.r}
                      fill="transparent"
                    />
                  </G>
                </G>
              </Svg>
            </View>
          </TouchableOpacity>

          <BarraDeAspectos
            width={this.state.espiral.medidas.image.w}
            fase={this.state.fase}
          />

          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center',
              width: this.state.espiral.medidas.image.w,
              height: 10,
            }}>
            {this.state.boton_me_vino && (
              <View>
                <DatePicker
                  ref="me_vino"
                  locale={I18n.current_short_locale}
                  mode="date"
                  date={m(this.state.me_vino.maxDate).format('YYYY-MM-DD')}
                  format="YYYY-MM-DD"
                  confirmBtnText={I18n.t('espiral.me_vino')}
                  cancelBtnText={I18n.t('espiral.cancelar')}
                  showIcon={false}
                  hideText={true}
                  maxDate={m(this.state.me_vino.maxDate).format('YYYY-MM-DD')}
                  onDateChange={(date) => this.me_vino(date)}
                  style={{height: 0}}
                  customStyles={{
                    dateInput: {height: 0, borderWidth: 0},
                    dateTouchBody: {height: 0},
                  }}
                />
                <MeVino
                  text={true}
                  onPress={() => this.refs.me_vino.onPressDate()}
                />
              </View>
            )}

            {this.state.boton_se_me_fue && (
              <View>
                <DatePicker
                  ref="se_me_fue"
                  locale={I18n.current_short_locale}
                  mode="date"
                  hideText={true}
                  date={m(Helper.today()).format('YYYY-MM-DD')}
                  minDate={m(this.state.se_me_fue.minDate).format('YYYY-MM-DD')}
                  maxDate={m(Helper.today()).format('YYYY-MM-DD')}
                  format="YYYY-MM-DD"
                  confirmBtnText={I18n.t('espiral.se_me_fue')}
                  cancelBtnText={I18n.t('espiral.cancelar')}
                  showIcon={false}
                  onDateChange={(date) => this.se_me_fue(date)}
                  style={{height: 0}}
                  customStyles={{
                    dateInput: {height: 0, borderWidth: 0},
                    dateTouchBody: {height: 0},
                  }}
                />
                <SeMeFue
                  text={true}
                  onPress={() => this.refs.se_me_fue.onPressDate()}
                />
              </View>
            )}
          </View>
        </View>
      </Cajon>
    );
  }
}
