'use strict'

import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { View, TouchableOpacity } from 'react-native'
import { Actions } from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/FontAwesome'
import Grid from 'react-native-grid-component'
import m from 'moment'

import realm from './models'

import { STYLES } from './styles'
import { Cajon } from './cajon'
import { Small, Paragraph, FaseH1, Color, BotonText } from './pseudo_html'
import { Helper } from './helper'
import { Logger } from './logger'
import { I18n } from './i18n'

/*
 * Listado de notas
 */
export class Notas extends Component {
  constructor () {
    super()

    Logger.log('Notas.constructor', 'Viendo notas')

    this.state = { data: this.obtener_notas() }
    this.update_data_source = this.update_data_source.bind(this)

    this.ciclo = Helper.ciclo_actual()
    this.fase = Helper.fase_actual(this.ciclo)

    Logger.log('Notas.constructor', this.state)
    Logger.log('Notas.constructor', this.ciclo)
    Logger.log('Notas.constructor', this.fase)
  }

  /*
   * Esperar por cambios en las notas
   */
  componentDidMount () {
    realm.objects('Nota').addListener(this.update_data_source)
  }

  /*
   * Dejar de esperar cambios en las notas
   */
  componentWillUnmount () {
    Logger.log('Notas.componentWillUnmount', 'Saliendo de notas')
    realm.objects('Nota').removeListener(this.update_data_source)
  }

  /*
   * Actualizar la lista si las notas cambian
   */
  update_data_source (collection, changes) {
    Logger.log('Notas.update_data_source', 'Actualizando datos')
    this.setState({ data: this.obtener_notas() })
  }

  /*
   * Obtener un listado de notas ordenadas por fecha (más nueva primero)
   * para completar la grilla.
   */
  obtener_notas () {
    return realm
      .objects('Nota')
      .sorted('created_at', true)
      .map(nota => { return { key: nota.id, nota: nota } })
  }

  /*
   * Muestra una grilla de notas, con un botón al fondo para crear una
   * nota nueva dentro del ciclo actual.
   */
  render () {
    return (
      <Cajon back={true}>
        <View style={STYLES.nota_grid}>
          <Grid
            itemsPerRow={2}
            data={this.state.data}
            itemHasChanged={(i1, i2) => i1 !== i2}
            renderItem={item => <NotaPreview nota={item.nota} key={item.key} id={item.key} />} />

          <View style={{ margin: 5 }}>
            <Icon.Button
              name="pencil"
              backgroundColor={this.fase.color}
              onPress={() => Actions.nota({ ciclo: this.ciclo })}>
              <BotonText>{I18n.t('notas.agregar')}</BotonText></Icon.Button>
          </View>
        </View>
      </Cajon>
    )
  }
}

/*
 * Previsualización de la nota
 */
class NotaPreview extends Component {
  static propTypes = {
   nota: PropTypes.object.isRequired
  };

  /*
   * Abre la nota por su ID
   */
  ir_a_nota (id) {
    Actions.nota({ id: id })
  }

  render () {
    const nota = this.props.nota
    const fase = realm.objectForPrimaryKey('Fase', nota.ciclo.calcular_fase(nota.dia))
    const lineas = nota.texto.split('\n').filter(Boolean).length - 1

    const i18n = {
      fase: I18n.t('notas.fase').toUpperCase(),
      dia: I18n.t('notas.dia').toUpperCase(),
      lineas: I18n.t('notas.lineas', { lineas: lineas })
    }

    return (
      <View style={STYLES.nota_preview}>
        <TouchableOpacity onPress={() => this.ir_a_nota(nota.id)}>
          <View>
            <Color color={fase.color}>
              <FaseH1>
                <Small>
                  {i18n.fase} {fase.id} - {i18n.dia} {nota.dia} - {m(nota.created_at).format(I18n.t('moment.date'))}
                </Small>
              </FaseH1>
            </Color>

            <Paragraph><Small>{nota.texto.split('\n', 1).slice(0, 19)}</Small></Paragraph>

            {lineas > 0
              ? <Paragraph><Small>{i18n.lineas}</Small></Paragraph>
              : null }
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}
