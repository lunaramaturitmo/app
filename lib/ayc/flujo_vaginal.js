/* eslint-disable prettier/prettier */
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { View, TouchableOpacity, Text, FlatList, Image } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import CheckBox from 'react-native-check-box'
import Slider from '@react-native-community/slider'

import { Sintoma } from './sintoma'
import { STYLES, WIDTH, HEIGHT } from '../styles'
import { H1, Paragraph, Small } from '../pseudo_html'
import { Cajon } from '../cajon'
import { VaginalDischarge } from '../svg_elements'
import { I18n } from '../i18n'
import { Logger } from '../logger'

/*
 * Flujo vaginal
 */
export class FlujoVaginal extends Sintoma {
  constructor (props) {
    super(props)

    this.state = {
      fase: this.props.fase,
      ciclo: this.props.ciclo,
      encuesta: this.props.encuesta,
      acciones: this.props.acciones.filter(item => item !== 'flujo_vaginal'),
      colors: [ 'transparent', 'white', '#fdf2ec', '#fcf9e5', '#fef3a2',
        '#fbd55d', '#dcf18f', '#f19f96', '#eb6073', '#d1646e',
        '#7f6167', '#4c4c48' ],
      texturas: [ 'seco', 'pegajoso', 'cremoso', 'elastico' ]
    }

    if (this.state.encuesta.color_del_flujo_vaginal === undefined) { this.state.encuesta.color_del_flujo_vaginal = '' }

    if (this.state.encuesta.olor_del_flujo_vaginal === undefined) { this.state.encuesta.olor_del_flujo_vaginal = false }

    if (this.state.encuesta.cantidad_del_flujo_vaginal === undefined) { this.state.encuesta.cantidad_del_flujo_vaginal = 0 }

    if (this.state.encuesta.textura_del_flujo === undefined) { this.state.encuesta.textura_del_flujo = '' }

    Logger.log('FlujoVaginal', this.state.encuesta)
  }

  /*
   * Activar o desactivar el botón de olor
   */
  toggle_smell () {
    this.agregar_a_encuesta('olor_del_flujo_vaginal',
      !this.state.encuesta.olor_del_flujo_vaginal)
  }

  /*
   * El color es transparente
   */
  transparent() {
    return (this.state.encuesta.color_del_flujo_vaginal === 'transparent')
  }

  /*
   * Textura individual
   */
  render_textura (item) {
    return (
      <CheckBox
        rightTextView={(<View style={{marginLeft: 5}}><Paragraph>{item.name}</Paragraph></View>)}
        isChecked={item.key === this.state.encuesta.textura_del_flujo}
        uncheckedCheckBoxColor="black"
        checkedCheckBoxColor="black"
        unCheckedImage={this.radio(false)}
        checkedImage={this.radio(true)}
        onClick={() => this.agregar_a_encuesta('textura_del_flujo', item.key)} />
    )
  }

  /*
   * Radios
   */
  radio(state = false) {
    const source = state ? 'https://panel.lunarcomunidad.com/ios/radio_checked.png' : 'https://panel.lunarcomunidad.com/ios/radio_unchecked.png'
    return (<Image style={{width: 25, height: 25, resizeMode: 'contain'}} source={{uri: source}}/>)
  }

  render () {
    return (
      <Cajon back={true}>
        <View style={[STYLES.container, { justifyContent: 'flex-start' }]}>
          <View style={STYLES.center}>
            <H1>{I18n.t('flujo_vaginal.color')}</H1>
          </View>

          <View style={{
            flexWrap: 'wrap',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            width: WIDTH * 0.7,
            marginBottom: 10,
            marginTop: 10 }}>

            {this.state.colors.map((color, i) => {
              return (<ColorDelFlujo
                key={`color-${i.toString()}`}
                color={color}
                selected={(this.state.encuesta.color_del_flujo_vaginal === color)}
                onPress={() => this.agregar_a_encuesta('color_del_flujo_vaginal', color)}/>)
            }
            )}

           <Paragraph
             style={{
               marginBottom: 0,
               opacity: this.transparent() ? 1 : 0
             }}><Small>{I18n.t('flujo_vaginal.transparent')}</Small></Paragraph>
          </View>

          <View style={STYLES.center}>
            <H1>{I18n.t('flujo_vaginal.cantidad')}</H1>
          </View>

          <View style={{
            width: WIDTH - 20,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginTop: 10
          }}>
            <VaginalDischarge height="30" width="30"/>
            <Paragraph><Small>{I18n.t('flujo_vaginal.promedio')}</Small></Paragraph>
            <VaginalDischarge height="40" width="40"/>
          </View>

          <Slider
            value={this.state.encuesta.cantidad_del_flujo_vaginal}
            style={{ width: WIDTH - 20, marginBottom: 10 }}
            minimumValue={-1}
            maximumValue={1}
            step={1}
            thumbTintColor="black"
            maximumTrackTintColor="black"
            minimumTrackTintColor="black"
            onValueChange={nivel => this.agregar_a_encuesta('cantidad_del_flujo_vaginal', nivel)} />

          {this.state.encuesta.olor_del_flujo_vaginal ?
          <View style={{ alignItems: 'center' }}>
            <View>
              <CheckBox
                rightTextView={(<View><Paragraph>{I18n.t('flujo_vaginal.olor')}</Paragraph></View>)}
                isChecked={this.state.encuesta.olor_del_flujo_vaginal}
                uncheckedCheckBoxColor="grey"
                checkedCheckBoxColor="grey"
                onClick={() => this.toggle_smell()} />
            </View>
          </View>
          : null}

          <View style={STYLES.center}>
            <H1>{I18n.t('flujo_vaginal.textura')}</H1>
          </View>

          <FlatList
            style={{ width: WIDTH - 20, marginTop: 10 }}
            keyExtractor={(item, index) => item.key}
            data={I18n.t('flujo_vaginal.texturas')}
            renderItem={({ item }) => this.render_textura(item) } />

          <View style={{ width: WIDTH, padding: 5, margin: 5, flex: 1, justifyContent: 'flex-end' }}>
            <Icon.Button
              name="check"
              style={{ backgroundColor: this.state.fase.color }}
              onPress={() => this.continuar_encuesta()}>
              <Text style={STYLES.boton}>{I18n.t('flujo_vaginal.continuar')}</Text>
            </Icon.Button>
          </View>
        </View>
      </Cajon>
    )
  }
}

/*
 * Un recuadro que representa el color actual
 */
class ColorDelFlujo extends Component {
  static propTypes = {
    color: PropTypes.string.isRequired,
    selected: PropTypes.bool,
    onPress: PropTypes.func.isRequired
  }

  static defaultProps = {
    selected: false
  }

  constructor (props) {
    super(props)

    this.state = {
      color: this.props.color,
      selected: this.props.selected,
      onPress: this.props.onPress
    }
  }

  UNSAFE_componentWillReceiveProps (new_props) {
    this.setState({
      selected: new_props.selected,
      color: new_props.color,
      onPress: new_props.onPress
    })
  }

  /*
   * Realizar la acción si no está presionado
   */
  handlePress () {
    if (this.state.selected) return

    this.state.onPress(this.state.color)
  }

  style () {
    const s = {
      margin: 5,
      width: WIDTH * 0.08,
      height: HEIGHT * 0.05,
      backgroundColor: this.state.color,
      borderColor: 'black',
      borderRadius: 1,
      borderWidth: 1
    }

    switch (true) {
      case (this.state.color === 'transparent'):
        s.borderStyle = 'dotted'
      case (this.state.selected):
        s.borderWidth = 2
        if (!s.borderStyle) s.borderStyle = 'solid'
    }

    return s
  }

  render () {
    return (
      <TouchableOpacity
        style={this.style()}
        onPress={() => this.handlePress()}></TouchableOpacity>
    )
  }
}
