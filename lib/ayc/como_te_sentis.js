import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Actions } from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/FontAwesome'

import m from 'moment'

import { View, Image, TouchableOpacity, Text } from 'react-native'
import { Logger } from '../logger'
import { STYLES, WIDTH, HEIGHT } from '../styles'
import { H1, Color, Small, Paragraph } from '../pseudo_html'
import { Cajon } from '../cajon'
import { I18n } from '../i18n'

/*
 * Grilla de estados, la pantalla principal de AYC.
 *
 * Recibe o crea una encuesta como un objeto y lo va pasando de vista en
 * vista hasta llegar a Gracias, que lo guarda.
 */
export class ComoTeSentis extends Component {
  static propTypes = {
    fase: PropTypes.object.isRequired,
    ciclo: PropTypes.object.isRequired,
    encuesta: PropTypes.object,
    fecha: PropTypes.string,
  }

  static defaultProps = {
    encuesta: {
      como_te_sentis: [],
      dolores: [],
      sintomas: []
    }
  }

  constructor (props) {
    super(props)

    this.state = {
      fase: this.props.fase,
      ciclo: this.props.ciclo,
      encuesta: this.props.encuesta,
      fecha: this.props.fecha,
    };
    // Inicializar los valores para la encuesta si no estaban definidos
    ['como_te_sentis', 'dolores', 'sintomas'].forEach(k => {
      if (this.state.encuesta[k] !== undefined) return

      this.state.encuesta[k] = []
    })

    Logger.log('ComoTeSentis.props', this.props.encuesta)
    Logger.log('ComoTeSentis.state', this.state.encuesta)
  }

  /*
   * El segundo paso son los síntomas
   */
  continuar_encuesta () {
    Actions.sintomas({
      fase: this.state.fase,
      ciclo: this.state.ciclo,
      encuesta: this.state.encuesta,
      fecha: this.state.fecha,
    })
  }

  /*
   * Agregar o quitar sensaciones
   */
  como_te_sentis (sensacion, estado = true) {
    // Trabajamos sobre una copia para poder cambiar el estado luego
    let sensaciones = this.state.encuesta.como_te_sentis.slice(0)

    if (estado) {
      // Agregar la sensación a la lista si no estaba
      if (!this.incluida_en_como_te_sentis(sensacion)) sensaciones.push(sensacion)
    } else {
      // Sino quitarla
      sensaciones = sensaciones.filter(x => x !== sensacion)
    }

    // Cambiar el estado
    this.agregar_a_encuesta('como_te_sentis', sensaciones)
  }

  /*
   * Cambiar el estado de la encuesta
   */
  agregar_a_encuesta (llave, valor) {
    const encuesta = this.state.encuesta
    encuesta[llave] = valor

    this.setState({ encuesta: encuesta })
  }

  /*
   * Está incluida?
   */
  incluida_en_como_te_sentis (sensacion) {
    return this.state.encuesta.como_te_sentis.includes(sensacion)
  }

  /*
   * Cada una de las sensaciones
   */
  render_item (item) {
    return (<Sensacion
      name={item.name}
      remote_id={item.key}
      key={item.key}
      color={item.color}
      selected={this.incluida_en_como_te_sentis(item.key)}
      onPress={key => this.como_te_sentis(key, !this.incluida_en_como_te_sentis(item.key))}
    />)
  }

  /*
   * TODO: centrar las sensaciones
   */
  render () {
    return (
      <Cajon back={true}>
        <View style={[STYLES.nota_grid, {justifyContent: 'space-between'}]}>

          <View style={[STYLES.center, {marginBottom: 10}]}>
            <H1>{I18n.t('como_te_sentis.title')}{m(this.props.fecha).format(I18n.t('moment.short_date'))}</H1>
          </View>

          <View style={{ flexWrap: 'wrap', flexDirection: 'row' }}>
            {I18n.t('como_te_sentis.estados').map(item => this.render_item(item))}
          </View>

          <View style={{ padding: 5, margin: 5, justifyContent: 'flex-end' }}>
            <Icon.Button
              name="check"
              style={{ backgroundColor: this.state.fase.color }}
              onPress={() => this.continuar_encuesta()}>
              <Text style={STYLES.boton}>{I18n.t('como_te_sentis.senales_fisicas')}</Text>
            </Icon.Button>
          </View>
        </View>
      </Cajon>
    )
  }
}

/*
 * La sensación individual
 */
class Sensacion extends Component {
  static propTypes = {
    remote_id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired,
    selected: PropTypes.bool
  }

  static defaultProps = { selected: false }

  constructor (props) {
    super(props)

    this.state = {
      remote_id: this.props.remote_id,
      name: this.props.name,
      color: this.props.color,
      onPress: this.props.onPress,
      selected: this.props.selected,
      image: {
        feliz: {uri: 'https://panel.lunarcomunidad.com/ios/happy.png'},
        triste: {uri: 'https://panel.lunarcomunidad.com/ios/sad.png'},
        enojada: {uri: 'https://panel.lunarcomunidad.com/ios/angry.png'},
        preocupada: {uri: 'https://panel.lunarcomunidad.com/ios/worried.png'},
        bien: {uri: 'https://panel.lunarcomunidad.com/ios/allright.png'},
        cansada: {uri: 'https://panel.lunarcomunidad.com/ios/tired.png'},
        hambrienta: {uri: 'https://panel.lunarcomunidad.com/ios/hungry.png'},
        elevada: {uri: 'https://panel.lunarcomunidad.com/ios/frisky.png'},
      }
    }
  }

  /*
   * Cambiamos el estado desde el contenedor
   */
  UNSAFE_componentWillReceiveProps (next) {
    this.setState({
      remote_id: next.remote_id,
      name: next.name,
      color: next.color,
      onPress: next.onPress,
      selected: next.selected
    })
  }

  /*
   * Ejecutar la función al presionar
   */
  handlePress () {
    this.state.onPress(this.state.remote_id)
  }

  /*
   * Estilo para el texto
   */
  paragraph_style () {
    const selected_style = {}

    if (this.state.selected) {
      selected_style.fontFamily = 'Raleway-ExtraBold'
      selected_style.fontWeight = 'bold'
    }

    return selected_style
  }

  /*
   * Resaltar con un borde
   */
  border_style () {
    const border_style = {
      alignItems: 'center',
      width: (WIDTH / 2) - 10,
      borderWidth: 2,
      borderColor: 'transparent',
      margin: 5,
      paddingTop: 5
    }

    if (this.state.selected)
      border_style.borderColor = this.state.color

    return border_style
  }

  render () {
    return (
      <View style={this.border_style()}>
        <TouchableOpacity onPress={() => this.handlePress()} style={{}}>
          <View style={{alignItems: 'center'}}>
            <Image style={{ height: 50, width: 50}} source={this.state.image[this.state.remote_id]} />

            <Paragraph style={this.paragraph_style()}>
              <Small>
                <Color color={this.state.color}>{this.state.name}</Color>
              </Small>
            </Paragraph>
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}
