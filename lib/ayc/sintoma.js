import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Actions } from 'react-native-router-flux'
import { Logger } from '../logger'

export class Sintoma extends Component {
  static propTypes = {
    encuesta: PropTypes.object.isRequired,
    fase: PropTypes.object.isRequired,
    ciclo: PropTypes.object.isRequired,
    acciones: PropTypes.array.isRequired
  }

  /*
   * Ir al siguiente síntoma o guardar la encuesta.
   */
  continuar_encuesta () {
    if (this.state.acciones.length > 0) {
      // Ir a la primera y pasar la encuesta como prop
      Actions[this.state.acciones[0]]({
        encuesta: this.state.encuesta,
        fase: this.state.fase,
        ciclo: this.state.ciclo,
        acciones: this.state.acciones
      })
    } else {
      Actions.gracias({
        fase: this.state.fase,
        ciclo: this.state.ciclo,
        encuesta: this.state.encuesta
      })
    }
  }

  /*
   * Cambia el estado de la encuesta
   */
  agregar_a_encuesta (llave, valor) {
    const encuesta = this.state.encuesta
    encuesta[llave] = valor

    this.setState({ encuesta: encuesta })

    Logger.log('Sintoma.agregar_a_encuesta', encuesta)
  }
}
