import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions } from 'react-native-router-flux'
import { View } from 'react-native'
import React, { Component } from 'react'
import { Picker } from '@react-native-community/picker'

import { Sintoma } from './sintoma'
import { STYLES, WIDTH } from '../styles'
import { H1, Paragraph, BotonText } from '../pseudo_html'
import { Cajon } from '../cajon'
import { I18n } from '../i18n'
import { Logger } from '../logger'

/*
 * Pérdidas
 */
export class Perdidas extends Sintoma {
  constructor (props) {
    super(props)

    this.state = {
      encuesta: this.props.encuesta,
      fase: this.props.fase,
      ciclo: this.props.ciclo,
      acciones: this.props.acciones.filter(item => item !== 'perdidas')
    }

    if (this.state.encuesta.sangrado_durante_o_despues_del_sexo === undefined) { this.state.encuesta.sangrado_durante_o_despues_del_sexo = false }

    if (this.state.encuesta.duracion_del_sangrado === undefined) { this.state.encuesta.duracion_del_sangrado = 0 }

    Logger.log('Perdidas', this.state.encuesta)
  }

  /*
   * Muestra u oculta el selector de horas
   */
  toggle_hours (on = false) {
    this.agregar_a_encuesta('sangrado_durante_o_despues_del_sexo', on)

    // Eliminar el valor
    if (!on) this.agregar_a_encuesta('duracion_del_sangrado', 0)
  }

  render () {
    return (
      <Cajon back={true}>
        <View style={[STYLES.container, { justifyContent: 'flex-start' }]}>
          <View style={STYLES.center}>
            <View style={{ alignItems: 'center' }}>
              <H1>{I18n.t('perdidas.title')}</H1>
              <Paragraph center>{I18n.t('perdidas.durante_o_despues')}</Paragraph>

              <View style={{ width: WIDTH * 0.7, flexDirection: 'row', justifyContent: 'space-between', marginTop: 30 }}>
                <Icon.Button style={{
                  backgroundColor: (this.state.encuesta.sangrado_durante_o_despues_del_sexo) ? this.state.fase.color : 'grey'
                }} onPress={() => this.toggle_hours(true)}>
                  <BotonText>{I18n.t('perdidas.si')}&nbsp;&nbsp;</BotonText>
                </Icon.Button>

                <Icon.Button style={{
                  backgroundColor: (this.state.encuesta.sangrado_durante_o_despues_del_sexo) ? 'grey' : this.state.fase.color
                }} onPress={() => this.toggle_hours(false)}>
                  <BotonText>{I18n.t('perdidas.nope')}&nbsp;&nbsp;</BotonText>
                </Icon.Button>
              </View>
            </View>

            {this.state.encuesta.sangrado_durante_o_despues_del_sexo
              ? <View style={{ alignItems: 'center', marginTop: 30 }}>
                <H1>{I18n.t('perdidas.duracion')}</H1>

                <Picker
                  style={{ width: 100 }}
                  selectedValue={this.state.encuesta.duracion_del_sangrado.toString()}
                  onValueChange={(value, index) => this.agregar_a_encuesta('duracion_del_sangrado', parseInt(value))}>
                  {[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(i => {
                    i = i.toString()

                    return (<Picker.Item key={i} label={i} value={i} />)
                  })}
                </Picker>
              </View>
              : null }

            <View style={{ width: WIDTH, padding: 5, margin: 5, flex: 1, justifyContent: 'flex-end' }}>
              <Icon.Button
                name="check"
                style={{ backgroundColor: this.state.fase.color }}
                onPress={() => this.continuar_encuesta()}>
                <BotonText>{I18n.t('perdidas.continuar')}</BotonText>
              </Icon.Button>
            </View>
          </View>
        </View>
      </Cajon>
    )
  }
}
