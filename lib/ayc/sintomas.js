/* eslint-disable prettier/prettier */
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { View, FlatList, Platform } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import CheckBox from 'react-native-check-box'

import m from 'moment'

import { STYLES, WIDTH, HEIGHT } from '../styles'
import { H1, Paragraph, Small, Br, BotonText } from '../pseudo_html'
import { Cajon } from '../cajon'
import { Actions } from 'react-native-router-flux'
import { I18n } from '../i18n'
import { Logger } from '../logger'
import { VaginalDischarge, Bleeding, Bloating, BloodInUrine, Burning, Fever, Headache, Insomnia, Nausea, PainDuringSex, Pain, Spotting, Vaccination } from '../svg_elements'

/*
 * Lista de síntomas con cajas tildables, cada una activa o desactiva
 * síntomas y genera un listado de pantallas a las que continuar.
 *
 * Recibe la encuesta.
 */
export class Sintomas extends Component {
  static propTypes = {
    encuesta: PropTypes.object.isRequired,
    fase: PropTypes.object.isRequired,
    ciclo: PropTypes.object.isRequired,
    fecha: PropTypes.string,
  }

  constructor (props) {
    super(props)

    this.state = {
      show_help: false,
      encuesta: this.props.encuesta,
      fase: this.props.fase,
      ciclo: this.props.ciclo,
      fecha: this.props.fecha,
    }

    if (this.state.encuesta.sintomas === undefined) { this.state.encuesta.sintomas = [] }

    if (this.state.encuesta.dolores === undefined) { this.state.encuesta.dolores = [] }

    Logger.log('Sintomas', this.state.encuesta)

    this.icons = {
      sangrado: <Bleeding width="30" height="30" />,
      flujo_vaginal: <VaginalDischarge width="30" height="30" />,
      perdidas: <Spotting width="30" height="30" />,
      dolor: <Pain width="30" height="30" />,
      vacunacion: <Vaccination width="30" height="30" />,
      hinchazon: <Bloating width="30" height="30" />,
      sangre_en_orina: <BloodInUrine width="30" height="30" />,
      picazon: <Burning width="30" height="30" />,
      fiebre: <Fever width="30" height="30" />,
      insomnio: <Insomnia width="30" height="30" />,
      nausea: <Nausea width="30" height="30" />,
      dolor_durante_el_sexo: <PainDuringSex width="30" height="30" />,
    }
  }

  /*
   * Agrega o quita sintomas
   */
  guardar_o_quitar_sintoma (sintoma, estado = true) {
    // slice(0) es como clone()
    let sintomas = this.state.encuesta.sintomas.slice(0)

    if (estado) {
      if (!sintomas.includes(sintoma)) sintomas.push(sintoma)
    } else {
      sintomas = sintomas.filter(x => x !== sintoma)
    }

    this.agregar_a_encuesta('sintomas', sintomas)
  }

  /*
   * Agrega o quita de la encuesta modificando el estado
   */
  agregar_a_encuesta (llave, valor) {
    const encuesta = this.state.encuesta
    encuesta[llave] = valor

    this.setState({ encuesta: encuesta })

    Logger.log('Sintomas.agregar_a_encuesta', encuesta)
  }

  /*
   * Va a la siguiente pantalla si la hay
   */
  continuar_encuesta () {
    // Encontrar todos los sintomas que tienen acciones
    const acciones = this.state.encuesta.sintomas
      .filter(sintoma => (Actions[sintoma]))

    if (acciones.length > 0) {
      // Ir a la primera y pasar la encuesta como estado
      Actions[acciones[0]]({
        encuesta: this.state.encuesta,
        fase: this.state.fase,
        ciclo: this.state.ciclo,
        acciones: acciones
      })
    } else {
      Actions.gracias({
        fase: this.state.fase,
        ciclo: this.state.ciclo,
        encuesta: this.state.encuesta
      })
    }
  }

  /*
   * Renderiza cada síntoma individual
   */
  render_item (item) {
    return (
      <SintomaItem
      icon={this.icons[item.key]}
      name={item.name}
      help={item.help}
      remote_id={item.key}
      checked={this.state.encuesta.sintomas.includes(item.key)}
      show_help={this.state.show_help}
      onPress={(sintoma, estado) => this.guardar_o_quitar_sintoma(sintoma, estado)}
      color={item.key === 'vacunacion' ? "fuchsia" : "grey" }/>)
  }

  render () {
    return (
      <Cajon back={true}>
        <View style={[STYLES.container, STYLES.main]}>
          <View style={STYLES.center}>
            <H1>{I18n.t('sintomas.title')}{m(this.props.fecha).format(I18n.t('moment.short_date'))}</H1>
          </View>

          <FlatList
            style={{ height: HEIGHT * 0.4, width: WIDTH - 20 }}
            keyExtractor={(item, index) => item.key}
            data={I18n.t('sintomas.sintomas')}
            extraData={this.state.show_help}
            renderItem={({ item }) => Platform.OS !== 'ios' 
            ? this.render_item(item) 
            : item.key !== 'vacunacion' && this.render_item(item)} />

          <View style={{ width: WIDTH, padding: 5, flex: 0.2, flexDirection: 'row', justifyContent: 'space-between' }}>
            <View style={{ margin: 5 }}>
              <Icon.Button
                name="question-circle"
                backgroundColor={'black'}
                onPress={() => this.setState({ show_help: !this.state.show_help })}>
                <BotonText>{I18n.t('sintomas.help')}</BotonText>
              </Icon.Button>
            </View>
            <View style={{ margin: 5, width: WIDTH / 2 }}>
              <Icon.Button
                name="check"
                backgroundColor={this.state.fase.color}
                onPress={() => this.continuar_encuesta()}>
                <BotonText>{I18n.t('sintomas.continuar')}</BotonText>
              </Icon.Button>
            </View>
          </View>
        </View>
      </Cajon>
    )
  }
}

class SintomaItem extends Component {
  static propTypes = {
    remote_id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    help: PropTypes.string,
    color: PropTypes.string.isRequired,
    show_help: PropTypes.bool.isRequired,
    icon: PropTypes.element,
    onPress: PropTypes.func.isRequired,
    checked: PropTypes.bool
  }

  static defaultProps = {
    checked: false,
    icon: null,
    help: null
  }

  constructor (props) {
    super(props)
    this.state = {
      checked: this.props.checked,
      show_help: this.props.show_help
    }
  }

  UNSAFE_componentWillReceiveProps (next) {
    this.setState({ show_help: next.show_help, checked: next.checked })
  }

  handlePress () {
    var state = !this.state.checked
    this.props.onPress(this.props.remote_id, state)
    this.setState({ checked: state })
  }

  render_name_and_help () {
    return (
      <View>
        <View style={{ flex: 0.2, flexDirection: 'row', alignItems: 'flex-start', width: WIDTH * 0.7 }}>
          {this.props.icon}
          <Paragraph>
            {this.props.name}

            {(this.props.help &&
              this.state.show_help)
              ? <Paragraph><Br/><Small>{this.props.help}</Small></Paragraph>
              : null}
          </Paragraph>
        </View>
      </View>
    )
  }

  render () {
    return (
      <View style={{ padding: 10, width: WIDTH - 30, paddingRight: 20 }}>
        <CheckBox
          rightTextView={this.render_name_and_help()}
          isChecked={this.state.checked}
          uncheckedCheckBoxColor={this.props.color}
          checkedCheckBoxColor={this.props.color}
          onClick={() => this.handlePress()} />
      </View>
    )
  }
}
