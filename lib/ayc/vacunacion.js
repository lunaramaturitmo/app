/* eslint-disable prettier/prettier */
import React from 'react'
import { View, Text } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import Toast from 'react-native-root-toast'
import { Picker } from '@react-native-community/picker'

import { Sintoma } from './sintoma'
import { STYLES, WIDTH} from '../styles'
import { H1, H2 } from '../pseudo_html'
import { Cajon } from '../cajon'
import { I18n } from '../i18n'
import { Logger } from '../logger'

/*
 * Vacunacion
 */
export class Vacunacion extends Sintoma {
  constructor (props) {
    super(props)

    this.state = {
      fase: this.props.fase,
      ciclo: this.props.ciclo,
      encuesta: this.props.encuesta,
      acciones: this.props.acciones.filter(item => item !== 'vacunacion'),
    }

    if (!this.state.encuesta.marca_vacuna) this.state.encuesta.marca_vacuna = ''
    if (!this.state.encuesta.dosis) this.state.encuesta.dosis = ''
    if (!this.state.encuesta.pais) this.state.encuesta.pais = ''

    Logger.log('Vacunacion', this.state.encuesta)
  }

  validar_datos () {
    if (this.state.encuesta.marca_vacuna.length > 0
        && this.state.encuesta.pais.length > 0
        && this.state.encuesta.dosis.length > 0) {

        this.continuar_encuesta()
        return
    }

    Toast.show(I18n.t('vacunacion.incompleto'))
  }

  render () {
    return (
      <Cajon back={true}>
        <View style={[STYLES.container, { justifyContent: 'flex-start' }]}>
          <View style={STYLES.center}>
            <H1>{I18n.t('vacunacion.title')}</H1>
          </View>

          <View style={{ alignItems: 'center', marginTop: 10 }}>
                <H2>{I18n.t('vacunacion.marca_vacuna')}</H2>

                <Picker
                  style={{ width: 250, marginTop: -20 }}
                  itemStyle={{height: 110}}
                  selectedValue={this.state.encuesta.marca_vacuna}
                  onValueChange={(value, index) => this.agregar_a_encuesta('marca_vacuna', value)}>
                  {I18n.t('vacunacion.marcas_vacuna').map(i => {
                    return (<Picker.Item key={i.key} label={i.name} value={i.key} />)
                  })}
                </Picker>
          </View>

          <View style={{ alignItems: 'center', marginTop: 10 }}>
                <H2>{I18n.t('vacunacion.dosis_a_registrar')}</H2>

                <Picker
                  style={{ width: 250, marginTop: -20}}
                  itemStyle={{height: 110}}
                  selectedValue={this.state.encuesta.dosis}
                  onValueChange={(value, index) => this.agregar_a_encuesta('dosis', value)}>
                  {I18n.t('vacunacion.dosis').map(i => {
                    return (<Picker.Item key={i.key} label={i.name} value={i.key} />)
                  })}
                </Picker>
          </View>

          <View style={{ alignItems: 'center', marginTop: 10 }}>
                <H2>{I18n.t('vacunacion.pais_de_vacunacion')}</H2>

                <Picker
                  style={{ width: 250, marginTop: -20}}
                  itemStyle={{height: 110}}
                  selectedValue={this.state.encuesta.pais}
                  onValueChange={(value, index) => this.agregar_a_encuesta('pais', value)}>
                  {I18n.t('vacunacion.paises').map(i => {
                    return (<Picker.Item key={i.code} label={i.name} value={i.code} />)
                  })}
                </Picker>
          </View>

          <View style={{paddingHorizontal: 20, marginTop: 5}}>
            <Text style={{fontSize: 13}}>{I18n.t('vacunacion.nota_sobre_vacunacion')}</Text>
          </View>

          <View style={{ width: WIDTH, padding: 5, margin: 5, flex: 1, justifyContent: 'flex-end' }}>
            <Icon.Button
              name="check"
              style={{ backgroundColor: this.state.fase.color }}
              onPress={() => this.validar_datos()}>
              <Text style={STYLES.boton}>{I18n.t('vacunacion.continuar')}</Text>
            </Icon.Button>
          </View>
        </View>
      </Cajon>
    )
  }
}
