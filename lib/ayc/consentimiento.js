import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { ScrollView, View, Linking } from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions } from 'react-native-router-flux'

import realm from '../models'
import { STYLES, WIDTH } from '../styles'
import { H1, Paragraph } from '../pseudo_html'
import { Cajon } from '../cajon'
import { Helper } from '../helper'
import { Logger } from '../logger'
import { I18n } from '../i18n'
import { PseudoMarkdown } from '../pseudo_markdown'

/*
 * Pregunta a la usuaria si quiere registrarse
 */
export class PedirConsentimiento extends Component {
  static propTypes = {
    fase: PropTypes.object.isRequired,
    ciclo: PropTypes.object.isRequired,
    encuesta: PropTypes.object,
    volver_a: PropTypes.string
  }

  static defaultProps = {
    volver_a: 'como_te_sentis'
  }

  constructor (props) {
    super(props)

    Logger.log('AYC.Consentimiento', 'ingresar')
  }

  /*
   * Acepta o rechaza el consentimiento
   */
  aceptar_o_rechazar (estado = false) {
    Logger.log('AYC.Consentimiento', `aceptar_o_rechazar: ${estado}`)

    // Si rechazamos el consentimiento, nos tenemos que registrar
    // internamente (?)
    if (estado) {
      this.registrar_usuaria()
    } else {
      this.no_registrar_usuaria()
    }
  }

  /*
   * Eligió no registrarse
   */
  no_registrar_usuaria () {
    Logger.log('AYC.Consentimiento', 'no_registrar_usuaria')

    const usuaria = Helper.usuaria()
    const props = {
      fase: this.props.fase,
      ciclo: this.props.ciclo
    }

    realm.write(() => usuaria.remote_id = 'no')

    if (this.props.volver_a === 'como_te_sentis')
      props.encuesta = this.props.encuesta

    // Ir a donde íbamos
    Actions.replace(this.props.volver_a, props)
  }

  /*
   * Ir a la pantalla de registro
   */
  registrar_usuaria () {
    Logger.log('AYC.Consentimiento', 'registrar_usuaria')
    Actions.replace('registro', {
      fase: this.props.fase,
      ciclo: this.props.ciclo,
      volver_a: this.props.volver_a
    })
  }

  render () {
    return (
      <Cajon back={true}>
        <ScrollView contentContainerStyle={[STYLES.non_view_container, {paddingLeft: WIDTH * 0.1, paddingRight: WIDTH*0.1 }]}>
          <H1>{I18n.t('consentimiento.title')}</H1>

          {I18n.t('consentimiento.texto').map((parrafo, i) => {
            const render = PseudoMarkdown.render(this.props.fase, parrafo)
            return (<Paragraph key={`parrafo_${i}`}>{render}</Paragraph>)
          })}

          <View style={{ marginBottom: 5 }}>
            <Icon.Button name="check"
              backgroundColor={this.props.fase.color}
              onPress={() => this.aceptar_o_rechazar(true)}>
              {I18n.t('consentimiento.aceptar')}
            </Icon.Button>
          </View>

          <View style={{ marginBottom: 5 }}>
            <Icon.Button name="close"
              backgroundColor={this.props.fase.color}
              onPress={() => this.aceptar_o_rechazar(false)}>
              {I18n.t('consentimiento.rechazar')}
            </Icon.Button>
          </View>

          <View style={{ marginBottom: 5 }}>
            <Icon.Button name="user-secret"
              backgroundColor={this.props.fase.color}
              onPress={() => Linking.openURL('http://www.lunarcomunidad.com/politica-de-privacidad-de-la-app')}>
              {I18n.t('consentimiento.privacidad')}
            </Icon.Button>
          </View>
        </ScrollView>
      </Cajon>
    )
  }
}
