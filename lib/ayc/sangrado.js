import React from 'react'
import { View, FlatList } from 'react-native'
import { Actions } from 'react-native-router-flux'
import Slider from '@react-native-community/slider'
import Icon from 'react-native-vector-icons/FontAwesome'
import CheckBox from 'react-native-check-box'

import { Sintoma } from './sintoma'
import { STYLES, WIDTH } from '../styles'
import { H1, Paragraph, Small, BotonText } from '../pseudo_html'
import { Cajon } from '../cajon'
import { Bleeding } from '../svg_elements'
import { I18n } from '../i18n'
import { Logger } from '../logger'

/*
 * Sangrado
 */
export class Sangrado extends Sintoma {
  constructor (props) {
    super(props)

    this.state = {
      encuesta: this.props.encuesta,
      fase: this.props.fase,
      ciclo: this.props.ciclo,
      disabled: { yes: false, no: true },
      show_hours: false,
      acciones: this.props.acciones.filter(item => item !== 'sangrado')
    }

    if (this.state.encuesta.nivel_de_sangrado === undefined) { this.state.encuesta.nivel_de_sangrado = 0 }

    if (this.state.encuesta.productos_utilizados === undefined) { this.state.encuesta.productos_utilizados = [] }

    Logger.log('Sangrado', this.state.encuesta)
  }

  /*
   * Agregar o quitar productos
   */
  guardar_o_quitar_productos (producto, estado = true) {
    let productos = this.state.encuesta.productos_utilizados.slice(0)

    if (estado) {
      if (!this.incluido_en_productos(producto)) productos.push(producto)
    } else {
      productos = productos.filter(x => x !== producto)
    }

    this.agregar_a_encuesta('productos_utilizados', productos)
  }

  /*
   * Verifica si el producto ya está incluido
   */
  incluido_en_productos (producto) {
    return this.state.encuesta.productos_utilizados.includes(producto.key)
  }

  /*
   * Genera síntomas individuales
   */
  render_item (item) {
    return (
      <CheckBox
        rightTextView={(<View style={{marginLeft: 5}}><Paragraph>{item.name}</Paragraph></View>)}
        isChecked={this.incluido_en_productos(item)}
        uncheckedCheckBoxColor="#8F0E16"
        checkedCheckBoxColor="#8F0E16"
        onClick={() => this.guardar_o_quitar_productos(item.key, !this.incluido_en_productos(item))} />
    )
  }

  render () {
    return (
      <Cajon back={true}>
        <View style={[STYLES.container, { justifyContent: 'flex-start' }]}>
          <View style={STYLES.center}>
            <H1>{I18n.t('sangrado.title')}</H1>

            <View style={{ flexDirection: 'row', width: WIDTH - 20, justifyContent: 'space-between', alignItems: 'center', marginTop: 30 }}>
              <Bleeding width="20" height="20" />
              <Paragraph><Small>{I18n.t('sangrado.promedio')}</Small></Paragraph>
              <Bleeding width="30" height="30" />
            </View>

            <Slider
              value={this.state.encuesta.nivel_de_sangrado}
              style={{ width: WIDTH - 20, marginBottom: 30 }}
              minimumValue={-1}
              maximumValue={1}
              step={1}
              thumbTintColor="#8F0E16"
              maximumTrackTintColor="#8F0E16"
              minimumTrackTintColor="#8F0E16"
              onValueChange={nivel => this.agregar_a_encuesta('nivel_de_sangrado', nivel)} />

            <H1>{I18n.t('sangrado.productos')}</H1>

            <FlatList
              style={{ width: WIDTH - 20, marginTop: 20 }}
              keyExtractor={(item, index) => item.key}
              data={I18n.t('sangrado.productos_utilizados')}
              renderItem={({ item }) => this.render_item(item) } />

            <View style={{ width: WIDTH, padding: 5, margin: 5, flex: 1, justifyContent: 'flex-end' }}>
              <Icon.Button
                name="check"
                style={{ backgroundColor: this.state.fase.color }}
                onPress={() => this.continuar_encuesta()}>
                <BotonText>{I18n.t('sangrado.continuar')}</BotonText>
              </Icon.Button>
            </View>
          </View>
        </View>
      </Cajon>
    )
  }
}
