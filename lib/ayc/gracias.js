import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { View, ImageBackground } from 'react-native'
import { Actions } from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/FontAwesome'

import realm from '../models'
import { STYLES, WIDTH } from '../styles'
import { H1, Paragraph, BotonText } from '../pseudo_html'
import { Cajon } from '../cajon'
import { I18n } from '../i18n'
import { Logger } from '../logger'
import { Helper } from '../helper'

/*
 * Guarda las encuestas en la base de datos y agradece a la usuaria.
 * Recibe la encuesta desde síntomas.
 */
export class Gracias extends Component {
  static propTypes = {
    fase: PropTypes.object.isRequired,
    ciclo: PropTypes.object.isRequired,
    encuesta: PropTypes.object.isRequired
  }

  constructor (props) {
    super(props)

    this.state = {
      fase: this.props.fase,
      ciclo: this.props.ciclo,
      encuesta: this.props.encuesta
    }

    Logger.log('Gracias', this.state.encuesta)
  }

  /*
   * Al cargar la vista guardar los datos, completando lo que haga falta
   */
  componentDidMount () {
    const data = this.state.encuesta
    data.ciclo = this.state.ciclo

    // Eliminar los datos si eliminamos el síntoma
    if (!data.sintomas.includes('dolor')) {
      data.dolores = []
      data.intensidad_del_dolor = 0
    }

    if (!data.sintomas.includes('sangrado')) data.nivel_de_sangrado = 0

    if (!data.sintomas.includes('flujo_vaginal')) {
      data.color_del_flujo_vaginal = ''
      data.olor_del_flujo_vaginal = false
      data.cantidad_del_flujo_vaginal = 0
      data.textura_del_flujo = ''
    }

    if (!data.sintomas.includes('perdidas')) {
      data.sangrado_durante_o_despues_del_sexo = false
      data.duracion_del_sangrado = 0
    }

    // TODO: Eliminar al sacar síntomas y dolores como modelos
    data.sintomas = data.sintomas.map(s => ({ nombre: s }))
    data.dolores = data.dolores.map(d => ({ parte_del_cuerpo: d }))
    data.updated_at = new Date()

    // TODO: Convertir a UUID
    if (data.id === undefined) data.id = Date.now()

    // Crear o actualizar
    realm.write(() => realm.create('Encuesta', data, true))
  }

  render () {
    return (
      <Cajon back={true}>
        <View style={[STYLES.container, STYLES.main]}>
          <ImageBackground
            resizeMode="contain"
            source= {{uri: 'https://panel.lunarcomunidad.com/ios/espiral_transparente.png'}}
            style={{ width: WIDTH * 0.8, height: WIDTH * 0.8 }}>
            <View style={STYLES.aspecto_body}>
              <H1>{I18n.t('gracias.title')}</H1>

              <Paragraph>{I18n.t('gracias.descripcion')}</Paragraph>

              <Icon.Button
                name="caret-left"
                style={{ backgroundColor: this.state.fase.color }}
                onPress={() => Actions.popTo('espiral')}>
                <BotonText>{I18n.t('gracias.volver')}</BotonText>
              </Icon.Button>
            </View>
          </ImageBackground>
        </View>
      </Cajon>
    )
  }
}
