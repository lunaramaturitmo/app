'use strict'

import * as Sentry from '@sentry/react-native'

import React, { DeviceEventEmitter, Platform } from 'react-native'
import PushNotification from 'react-native-push-notification'
import m from 'moment'
import shuffle from 'shuffle-array'
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import analytics from '@react-native-firebase/analytics'

import realm from './models'

import { Helper } from './helper'
import { Logger } from './logger'
import { I18n } from './i18n'

/*
 * Gestiona y agenda notificaciones
 */
export class Notificaciones {
  /*
   * IDs para las notificaciones
   */
  static NO_ME_VINO = '2000'
  /*
   * Nombre del canal de notificaciones para Android, si modificamos
   * createChannel hay que cambiar el nombre.
   */
  static CHANNEL    = 'Lunar'

  /*
   * Realiza una sincronización completa de las notificaciones, para
   * eliminar las que quedaron locales y ya no existen en el backend.
   */
  static sincronizacion_completa (notificaciones) {
    const remote_ids = notificaciones.data.map(n => n.id)
    const local_ids = realm.objects('Notificacion').map(n => n.id)
    const remove = local_ids.filter(x => !remote_ids.includes(x))

    for (const id of remove) {
      realm.write(() => realm.delete(realm.objectForPrimaryKey('Notificacion', id)))

      analytics().logEvent('lunar_notification_extra_eliminada', { id })
    }
  }

  /*
   * Pide permisos a la usuaria para poder enviar notificaciones
   */
  static configurar (callback, bind) {
    Logger.log('Notificaciones.configurar', 'Configurando')

    // En iOS hay que asegurarse que tenemos los permisos para enviar
    // notificaciones
    PushNotification.configure({
      onNotification: notification => {
        callback(notification, bind)

        if (Platform.OS === 'ios')
          notification.finish(PushNotificationIOS.FetchResult.NoData)
      },
      requestPermissions: (Platform.OS === 'ios')
    })

    // TODO: avisarle a la usuaria que sin notificaciones se pierde la
    // mayor parte de la app
    if (Platform.OS === 'ios') {
      PushNotification.requestPermissions().then(res => {
        analytics().logEvent('lunar_notification_ios', { status: res })
        return res.alert
      })
    }

    PushNotification.channelExists(this.CHANNEL, exists => {
      if (exists) return

      PushNotification.createChannel({
        channelId: this.CHANNEL,
        channelName: "Lunar",
        channelDescription: "Notificaciones de Lunar"
      }, created => {
        analytics().logEvent('lunar_notification_channel_configured')
      })
    })

    PushNotification.channelBlocked(this.CHANNEL, blocked => {
      if (!blocked) return
      analytics().logEvent('lunar_notification_channel_blocked')
    })
  }

  /*
   * Genera una notificacion local con todas las opciones de
   * react-native-push-notification
   *
   * Las notificaciones tienen que verse en el panel del dispositivo y
   * solo se cancelan al ingresar a la App y verlas completas.
   *
   * @param [String]
   * @param [String]
   * @param [Date]
   */
  static schedule (id, contenido, date) {
    if (contenido === null) {
      Logger.log('Notificaciones.contenido_vacio', id)
      return false
    }

    if (date.getTime() < Date.now()) {
      Logger.log('Notificaciones.no_enviar_al_pasado', date.toString())
      return false
    }

    // Eliminar caracteres de IDs antiguos e ir borrando notificaciones
    // rotas
    if (id.replace(/[0-9]/g, '').length > 0) {
      realm.write(() => {
        let n = realm.objectForPrimaryKey('Notificacion', id)
        if (n) realm.delete(n)
      })

      id = id.replace(/[a-z]/g, '').slice(0,8)
    }

    // La notificación tiene que ser un Integer
    id = parseInt(id)

    const _notificacion = {
      channelId: this.CHANNEL,
      id,
      userInfo: { id, contenido },
      category: 'reminder',
      title: 'Lunar',
      message: contenido,
      bigText: contenido,
      ticker: contenido,
      date,
      allowWhileIdle: true,
      autoCancel: true,
      ongoing: true,
      ignoreInForeground: false
    }

    Logger.log('Notificaciones.crear', _notificacion)

    try {
      PushNotification.localNotificationSchedule(_notificacion)
      analytics().logEvent('lunar_notification_scheduled', { id })
    } catch (e) {
      Sentry.captureException(e)
    }

    return true
  }

  /*
   * Crea las notificaciones en la base de datos y las modifica o
   * elimina si es necesario..
   *
   * El parámetro es el objeto de notificaciones devuelto por la API (o
   * el JSON incorporado).
   */
  static crear (notificaciones) {
    if (!notificaciones || !notificaciones.data) return

    let action = ''

    // Recorrer todas las notificaciones
    for (const n of notificaciones.data) {
      const id = n.id
      const guardada = realm.objectForPrimaryKey('Notificacion', id)
      const notificacion = n.attributes

      // No crear si existe, no usamos la actualización inteligente de
      // realm porque cambiaría notificaciones en uso a sin usar
      if (guardada) {
        if (notificacion.deleted) {
          Logger.log('Notificaciones.crear', `Eliminando ${id}`)
          realm.write(() => realm.delete(guardada))

          action = 'removed'
        } else {
          // Actualizarlas si hubo algún cambio en el contenido, aunque no
          // se va a ver hasta que se reprogramen...
          Logger.log('Notificaciones.crear', `Actualizando ${id}`)

          realm.write(() => {
            guardada.texto = notificacion.contenido
            guardada.etapa = notificacion.etapa
            guardada.run_at = (notificacion.run_at_time || '')
          })

          action = 'updated'
        }
      } else {
        // Si está borrada ni siquiera crearla y cancelarla si ya estaba
        // cargada.
        if (notificacion.deleted) {
          Logger.log('Notificaciones.crear', `Ignorando ${id}`)
          if (notificacion.run_at_date) Notificaciones.cancelar(id)

          continue
        }

        // Si la notificación está programada para una fecha no hace
        // falta crearla en la base de datos, la programamos
        // inmediatamente
        if (notificacion.run_at_date) {
          let run_at_date = notificacion.run_at_date
          run_at_date += ' '
          run_at_date += (notificacion.run_at_time || '12:00')
          run_at_date = m(run_at_date).toDate()

          // No enviar notificaciones pasadas
          if (run_at_date.getTime() < (new Date()).getTime()) return

          Logger.log('Notificaciones.crear',
            `Notificacion agendada para ${run_at_date}`)

          // Cancelar antes de volver a cargar
          Notificaciones.schedule(id, notificacion.contenido, run_at_date)
          action = 'programmed'
        } else {
          // Crear o actualizar la notificación en la base de datos
          const _notificacion = {
            id: id,
            texto: notificacion.contenido,
            en_uso: false,
            etapa: notificacion.etapa,
            run_at: (notificacion.run_at_time || '')
          }

          Logger.log('Notificaciones.crear', _notificacion)
          realm.write(() => realm.create('Notificacion', _notificacion, 'all'))
          action = 'created'
        }
      }

      // Informar la acción
      analytics().logEvent(`lunar_notification_${action}`, { id })
    }
  }

  /*
   * Cancelar todas las notificaciones
   */
  static vaciar () {
    Logger.log('Notificaciones.vaciar', 'Vaciando')
    PushNotification.cancelAllLocalNotifications()
  }

  /*
   * Cancelar una notificación individual
   */
  static cancelar (id) {
    PushNotification.cancelLocalNotifications({ id })
  }

  static no_me_vino (fecha) {
    const date = m(fecha).startOf('day').add(10, 'days').add(12, 'hours').toDate()

    Logger.log('Notificaciones.no_me_vino', date)

    Notificaciones.schedule(Notificaciones.NO_ME_VINO,
      I18n.t('notificaciones.no_me_vino'),
      date)
  }

  /*
   * Agendar notificaciones periódicas teniendo en cuenta el ciclo y
   * periodicidad seleccionada
   */
  static periodicas (cada_cuanto, ciclo) {
    Logger.log('Notificaciones.periodicas', 'Configurando')
    // XXX Sacar la periodicidad en dias no nos permite sacar la
    // periocidad en cambios de fase.
    // TODO cuando agreguemos el calendario vamos a tener lógica para
    // saber en que fecha viene cada fase
    let periodicidad = 0
    switch (cada_cuanto) {
      case 'cada_3_dias':
        periodicidad++
      case 'dia_por_medio':
        periodicidad++
      case 'diaria':
        periodicidad++
        break
      case 'solo_en_cambio':
        periodicidad = 0
        break
    }

    Logger.log('Notificaciones.periodicas', { cada_cuanto: cada_cuanto, periodicidad: periodicidad })
    analytics().logEvent('lunar_notification_periodicity', { periodicity: cada_cuanto })

    // Siempre trabajamos sobre el mediodía
    const hoy = m(Helper.today()).add(12, 'hours').toDate()

    // Obtener todas las etapas según las fases, porque pueden estar
    // traducidas!
    realm.objects('Fase').forEach(fase => {
      Logger.log('Notificacion.periodicas', fase.etapa)
      const duracion = ciclo.duracion_para_etapa_segun_fase(fase.id)
      // No programar notificaciones cuando no hay
      if (duracion === 0) return;

      const from = m(ciclo.fecha_para_etapa_segun_fase(fase.id)).startOf('day').toDate()
      // TODO esto lo tiene que saber el ciclo no la notificacion
      const to = m(from).add(duracion - 1, 'days').toDate()
      // La fecha inicial
      let send_at = m(from).add(12, 'hours').toDate()

      // solo queremos una notificación a menos que le demos
      // periodicidad
      let cantidad = 1
      if (periodicidad > 0) cantidad = Math.floor(duracion / periodicidad)

      // Obtener todas las notificaciones de esta etapa que no estén en
      // uso
      const notificaciones = realm.objects('Notificacion')
        .filtered('etapa == $0 && en_uso == $1', fase.etapa, false)

      Logger.log('Notificaciones.periodicas', {
        from: from,
        to: to,
        duracion: duracion,
        cantidad: cantidad
      })

      if (cantidad === 0) return

      // Se nos terminaron las notificaciones!
      if (notificaciones.length < cantidad) {
        Logger.log('Notificaciones.periodicas', 'Se terminaron las notificaciones')
        realm.write(() => {
          realm.objects('Notificacion')
            .filtered('etapa == $0', fase.etapa)
            .map(n => n.en_uso = false)
        })
      }

      // Convertir los resultados a un array para poder randomizarlos
      // Randomizar las notificaciones
      const notificaciones_random = [shuffle.pick(notificaciones.map(n => n), { picks: cantidad })].flat()

      Logger.log('Notificaciones.periodicas', `Encontradas ${notificaciones_random.length}`)
      notificaciones_random.forEach(notificacion => {
        Logger.log('Notificaciones.periodicas', notificacion)

        // Si la periodicidad es 0, quiere decir que solo mandamos una
        // notificacion en cambio de etapa
        if (periodicidad > 0) {
          send_at = m(send_at).add(periodicidad, 'days').toDate()
        }

        // No enviar notificaciones al pasado
        if (send_at.getTime() < hoy.getTime()) {
          Logger.log('Notificaciones.periodicas', 'No enviar notificaciones al pasado')
          return
        }

        let _send_at = send_at

        // Si la notificación tiene una hora específica, programarla
        // para esa hora a menos que ya haya pasado
        if (notificacion.run_at && notificacion.run_at.length > 0) {
          Logger.log('Notificaciones.periodicas', `Programar para las ${notificacion.run_at}`)
          _send_at = m(m(send_at).format('YYYY-MM-DD') + ' ' + notificacion.run_at).toDate()
        }

        // Si la notificacion toca hoy, que sea un poco mas tarde
        if (_send_at.getTime() <= hoy.getTime()) {
          Logger.log('Notificaciones.periodicas', 'Enviar dentro de una hora')
          _send_at = m().add(1, 'hour').toDate()

          if (notificacion.run_at) return
        }

        // No colocar notificaciones fuera del ciclo
        if (send_at.getTime() > to.getTime()) {
          Logger.log('Notificaciones.periodicas', 'No enviar fuera del ciclo')
          return
        }

        Notificaciones.schedule(notificacion.id, notificacion.texto, _send_at)
        realm.write(() => notificacion.en_uso = true)
      })
    })
  }
}
