import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native'
import { Actions } from 'react-native-router-flux'
import Svg, { Rect } from 'react-native-svg'

import realm from './models'
import { STYLES } from './styles'
import { Cajon } from './cajon'
import { LunaNueva, LunaCreciente, LunaLlena, LunaMenguante, BarraDeAspectos } from './svg_elements'
import { FaseH1, FaseH2 } from './pseudo_html'
import { I18n } from './i18n'

/*
 * La biblioteca de aspectos
 */
export class Biblioteca extends Component {
  constructor () {
    super()

    this.state = {
      fases: realm.objects('Fase').sorted('id'),
      lunas: [null]
    }

    this.state.fases.forEach(fase => {
      let luna

      switch (fase.id) {
        case 1:
          luna = (<LunaNueva scale="0.8" color={fase.color} />)
          break
        case 2:
          luna = (<LunaCreciente scale="0.8" color={fase.color} />)
          break
        case 3:
          luna = (<LunaLlena scale="0.8" color={fase.color} />)
          break
        case 4:
          luna = (<LunaMenguante scale="0.8" color={fase.color} />)
          break
      }

      this.state.lunas.push(luna)
    })
  }

  render () {
    return (
      <Cajon>
        <View style={{ flex: 1, backgroundColor: '#f0ede9', paddingTop: 30, paddingBottom: 10, alignItems: 'center', justifyContent: 'space-between' }}>
          {this.state.fases.map((fase, i) => {
            return (<View key={`fase-${i}`} style={STYLES.barra_de_aspectos_exterior}>
              <View style={STYLES.barra_de_aspectos_titulo}>
                {this.state.lunas[fase.id]}
                <FaseH1>{I18n.t('biblioteca.fase').toUpperCase()} {fase.id}. </FaseH1>
                <FaseH2>{fase.nombre}</FaseH2>
              </View>

              <BarraDeAspectos fase={fase} />
            </View>)
          })}
        </View>
      </Cajon>
    )
  }
}
