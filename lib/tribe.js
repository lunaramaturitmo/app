import { PANEL } from './const'

import PropTypes from 'prop-types'
import React, { Component } from 'react'

import { Platform, SafeAreaView, Text, TouchableOpacity, BackHandler } from 'react-native'
import { WebView } from 'react-native-webview'
import { Actions } from 'react-native-router-flux'

import DeviceInfo from 'react-native-device-info'

import realm from './models'
import { Backend } from './backend'
import { Helper } from './helper'
import { Logger } from './logger'
import { Loader } from './loader'
import {STYLES} from './styles'
import { I18n } from './i18n'

/*
 * Tribe
 */
export class Tribe extends Component {
  static propTypes = {
    fase: PropTypes.object.isRequired,
    ciclo: PropTypes.object.isRequired
  }

  /*
   * Cargamos una URL especial del backend que realiza el login de la
   * usuaria y la redirige al login OAuth2 de Tribe, que le va a pedir
   * autorización para acceder a su correo y nombre.  Apuntamos a que
   * tenga que ser la única acción manual de la usuaria y que no tenga
   * que iniciar sesión.
   */
  constructor (props) {
    super(props)

    this.state = {
      url: `${PANEL}/tribe/`,
      usuaria: Helper.usuaria(),
      cargando: true,
    }

    this.headers = { Authorization: `Basic ${Backend.authorization(this.state.usuaria)}` }
    this.state.auth = this.headers
    this.WEBVIEW_REF = React.createRef()
  }

  /*
   * Controlar loader
   */
  cambiarEstadoLoader() {
    this.setState({cargando: false})
  }

  componentDidMount () {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton = () => {
    if (this.state.canGoBack) {
      this.WEBVIEW_REF.current.goBack()
    } else {
      Actions.pop()
    }

    return true;
  };

  handleWebViewNavigationStateChange(navState) {
    this.setState({ canGoBack: navState.canGoBack });
  }

  /*
   * XXX: Copiado de lib/registro.js
   */
  user_agent () {
    let ua = DeviceInfo.getUserAgentSync().replace(/; *wv/, '')

    if (Platform.OS === 'ios') ua = ua + ' Safari/604'

    return ua
  }

  /*
   * Para poder iniciar la sesión de la usuaria siempre tenemos que
   * pasar la usuaria y contraseña en las URLs del panel.  Con esta
   * función que llama WebView, si la URL pertenece al panel, recarga el
   * estado para que la WebView vuelva a cargar la URL con los headers
   * correspondientes.
   *
   * Según la documentación los headers se envían solo en la primera
   * visita pero descubrimos que se cargan en todas las visitas,
   * entonces tenemos que decidir a cuáles URLs les damos credenciales o
   * no.
   *
   * https://github.com/react-native-webview/react-native-webview/blob/v10.9.3/docs/Guide.md#setting-custom-headers
   */
  agregar_la_autenticacion (request) {
    const new_state = { url: request.url, auth: {} }

    if (request.url.startsWith(PANEL)) new_state.auth = this.headers

    // Cambiar el estado
    this.setState(new_state)

    // Detener la carga
    return false
  }

  render () {
    return (
      <SafeAreaView style={[STYLES.android_safe_area, {flex: 1}]}>
        <TouchableOpacity
          style={{padding: 10}}
          onPress={() => Actions.pop()}>
          <Text style={{fontSize: 18}}>◀ {I18n.t('gracias.volver')}</Text>
        </TouchableOpacity>

        <Loader cargando={this.state.cargando}/>

        <WebView
          style={{ flex: 1 }}
          onMessage={event => {}}
          onError={error => Logger.log('Tribe', error)}
          cacheEnabled={false}
          userAgent={this.user_agent()}
          useWebKit={true}
          allowsLinkPreview={false}
          javaScriptEnabled={true}
          source={{ uri: this.state.url, headers: this.state.auth }}
          onShouldStartLoadWithRequest={r => this.agregar_la_autenticacion(r)}
          ref={this.WEBVIEW_REF}
          onNavigationStateChange={n => this.handleWebViewNavigationStateChange(n)}
          onLoad={() => this.cambiarEstadoLoader()}
        />
      </SafeAreaView>
    )
  }
}
