import PropTypes from 'prop-types'
import React, { Component } from 'react'
import ReactNative, { View, TextInput } from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'
import ProgressBarAnimated from 'react-native-progress-bar-animated'
import m from 'moment'

import { Cajon } from './cajon'
import { Paragraph, H1, Resaltado, Small } from './pseudo_html'
import { STYLES, WIDTH } from './styles'

import realm from './models'

import { Helper } from './helper'
import { Backend } from './backend'
import { Logger } from './logger'
import { I18n } from './i18n'

/*
 * La recuperación de datos solicita la dirección de correo y contraseña
 * de recuperación.
 *
 * Obtiene el backup de la API, lo decifra y lo procesa, recuperando los
 * datos, incluyendo la contraseña anterior para envío de información.
 */
export class RecuperarDatos extends Component {
  constructor (props) {
    super(props)

    this.state = {
      password_para_backup: '',
      email: '',
      message: '',
      progress: 0,
      max: 100
    }

    Logger.log('RecuperarDatos', 'constructor');
  }

  /* Necesitamos `tocar` los ajustes para poder volver a mostrar la
   * pantalla de bienvenida al volver a la espiral si estamos
   * cancelando.
   */
  componentWillUnmount () {
    const ajustes = Helper.ajustes()
    const ayer = m().startOf('day').add(-1, 'days').toDate()
    realm.write(() => ajustes.sincronizacion = ayer)
  }

  /*
   * Iniciar la recuperación de datos
   */
  recuperar_datos () {
    if (this.state.password_para_backup.length !== 8) return
    if (this.state.email.length === 0) return

    this.setState({ message: '', progress: 0 })

    Logger.log('RecuperarDatos', 'recuperar_datos');

    Backend.recuperar_datos(this.state.email,
      this.state.password_para_backup,
      (message, progress) => {
        this.setState({
          message: message,
          progress: progress
        })

        Logger.log('Backend.recuperar_datos', message, progress)
      })
  }

  render () {
    return (<Cajon back={true}>
      <View style={[STYLES.container, STYLES.main]}>
        <View style={{ width: WIDTH * 0.9 }}>
          <Paragraph>{I18n.t('recuperar_datos.introduccion')}</Paragraph>
        </View>

        <TextInput
          placeholder={I18n.t('recuperar_datos.email')}
          keyboardType="email-address"
          autoCapitalize="none"
          placeholderTextColor="grey"
          value={this.state.email}
          selectionColor="#00e3ac"
          underlineColorAndroid="#f0ede9"
          onChangeText={text => this.setState({ email: text || '' })}
          style={[STYLES.nota_input, { borderColor: '#00e3ac' }]}/>

        <TextInput
          placeholder={I18n.t('recuperar_datos.password')}
          autoCapitalize="none"
          placeholderTextColor="grey"
          value={this.state.password_para_backup}
          selectionColor="#00e3ac"
          underlineColorAndroid="#f0ede9"
          onChangeText={text => this.setState({ password_para_backup: text || '' })}
          style={[STYLES.nota_input, { borderColor: '#00e3ac' }]}/>

        <Icon.Button
          name="download"
          backgroundColor="#00d0ff"
          style={{ width: WIDTH * 0.9 }}
          enabled={() => this.state.progress === 0}
          onPress={() => this.recuperar_datos()}>
          {I18n.t('recuperar_datos.recuperar')}</Icon.Button>

        <View style={{ marginTop: 5 }}>
          <ProgressBarAnimated
            width={WIDTH * 0.9}
            value={this.state.progress}
            maxValue={this.state.max} />

          <Paragraph><Small>{this.state.message}</Small></Paragraph>
        </View>

      </View>
    </Cajon>)
  }
}
