import { PANEL } from './const'

import PropTypes from 'prop-types'
import React, { Component } from 'react'

import DeviceInfo from 'react-native-device-info'

import { Platform } from 'react-native'
import { WebView } from 'react-native-webview'
import { Actions } from 'react-native-router-flux'

import realm from './models'

import { Helper } from './helper'
import { Logger } from './logger'
import { I18n } from './i18n'
import { Random } from './random'

/*
 * Esta clase realiza el registro cargándolo desde el panel de
 * administración.  Cuando recibe el mensaje de registro correcto, toma
 * la contraseña, la almacena y continua con lo que estaba haciendo.
 */
export class Registro extends Component {
  static propTypes = {
    ciclo: PropTypes.object.isRequired,
    volver_a: PropTypes.string
  }

  static defaultProps = {
    volver_a: 'como_te_sentis'
  }

  /*
   * Un UserAgent al que le eliminamos lo que lo identifica como un
   * navegador incorporado.
   *
   * XXX: Mantener sincronizado con lib/tienda.js
   */
  user_agent () {
    let ua = DeviceInfo.getUserAgentSync().replace(/; *wv/, '')

    if (Platform.OS === 'ios') ua = ua + ' Safari/604'

    return ua
  }

  /*
   * Termina el registro guardando los datos recibidos y siguiendo con
   * lo que estábamos haciendo
   */
  terminar_registro (data) {
    // El servidor nos envía un JSON stringificado
    data = JSON.parse(data)

    Logger.log('Registro.terminar_registro', `Registrada ${data.email}`)

    // El servidor puede decir dos cosas, que la usuaria terminó el
    // registro y luego que tocó el botón de cerrar.  Hacemos esto en
    // dos pasos porque no todas encuentran el botón.
    if (data.close) {
      // Ir a la pantalla que pidió la usuaria
      Actions.replace(this.props.volver_a, {
        ciclo: this.props.ciclo,
        fase: realm.objectForPrimaryKey('Fase', this.props.ciclo.calcular_fase())
      })
    } else {
      // Terminar el registro
      const usuaria = Helper.usuaria()
      // TODO: Eliminar consentimientos
      const consentimiento = Helper.consentimiento()

      realm.write(() => {
        usuaria.email = data.email
        usuaria.registrada = new Date()
        // La contraseña es generada por el backend
        usuaria.password = data.password
        // TODO: Reemplazar por UUID
        usuaria.remote_id = data.id.toString()
        usuaria.password_para_backup = Random.generar_password(8)

        consentimiento.fecha = new Date()
        consentimiento.estado = true
      })
    }
  }

  /*
   * La URL a visitar, nos identifica como la app y le pide una
   * traducción al backend.
   */
  uri () {
    return `${PANEL}/auth/usuarias/sign_up?locale=${I18n.current_short_locale}&app&${Platform.OS}`
  }

  /*
   * Mostramos un navegador a pantalla completa
   */
  render () {
    return (
      <WebView
        style={{ flex: 1 }}
        onMessage={event => this.terminar_registro(event.nativeEvent.data)}
        onError={error => Logger.log('Registro', error)}
        cacheEnabled={false}
        userAgent={this.user_agent()}
        useWebKit={true}
        allowsLinkPreview={false}
        javaScriptEnabled={true}
        thirdPartyCookiesEnabled={false}
        sharedCookiesEnabled={false}
        source={{ uri: this.uri() }}
      />
    )
  }
}
