import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { View, Modal, Image } from 'react-native'
import { Actions } from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/FontAwesome'

import { H1, Paragraph } from './pseudo_html'
import { STYLES, WIDTH } from './styles'
import { I18n } from './i18n'

/*
 * La pantalla de configuración inicial ofreciendo a las usuarias
 * recuperar datos.
 */
export class ConfiguracionInicial extends Component {
  static propTypes = { mostrar: PropTypes.bool.isRequired }

  constructor (props) {
    super(props)
    this.state = { mostrar: props.mostrar }
  }

  /*
   * Solo volver a mostrar cuando la escena actual es la espiral
   */
  UNSAFE_componentWillReceiveProps (next) {
    this.setState({
      mostrar: (next.mostrar && Actions.currentScene === 'espiral')
    })
  }

  /*
   * Abrir o cerrar
   */
  toggle () {
    this.setState({ mostrar: !this.state.mostrar })
  }

  /*
   * Cerrar y mostrar ajustes
   */
  ir_a_ajustes () {
    Actions.ajustes()
    this.toggle()
  }

  /*
   * Cerrar y recuperar datos
   */
  recuperar_datos () {
    Actions.recuperar_datos()
    this.toggle()
  }

  render () {
    return (
      <Modal
        animationType={'slide'}
        transparent={false}
        visible={this.state.mostrar}
        onRequestClose={() => true}>

        <View style={STYLES.container}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center'
            }}>
            <View
              style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center'
              }}>

              <Image
                source={{uri: 'https://panel.lunarcomunidad.com/ios/logo.png'}}
                resizeMode="contain"
                style={{ width: WIDTH * 0.7 }} />

            </View>
          </View>

          <View style={{
            marginLeft: 10,
            marginRight: 10,
            marginBottom: 100
          }}>
            <H1>{I18n.t('configuracion_inicial.bienvenida')}</H1>

            <Paragraph>{I18n.t('configuracion_inicial.saber_mas')}</Paragraph>

            <Icon.Button
              name="cog"
              backgroundColor="#00e3ac"
              onPress={() => this.ir_a_ajustes()}>
              {I18n.t('configuracion_inicial.primera_vez')}
            </Icon.Button>

            <View style={{ marginTop: 5 }}>
              <Icon.Button
                name="download"
                backgroundColor="#00d0ff"
                onPress={() => this.recuperar_datos()}>
                {I18n.t('configuracion_inicial.backup')}</Icon.Button>
            </View>
          </View>
        </View>
      </Modal>)
  }
}
