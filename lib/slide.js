import { PANEL } from './const'

import PropTypes from 'prop-types'
import React, { Component } from 'react'

import { View, ScrollView, Text, Image } from 'react-native'
import { STYLES, WIDTH } from './styles'
import { Paragraph, H1, H2, Color, Br } from './pseudo_html'

/*
 * La diapositiva del aspecto
 */
export class Slide extends Component {
  static propTypes = { children: PropTypes.element.isRequired }

  render () {
    return (
      <View style={{}}>
        {this.props.children}
      </View>
    )
  }
}

/*
 * El contenido de la diapositiva
 */
export class SlideParagraph extends Component {
  static propTypes = {
    fase: PropTypes.object.isRequired,
    children: PropTypes.element,
    elemento: PropTypes.element.isRequired,
    aspecto: PropTypes.object.isRequired,
    imagen: PropTypes.string
  }

  render () {
    return (
      <View style={{ flex: 1, alignItems: 'flex-start', paddingTop: 40, paddingLeft: 10, paddingRight: 10, justifyContent: 'space-between' }}>

        <View style={{ flexDirection: 'row' }}>
          {this.props.elemento}

          <Color color={this.props.fase.color}>
            <H1>{this.props.aspecto.elemento.toUpperCase()} </H1><Br/>
            <H2>{this.props.aspecto.sub_titulo}</H2>
          </Color>
        </View>

        {this.props.imagen.length > 0
          ? <Image
            style={{ width: WIDTH - 20, height: WIDTH - 20 }}
            resizeMode="contain"
            source={{ uri: this.props.imagen }}/>
          : null}

        {this.props.children ?
          <ScrollView contentContainerStyle={{ paddingTop: 40, paddingBottom: 40, paddingLeft: 20, paddingRight: 20, alignItems: 'center', justifyContent: 'center' }}>
            <Paragraph>{this.props.children}</Paragraph>
          </ScrollView>
          : null}

        {this.props.imagen.length > 0 ? <View></View> : null}
      </View>
    )
  }
}
