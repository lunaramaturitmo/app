import { TEST, API, HOST, PANEL, VERSION } from './const'

import * as Updates from 'expo-updates'
import * as Sentry from '@sentry/react-native'

import { AsyncStorage } from 'react-native'

import DeviceInfo from 'react-native-device-info'
import analytics from '@react-native-firebase/analytics'

import sjcl from 'sjcl'
import m from 'moment'
import { Base64 } from 'js-base64'

import realm from './models'
import { Helper } from './helper'
import { Logger } from './logger'
import { Notificaciones } from './notificaciones'
import { I18n } from './i18n'
import { Random } from './random'
import { AdhocStorage } from './adhoc_storage'

/*
 * Gestiona la sincronización con el Backend
 */
export class Backend {
  static user_agent () {
    return `${DeviceInfo.getUserAgentSync()} LunarApp/${VERSION}`
  }

  /*
   * Genera una petición PATCH autenticada
   */
  static patch_request (body = {}, auth = '') {
    return {
      method: 'PATCH',
      body: JSON.stringify(body),
      headers: {
        Accept: 'application/json',
        Host: HOST,
        Authorization: `Basic ${auth}`,
        'User-Agent': this.user_agent(),
        'Content-Type': 'application/json'
      }
    }
  }

  /*
   * Genera una petición POST autenticada
   */
  static post_request (body = {}, auth = '') {
    return {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        Accept: 'application/json',
        Host: HOST,
        Authorization: `Basic ${auth}`,
        'User-Agent': this.user_agent(),
        'Content-Type': 'application/json'
      }
    }
  }

  /*
   * Genera una petición GET autenticada
   */
  static get_request (auth = '', etag = '') {
    return {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Host: HOST,
        Authorization: `Basic ${auth}`,
        'User-Agent': this.user_agent(),
        'Content-Type': 'application/json',
        'If-None-Match': etag
      }
    }
  }

  /*
   * Crea un recurso en el backend y devuelve el JSON de respuesta
   *
   * TODO: Gestionar errores
   */
  static save (usuaria, endpoint, data, version = 'v1') {
    const url = `${API}${version}${endpoint}`

    return fetch(url, this.post_request(data, this.authorization(usuaria))).then(r => r.json())
  }

  /*
   * Actualiza un recurso y devuelve el JSON de respuesta
   */
  static update (usuaria, endpoint, data, version = 'v1') {
    const url = `${API}${version}${endpoint}/${data.id}`

    return fetch(url, this.patch_request(data, this.authorization(usuaria))).then(r => r.json())
  }

  /*
   * Obtiene un recurso, con un timestamp opcional
   */
  static async get (usuaria, endpoint, time = null, data = {}, version = 'v1', cache = false) {
    if (!time) data.time = Date.now()
    if (!data.test)
      if (TEST) data.test = true;

    if (!data.iso) data.iso = I18n.current_short_locale
    if (!data.app) data.app = true
    if (!data.time && time) data.time = time

    if (cache) delete data.time

    data = Object.keys(data).map(k => `${k}=${data[k]}`).join('&')

    const url = `${API}${version}${endpoint}?${data}`

    Logger.log('Fetch', url)

    const etag = (cache ? await AsyncStorage.getItem(url) : '')

    return fetch(url, this.get_request(this.authorization(usuaria), etag)).then(r => {
      if (r.status === 401) this.recuperar_cuenta()

      if (cache) AsyncStorage.setItem(url, r.headers.get('etag')).then(r => r)

      return (r.status === 304) ? {} : r.json()
    })
  }

  /*
   * Sincroniza todos los datos aun sin sincronizar!
   *
   * Sincroniza la usuaria si se registró
   *
   * Trae todas las encuestas no sincronizadas junto con sus sintomas y
   * dolores
   *
   * Si su ciclo no fue sincronizado, lo sincroniza primero para obtener
   * su ID
   *
   * XXX: Cambiar a UUID
   */
  static async sync (callback = null) {
    analytics().logEvent('lunar_sync_start', {});

    if (!TEST) {
      analytics().logEvent('lunar_update_start', {});

      const update = await Updates.checkForUpdateAsync()

      if (update.updateAvailable) {
        await Updates.fetchUpdateAsync()
        await Updates.reloadAsync()

        analytics().logEvent('lunar_update_end', {});

        // Al actualizar no podemos garantizar que el resto continúe así
        // que salimos.
        return
      } else {
        analytics().logEvent('lunar_update_none', {});
      }
    }

    // Si no tenemos consentimiento, no hay nada para hacer
    if (!Helper.registrada()) {
      analytics().logEvent('lunar_sync_none', {});

      return;
    }

    const usuaria = Helper.usuaria()
    const ajustes = Helper.ajustes()
    const hoy = Helper.today().getTime()
    const sync = ajustes.sincronizacion
    const sincronizo = m(sync).startOf('day').toDate().getTime()

    Notificaciones.crear(await this.get(usuaria, '/notificaciones.json', sync.getTime()))

    analytics().logEvent('lunar_sync_notificaciones', {});

    // Hacer una sincronización completa una sola vez
    await AdhocStorage.getset('notificaciones_sincronizacion_completa', async () => {
      const time = m('2019-01-01').startOf('year').toDate().getTime()

      Notificaciones.sincronizacion_completa(await this.get(usuaria, '/notificaciones.json', time))
    })

    let r = await this.get(usuaria, '/recomendados.json')
    for (const recomendado of r.data) {
      if (recomendado.attributes.deleted) {
        const deleted = realm.objectForPrimaryKey('Recomendado', recomendado.id)

        if (!deleted) continue;

        Logger.log('Backup.recomendados', `Borrando ${recomendado.id}`)

        realm.write(() => realm.delete(deleted))
      } else {
        const attrs = recomendado.attributes
        attrs.id = recomendado.id

        if (attrs.until === null) attrs.until = 0

        realm.write(() => realm.create('Recomendado', attrs, true))
      }
    }

    analytics().logEvent('lunar_sync_recomendados', {});

    // TODO: Refactorizar para pasar las opciones como un objeto
    this.actualizar_fases(await this.get(usuaria, '/fases.json', null, {}, 'v1', true))

    analytics().logEvent('lunar_sync_fases', {});

    if (typeof callback === 'function') callback()

    // Solo sincronizar las encuestas donde la fecha de actualización
    // sea superior a la de sincronización
    const ciclos = realm.objects('Ciclo')

    for (const ciclo of ciclos) {
      const data = {
        ciclo: {
          me_vino: ciclo.me_vino,
          se_me_fue: ciclo.se_me_fue,
          duracion_promedio: ciclo.duracion,
          fin: ciclo.fin_del_ciclo
        }
      }

      // Crear el ciclo antes de enviar la encuesta
      if (ciclo.remote_id.length === 0) {
        r = await this.save(usuaria, '/ciclos', data)
        realm.write(() => {
          Logger.log('Backend.ciclo', 'Ciclo guardado')
          ciclo.remote_id = r.data.id
          ciclo.synced_at = new Date()
        })
      } else {
        if (!Helper.synced(ciclo)) {

          // Actualizar el ciclo
          Logger.log('Backend.ciclo', 'Actualizando ciclo')

          // Agregar la ID al ciclo
          data.id = ciclo.remote_id
          await this.update(usuaria, '/ciclos', data)
          realm.write(() => ciclo.synced_at = new Date())
        } else {
          Logger.log('Backend.ciclo', 'El ciclo está sincronizado')
        }
      }

      // Sincronizar las encuestas
      //
      // XXX: A esta altura confiamos que el remote_id se guardó en el
      // ciclo
      for (encuesta of ciclo.encuestas) {
        await this.guardar_encuesta(usuaria, encuesta)
      }

      analytics().logEvent('lunar_sync_ciclos', {});
    }

    analytics().logEvent('lunar_sync_end', {});

    // Guardar la fecha de sincronización
    realm.write(() => ajustes.sincronizacion = new Date())
  }

  /*
   * Actualiza las fases a partir de los datos locales o los obtenidos
   * del backend
   */
  static actualizar_fases (fases) {
    if (!fases || !fases.data) return

    // Filtrar todos los aspectos incluidos via JSONAPI
    const aspectos = fases.included.filter(item => item.type === 'aspecto')
    // Filtrar todos los slides incluidos via JSONAPI
    const slides = fases.included.filter(item => item.type === 'slide')

    Logger.log('Backend.actualizar_fases.aspectos', aspectos.length)
    Logger.log('Backend.actualizar_fases.slides', slides.length)

    // Por cada fase
    fases.data.forEach(f => {
      let fase

      // Los atributos están en el objeto `attributes`
      const attrs = f.attributes
      // Convertir el ID a entero
      attrs.id = parseInt(f.id)

      // No necesitamos esto
      delete attrs.locale

      // Creamos o actualizamos la fase si ya existía
      realm.write(() => fase = realm.create('Fase', attrs, true))

      // Obtener los aspectos para esta fase
      aspectos
        .filter(item => fase.id === parseInt(item.relationships.fase.data.id))
        .forEach(a => {
          const attrs = a.attributes
          attrs.id = parseInt(a.id)

          delete attrs.locale

          // Tenemos que chequear si ya existía para saber si lo
          // actualizamos o lo creamos dentro de la fase
          realm.write(() => {
            const aspecto = realm.objectForPrimaryKey('Aspecto', attrs.id)

            if (aspecto) {
              Logger.log('Aspecto.actualizar', `Actualizando ${attrs.id}`)
              realm.create('Aspecto', attrs, true)
            } else {
              Logger.log('Aspecto.crear', `Creando ${attrs.id}`)
              fase.aspectos.push(attrs)
            }
          })

          // XXX: Tenemos que buscarlo después de crearlo/actualizarlo
          // porque push no devuelve el objeto creado.
          const aspecto = realm.objectForPrimaryKey('Aspecto', attrs.id)

          slides
            .filter(item => aspecto.id === parseInt(item.relationships.aspecto.data.id))
            .forEach(s => {
              let slide
              const attrs = s.attributes
              attrs.id = parseInt(s.id)

              delete attrs.locale

              if (!attrs.imagen) attrs.imagen = ''
              if (!attrs.contenido) attrs.contenido = ''

              slide = realm.objectForPrimaryKey('Slide', attrs.id)

              // Las slides se pueden eliminar
              if (slide) {
                if (attrs.deleted) {
                  Logger.log('Slide', `Eliminando ${attrs.id}`)
                  realm.write(() => realm.delete(slide))
                } else {
                  Logger.log('Slide', `Actualizando ${attrs.id}`)
                  realm.write(() => realm.create('Slide', attrs, true))
                }
              } else {
                if (attrs.deleted) return

                realm.write(() => aspecto.slides.push(attrs))
              }
            })
        })
    })
  }

  /*
   * Crear o actualizar encuesta en el backend
   */
  static guardar_encuesta (usuaria, encuesta) {
    if (encuesta.ciclo.remote_id.length === 0) {
      Logger.log('Backend.encuesta', 'El ciclo todavía no se sincronizó')
      return
    }

    const data = { encuesta: {} }
    const encuesta_params = [
      'nivel_de_sangrado', 'sangrado_durante_o_despues_del_sexo',
      'duracion_del_sangrado', 'intensidad_del_dolor',
      'color_del_flujo_vaginal', 'cantidad_del_flujo_vaginal',
      'olor_del_flujo_vaginal', 'created_at', 'updated_at',
      'textura_del_flujo', 'pais', 'marca_vacuna', 'dosis'
    ]

    encuesta_params.forEach(param => data.encuesta[param] = encuesta[param])
    // TODO: Convertir a Array
    data.encuesta.sintomas = encuesta.sintomas.map(sintoma => sintoma.nombre)
    data.encuesta.dolores = encuesta.dolores.map(dolor => dolor.parte_del_cuerpo)
    // Usamos slice() para obtener una copia del Array
    data.encuesta.productos_utilizados = encuesta.productos_utilizados.slice()
    data.encuesta.como_te_sentis = encuesta.como_te_sentis.slice()

    // Si no fue sincronizada aun, la creamos
    if (encuesta.remote_id.length === 0) {
      Logger.log('Backend.guardar_encuesta', 'Enviando encuesta nueva')
      Backend.save(usuaria,
        `/ciclos/${encuesta.ciclo.remote_id}/encuestas`, data)
        .then(r => {
          realm.write(() => {
            encuesta.remote_id = r.data.id
            encuesta.synced_at = new Date()
          })
        })
        .catch(error => Logger.log('Backend.guardar_encuesta', 'No se pudo guardar la encuesta para el ciclo'))
    } else {
      // No hacer nada si ya fue sincronizada
      if (Helper.synced(encuesta)) return

      // Agregar el ID a los datos
      data.id = encuesta.remote_id
      Backend.update(usuaria, `/ciclos/${encuesta.ciclo.remote_id}/encuestas`, data)
        .then(r => {
          Logger.log('Encuesta', 'Actualizada encuesta')
          realm.write(() => encuesta.synced_at = new Date())
        })
        .catch(error => Logger.log('Backend.guardar_encuesta', 'No se pudo actualizar la encuesta'))
    }
  }

  /*
   * Genera la autorización básica de la usuaria
   */
  static authorization (usuaria) {
    return Base64.encode(`${usuaria.email}:${usuaria.password}`)
  }

  /*
   * Genera los atributos para el cifrado
   *
   * Vamos a recibir una contraseña, generar una sal, derivar una
   * contraseña a partir de esto con PBKDF2, hashear la contraseña
   * derivada y usar partes de ese hash para cifrar.
   */
  static generar_atributos_para_cifrado (password, salt = null) {
    // Si no pasamos la sal, generamos una aleatoria
    if (!salt) salt = Random.generar_password(8)
    // Y la convertimos a bits
    salt = sjcl.codec.utf8String.toBits(salt)

    // Devolvemos la promesa de entregar atributos
    try {
      const pbkdf2 = sjcl.misc.pbkdf2(password, salt)
      const hash = sjcl.hash.sha512.hash(pbkdf2)

      // Estamos usando los bits de cryptpad
      return {
        key: sjcl.bitArray.bitSlice(hash, 0, 256),
        iv: sjcl.bitArray.bitSlice(hash, 256, 384),
        auth: sjcl.bitArray.bitSlice(hash, 384, 512),
        salt: sjcl.codec.base64.fromBits(salt)
      }
    } catch (e) {
      Logger.log('Backend.generar_atributos_para_cifrado', e)
      return null
    }
  }

  /*
   * Cifrar utilizando los datos en texto plano y los atributos
   */
  static cifrar (data, attr) {
    return new Promise((resolve, reject) => {
      try {
        const prf = new sjcl.cipher.aes(attr.key)
        const d = sjcl.codec.utf8String.toBits(data)
        const cipherbits = sjcl.mode.gcm.encrypt(prf, d, attr.iv, attr.auth)

        resolve(sjcl.codec.base64.fromBits(cipherbits))
      } catch (e) {
        Logger.log('Backend.cifrar', e)
        reject('No se pudo cifrar')
      }
    })
  }

  /*
   * Hacer el proceso inverso con los mismos atributos, si algo cambia,
   * los datos fueron modificados y no hay resultado.
   */
  static decifrar (ciphertext, attr) {
    try {
      const prf = new sjcl.cipher.aes(attr.key)
      const c = sjcl.codec.base64.toBits(ciphertext)
      const text = sjcl.mode.gcm.decrypt(prf, c, attr.iv, attr.auth)

      return sjcl.codec.utf8String.fromBits(text)
    } catch {
      return null
    }
  }

  /*
   * Volcar los datos de la base de datos a JSON
   *
   * XXX: No hay forma de hacerlo genérico?
   */
  static realm_a_json () {
    let json = {}

    // Los ajustes, devolvemos un array pero solo queremos uno (?)
    json.ajustes = realm.objects('Ajustes').map(ajustes => {
      return {
        duracion_promedio: ajustes.duracion_promedio,
        frecuencia_notificaciones: ajustes.frecuencia_notificaciones,
        primera_vez: ajustes.primera_vez,
        reconfiguracion: ajustes.reconfiguracion,
        sincronizacion: ajustes.sincronizacion
      }
    })

    // Todos los ciclos
    json.ciclos = realm.objects('Ciclo').map(ciclo => {
      const c = {
        id: ciclo.id,
        me_vino: ciclo.me_vino,
        se_me_fue: ciclo.se_me_fue,
        fin_del_ciclo: ciclo.fin_del_ciclo,
        activo: ciclo.activo,
        se_me_fue_temporal: ciclo.se_me_fue_temporal,
        remote_id: ciclo.remote_id,
        fase: ciclo.fase.id,
        synced_at: ciclo.synced_at,
        created_at: ciclo.created_at,
        updated_at: ciclo.updated_at
      }

      // Agregamos todas las notas
      c.notas = ciclo.notas.map(nota => {
        return {
          id: nota.id,
          created_at: nota.created_at,
          updated_at: nota.updated_at,
          texto: nota.texto
        }
      })

      // Y las encuestas.  Los arrays se duplican/clonan.
      c.encuestas = ciclo.encuestas.map(encuesta => {
        const e = {
          id: encuesta.id,
          como_te_sentis: encuesta.como_te_sentis.slice(),
          nivel_de_sangrado: encuesta.nivel_de_sangrado,
          sangrado_durante_o_despues_del_sexo: encuesta.sangrado_durante_o_despues_del_sexo,
          duracion_del_sangrado: encuesta.duracion_del_sangrado,
          intensidad_del_dolor: encuesta.intensidad_del_dolor,
          color_del_flujo_vaginal: encuesta.color_del_flujo_vaginal,
          cantidad_del_flujo_vaginal: encuesta.cantidad_del_flujo_vaginal,
          olor_del_flujo_vaginal: encuesta.olor_del_flujo_vaginal,
          textura_del_flujo: encuesta.textura_del_flujo,
          productos_utilizados: encuesta.productos_utilizados.slice(),
          pais: encuesta.pais,
          marca_vacuna: encuesta.marca_vacuna,
          dosis: encuesta.dosis,
          remote_id: encuesta.remote_id,
          created_at: encuesta.created_at,
          updated_at: encuesta.updated_at,
          synced_at: encuesta.synced_at
        }

        e.sintomas = encuesta.sintomas.map(sintoma => {
          return { nombre: sintoma.nombre }
        })

        e.dolores = encuesta.dolores.map(dolor => {
          return { parte_del_cuerpo: dolor.parte_del_cuerpo }
        })

        return e
      })

      return c
    })

    // XXX: Devolvemos todas las usuarias pero solo queremos la última
    json.usuaria = realm.objects('Usuaria').map(usuaria => {
      return {
        id: usuaria.id,
        remote_id: usuaria.remote_id,
        email: usuaria.email,
        registrada: usuaria.registrada,
        password: usuaria.password,
        password_para_backup: usuaria.password_para_backup
      }
    })

    json.consentimientos = realm.objects('Consentimiento').map(consentimiento => {
      return {
        id: consentimiento.id,
        fecha: consentimiento.fecha,
        estado: consentimiento.estado,
        remote_id: consentimiento.remote_id
      }
    })

    json.notificaciones = realm.objects('Notificacion').map(n => {
      return {
        id: n.id,
        etapa: n.etapa,
        texto: n.texto,
        en_uso: n.en_uso,
        run_at: n.run_at
      }
    })

    return json
  }

  /*
   * Generar un backup cifrado utilizando la contraseña de la usuaria
   */
  static async backup (force = false, onSuccess = null, onError = null) {
    // No hacer nada si no está registrada
    if (!Helper.registrada()) return

    const usuaria = Helper.usuaria()
    const backup = Helper.backup()
    const ultimo_backup = m(backup.ultimo).startOf('day').toDate().getTime()
    const hoy = Helper.today().getTime()

    if (!force && ultimo_backup === hoy) {
      Logger.log('Backend.backup', `Último backup: ${backup.ultimo}`)
      return true
    }

    let password = usuaria.password_para_backup

    // Generar una contraseña ad hoc y guardarla, asumiendo que el
    // dispositivo de la usuaria es seguro!
    if (password.length === 0) {
      Logger.log('Backend.backup', 'Generando contraseña de recuperación')
      password = Random.generar_password(8)
      realm.write(() => usuaria.password_para_backup = password)
    }

    analytics().logEvent('lunar_backup_start', {});

    // Obtener los datos y convertirlos a una String
    const json = JSON.stringify(Backend.realm_a_json())
    // Obtener los atributos para cifrar en base a la contraseña
    const attr = Backend.generar_atributos_para_cifrado(password)
    // Cifrar el texto
    Backend.cifrar(json, attr).then(ciphertext => {
      // Enviarlo al servidor y si queda guardado recordar que se hizo
      return Backend.save(usuaria, '/backups',
        { backup: { ciphertext: ciphertext, salt: attr.salt } })
        .then(r => {
          if (r.data && r.data.id) {
            const backup = Helper.backup()

            realm.write(() => {
              backup.ultimo = new Date()
              backup.cuenta += 1
            })

            analytics().logEvent('lunar_backup_end', {});
          }

          /*
            * Enviar la contraseña para salvaguardar
            *
            * TODO: Cuando tengamos sodium vamos a cifrar la
            * contraseña localmente
            */
          Backend.save(usuaria, '/backups/password_para_backup',
            {
              backup: {
                id: r.data.id,
                password_para_backup: usuaria.password_para_backup
              }
            }).then(o => Logger.log('Backend.backup', 'Salvaguardar la contraseña'))

          // Callback
          if (onSuccess) onSuccess()

          // Si es el primer backup, enviar un recordatorio
          if (!backup.primera_vez) return

          Backend.save(usuaria, '/backups/password',
            { locale: I18n.current_short_locale, backup: { password_para_backup: usuaria.password_para_backup } })
            .then(r => {
              realm.write(() => backup.primera_vez = false)
              Logger.log('Backend.backup', 'Recordatorio enviado')
            })
        })
        .catch(error => {
          Logger.log('Backend.backup', error)
          if (onError) onError()
        })
    }, error => analytics().logEvent('lunar_backup_error', {}));
  }

  /*
   * Convertir a Date todos estos campos
   */
  static recuperar_fechas (objeto) {
    // XXX: buscar 'date' en lib/models.js
    ['created_at', 'updated_at', 'synced_at', 'me_vino', 'se_me_fue',
      'fin_del_ciclo', 'ultimo', 'fecha', 'registrada',
      'reconfiguracion', 'sincronizacion'].forEach(key => {
      if (typeof objeto[key] === 'string') { objeto[key] = m(objeto[key]).toDate() }
    })

    return objeto
  }

  /*
   * Decifrar el backup y rellenar la base de datos
   *
   * TODO: progress_cb no actualiza la interfaz a medida que avanzamos
   */
  static async recuperar_datos (email, password_para_backup, progress_cb) {
    progress_cb(I18n.t('recuperar_datos.obtener'), 1)

    const r = await this.get({ email: email, password: '' }, '/backups', null, { email: email }, 'v2')

    if (r.data === undefined) {
      progress_cb(I18n.t('recuperar_datos.no_hay'), 0)
      return false
    }

    realm.write(() => {
      // Eliminar todas las notificaciones
      // realm.delete(realm.objects('Notificacion'))
      // Borrar los ciclos anteriores
      // TODO: No borrarlos, ver #151
      realm.delete(realm.objects('Ciclo'))
      realm.delete(realm.objects('Consentimiento'))
      realm.delete(realm.objects('Usuaria'))
      realm.delete(realm.objects('Ajustes'))

      for (const backup of r.data) {
        progress_cb(I18n.t('recuperar_datos.decifrando'), 2)

        const attr = this.generar_atributos_para_cifrado(password_para_backup, Base64.decode(backup.attributes.salt))

        if (!attr) {
          progress_cb(I18n.t('recuperar_datos.no_se_pudo_decifrar'), 0)
          continue
        }

        const text = this.decifrar(backup.attributes.ciphertext, attr)

        if (!text) {
          progress_cb(I18n.t('recuperar_datos.contrasenia_incorrecta'), 0)
          continue
        }

        let json = JSON.parse(text)
        progress_cb(I18n.t('recuperar_datos.obtenidos'), 3)

        if (!json) {
          progress_cb(I18n.t('recuperar_datos.daniados'), 0)
          continue
        }

        // Notificaciones
        progress_cb(I18n.t('recuperar_datos.notificaciones'), 5)

        // Dividir el espacio reservado para el progreso de
        // notificaciones por la cantidad de notificaciones a crear
        let p = Math.round((50 - 6) / json.notificaciones.length)
        let i = 0
        let j = 0

        i = 0
        for (const notificacion of json.notificaciones) {
          realm.create('Notificacion', this.recuperar_fechas(notificacion), true)

          progress_cb(I18n.t('recuperar_datos.notificaciones'), Math.floor(5 + (i + 1) * p))
          i++
        }

        // Ciclos
        progress_cb(I18n.t('recuperar_datos.ciclos'), 50)

        const total = json.ciclos.length + json.ciclos.map(c => c.notas.length + c.encuestas.length).reduce((s, t) => s + t)
        p = Math.round((97 - 51) / total)

        i = 0
        for (const c of json.ciclos) {
          const o = Math.floor(50 + (i + 1) * p)
          progress_cb(I18n.t('recuperar_datos.ciclos'), o)

          const encuestas = c.encuestas
          const notas = c.notas

          delete c.notas
          delete c.encuestas

          // Recuperar la fase a la que pertenece el ciclo
          // XXX: Por qué los ciclos pertenecen a fases?
          c.fase = realm.objectForPrimaryKey('Fase', c.id)

          const ciclo = realm.create('Ciclo', this.recuperar_fechas(c), true)

          j = 0
          for (nota of notas) {
            progress_cb(I18n.t('recuperar_datos.nota'), Math.floor(o + (j + 1)))

            nota.ciclo = ciclo
            realm.create('Nota', this.recuperar_fechas(nota), true)
            j++
          }

          j = 0
          for (const e of encuestas) {
            progress_cb(I18n.t('recuperar_datos.encuestas'), Math.floor(o + notas.length + j + 1))

            e.ciclo = ciclo
            const sintomas = e.sintomas
            const dolores = e.dolores

            delete e.sintomas
            delete e.dolores

            // XXX: Retrocompatibilidad
            if (typeof e.como_te_sentis === 'string') { e.como_te_sentis = [e.como_te_sentis] }

            const encuesta = realm.create('Encuesta', this.recuperar_fechas(e), true)

            realm.delete(encuesta.sintomas)
            realm.delete(encuesta.dolores)

            for (const sintoma of sintomas) {
              encuesta.sintomas.push(this.recuperar_fechas(sintoma))
            }

            for (const dolor of dolores) {
              encuesta.dolores.push(this.recuperar_fechas(dolor))
            }

            j++
          }
        }

        // Consentimiento
        progress_cb(I18n.t('recuperar_datos.consentimiento'), 97)
        for (const c of json.consentimientos) {
          realm.create('Consentimiento', c, true)
        }

        // Usuaria
        progress_cb(I18n.t('recuperar_datos.usuaria'), 98)
        // Eliminar todas las usuarias pre-existentes y recuperar
        // solo la última del backup, **no la primera**
        // XXX: #151
        realm.create('Usuaria', json.usuaria[json.usuaria.length - 1], true)

        // Ajustes
        progress_cb(I18n.t('recuperar_datos.ajustes'), 99)
        realm.create('Ajustes', json.ajustes[0], true)

        progress_cb(I18n.t('recuperar_datos.listo'), 100)
      }
    })
  }

  /*
   * Des-registra a la usuaria y elimina todos los datos del backend
   */
  static retirar_consentimiento (usuaria, consentimiento) {
    const auth = this.authorization(usuaria)

    Logger.log('RetirarConsentimiento', 'Eliminando cuenta...')

    fetch(`${API}/v1/consentimientos/retirar`, {
      method: 'DELETE',
      headers: {
        'User-Agent': this.user_agent(),
        Host: HOST,
        Authorization: `Basic ${auth}`
      }
    }).then(r => Logger.log('Consentimiento', 'Retirado'))
      .catch(error => Logger.log('RetirarConsentimiento', error))

    realm.write(() => {
      consentimiento.estado = false
      usuaria.email = 'no'
      usuaria.remote_id = ''
      usuaria.password_para_backup = ''
      usuaria.password = ''
    })
  }

  /*
   * Intenta recuperar la cuenta si no tiene la contraseña
   */
  static async recuperar_cuenta () {
    const usuaria = Helper.usuaria()
    const id = usuaria.remote_id
    const data = {
      id,
      usuaria: {
        email: usuaria.email,
        created_at: usuaria.registrada.getTime().toString(),
        id: usuaria.remote_id,
        password: usuaria.password,
        ciclos: realm.objects('Ciclo').map(c => c.remote_id).filter(x => x.length > 0)
      }
    }

    let action = 'error'

    try {
      const response = await this.update(usuaria, `/usuarias`, data, 'v2')

      if (response.password) {
        realm.write(() => usuaria.password = response.password)
        action = 'recovered'
      } else {
        action = 'insufficient_data'
      }
    } catch (e) {
      Sentry.captureException(e)
    }

    Logger.log('Backend.recuperar_cuenta', `Recuperación ${id}: ${action}`)

    analytics().logEvent(`lunar_account_recovery_${action}`, { id })
  }
}
