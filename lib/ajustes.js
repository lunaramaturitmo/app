'use strict'

import { MENSTRUACION_PROMEDIO } from './const'

import React, { Component } from 'react'
import ReactNative, { ScrollView, View, Text } from 'react-native'

import m from 'moment'
import Icon from 'react-native-vector-icons/FontAwesome'
import DatePicker from 'react-native-datepicker'
import Toast from 'react-native-root-toast'
import ModalSelector from 'react-native-modal-selector'
import { Actions } from 'react-native-router-flux'

import realm, { Ciclo } from './models'
import { Notificaciones } from './notificaciones'
import { Helper } from './helper'
import { Logger } from './logger'
import { Backend } from './backend'
import { H1, AjustesH1, Paragraph, Small, Br } from './pseudo_html'
import { Cajon } from './cajon'
import { STYLES, WIDTH } from './styles'
import { I18n } from './i18n'
import { PseudoMarkdown } from './pseudo_markdown'

/*
 * La pantalla de Ajustes
 */
export class Ajustes extends Component {
  /*
   * Iniciar la vista
   */
  constructor () {
    super()

    this.state = this.obtener_estado()
    this.state.dirty = false

    Logger.log('Ajustes.constructor', this.state)
    Logger.log('Ajustes.constructor.ciclo', this.state.ciclo)
  }

  /*
   * Mantener actualizado el ciclo con los últimos ajustes antes de irse
   */
  componentWillUnmount () {
    Logger.log('Ajustes.componentWillUnmount', 'Cerrando ajustes')

    if (this.state.ajustes.primera_vez) {
      Logger.log('Ajustes.componentWillUnmount', 'Primera vez')
      // Al salir de la configuración por primera vez ya no nos pide
      // reconfigurar
      realm.write(() => this.state.ajustes.primera_vez = false)
    }

    // Reprogramar las notificaciones si cambiamos algo
    if (this.state.dirty) this.programar_notificaciones()
  }

  /*
   * Genera el objeto con el estado actual
   */
  obtener_estado () {
    const ciclo = Helper.ciclo_actual()
    const ajustes = Helper.ajustes()

    return {
      ajustes: ajustes,
      ciclo: ciclo,
      usuaria: Helper.usuaria(),
      consentimiento: Helper.consentimiento(),
      fase: Helper.fase_actual(ciclo),
      confirmacion: false,
      promedio: this.obtener_promedio(ajustes.duracion_promedio),
      me_vino: { maxDate: Helper.today() },
      se_me_fue: {
        minDate: ciclo.me_vino,
        maxDate: Helper.se_me_fue_estimada(ciclo.me_vino)
      }
    }
  }

  /*
   * Alias de Helper.guardar_ajuste cambiando el estado a `dirty`
   */
  guardar_ajuste (key, value) {
    Helper.guardar_ajuste(key, value)

    this.setState({
      dirty: true,
      ajustes: Helper.ajustes()
    })
  }

  /*
   * Modificar la duración promedio en los ajustes
   */
  cambiar_duracion_promedio (promedio) {
    this.guardar_ajuste('duracion_promedio', promedio)

    // Si estamos configurando por primera vez también queremos cambiar
    // la duración del ciclo actual
    if (!this.state.ajustes.primera_vez) return

    // Cambiar la fecha al mismo día de inicio pero con otra duración
    // promedio para forzar al ciclo a recomponerse
    this.cambiar_fecha('me_vino', this.state.ciclo.me_vino)
  }

  /*
   * Programar las notificaciones según la frecuencia seleccionada
   */
  programar_notificaciones () {
    Logger.log('Ajustes.programar_notificaciones', 'Configurando')
    // Eliminar todas las que había programadas
    Notificaciones.vaciar()
    // Programar recordatorios
    Notificaciones.no_me_vino(this.state.ciclo.fin_del_ciclo)
    // Programar según frecuencia
    Notificaciones.periodicas(this.state.ajustes.frecuencia_notificaciones, this.state.ciclo)

    Toast.show(I18n.t('ajustes.reprogramando'))
  }

  /*
   * Calcular una duración promedio en base a todos los ciclos
   * anteriores
   */
  obtener_promedio (promedio_default) {
    // Solo tenemos en cuenta los ciclos cerrados
    const ciclos = realm.objects('Ciclo').filtered('activo == $0', false)
    const duraciones = ciclos.map(c => c.duracion).filter(d => d !== 0)

    // No devolver Infinity
    if (duraciones.length === 0) return promedio_default

    Logger.log('Ajustes.duraciones', duraciones)

    return Math.floor(duraciones.reduce((t, d) => t + d)
                      / duraciones.length)
  }

  /*
   * Cambia las fechas del ciclo.
   *
   * Las fechas se cambian cuando se cambia la fecha de me vino (del
   * primer ciclo al configurar la app o el actual)
   *
   * Cuando estamos configurando por primera vez, el ciclo puede ser
   * mayor a la duración promedio, porque la usuaria puede estar
   * atrasada en su ciclo.  En estos casos, se crea un ciclo promedio
   * sin ajustes, para que la usuaria vea los días del ciclo en la
   * espiral (empiezan en fase 4).
   *
   * @param [String] me_vino / se_me_fue
   * @param [String|Date] la fecha devuelta por el selector de fecha
   */
  cambiar_fecha (key, fecha) {
    // Asegurarse que tenemos una fecha. Siempre trabajamos con el
    // ciclo actual
    const date = m(fecha).startOf('day').toDate(),
          ciclo = this.state.ciclo,
          state = {}

    Logger.log('Ajustes.cambiar_fecha', {
      key: key,
      fecha: fecha,
      date: date
    })

    // Configura la fecha máxima y mínima para el próximo cambio
    switch (key) {
      case 'se_me_fue':
        state.se_me_fue = {
          minDate: ciclo.me_vino,
          maxDate: Helper.se_me_fue_estimada(ciclo.me_vino)
        }
        break
      case 'me_vino':
        // no se cambia nada, la fecha mínima es cualquier fecha y la
        // máxima es siempre hoy.
        break
      default:
    }

    // Guardar las modificaciones
    realm.write(() => {
      // Cambiar la fecha
      ciclo[key] = date
      // Calcular el fin del ciclo al promedio informado
      ciclo.fin_del_ciclo = Helper.fin_del_ciclo_estimado(ciclo.me_vino)
      ciclo.updated_at = new Date

      // Si al cambiar la fecha de me vino la duración supera los 7
      // días, ajustarla
      if (key === 'me_vino' &&
          ciclo.se_me_fue_temporal &&
          (ciclo.duracion_menstrual > Ciclo.duracion_menstrual_estimada_segun_duracion(this.state.ajustes.duracion_promedio) ||
           ciclo.duracion_menstrual < 1)) {
        Logger.log('Ajustes.cambiar_fecha', 'Ajustando fecha')
        ciclo.se_me_fue = Helper.se_me_fue_estimada(ciclo.me_vino)
      }
    })

    state.ciclo = ciclo
    state.dirty = true

    Toast.show(I18n.t('ajustes.cambios_guardados'))

    // Actualiza el estado
    this.setState(state)

    Logger.log('Ajustes.cambiar_fecha', state)
    Logger.log('Ajustes.cambiar_fecha.ciclo', ciclo)
  }

  /*
   * Obtiene el valor a partir de la etiqueta o devuelve un valor por
   * defecto
   *
   * @param [Object] la lista a generar
   * @param [String] el atributo
   * @param [String] el valor por defecto
   */
  label_or_default (list, key, default_key) {
    const found = list.find(e => e.key === key)

    if (found === undefined) return default_key

    return found.label
  }

  /*
   * Lista de frecuencias
   */
  frecuencias () {
    return Object.keys(I18n.t('ajustes.frecuencias'))
      .map(k => ({ key: k, label: I18n.t('ajustes.frecuencias')[k] }))
  }

  /*
   * Lista de duraciones promedio disponibles, de 20 a 35 días
   *
   * XXX: En realidad era 21 a 35 días pero como es un bug histórico que
   * afecta las opciones de las usuarias, lo dejamos.  De todas formas
   * el cálculo de ciclos aplica para 20 días también.
   */
  duraciones () {
    return Array(16).fill(1)
      .map((x, i) => ({ key: 20 + i, label: `${(20 + i).toString()} ${I18n.t('ajustes.dias')}` }))
  }

  /*
   * Duración promedio inicial.  El valor de la opción o un valor por
   * defecto.
   */
  duracion () {
    return this.label_or_default(this.duraciones(),
                                 this.state.ajustes.duracion_promedio,
                                 `28 ${I18n.t('ajustes.dias')}`)
  }

  /*
   * Frecuencia inicial.  El valor de la opción o un valor por defecto.
   */
  frecuencia () {
    return this.label_or_default(this.frecuencias(),
                                 this.state.ajustes.frecuencia_notificaciones,
                                 I18n.t('ajustes.frecuencias.dia_por_medio'))
  }


  /*
   * Genera la vista
   */
  render () {
    return (
      <Cajon back={true}>
        <ScrollView contentContainerStyle={[STYLES.non_view_container, { paddingBottom: 40 }]}>
          <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between', marginLeft: 20, marginRight: 20 }}>
            <View style={STYLES.ajustes_seccion}>
              <H1>{I18n.t('ajustes.ciclo_actual')}</H1>
              <AjustesH1>{this.state.ajustes.primera_vez ? I18n.t('ajustes.ultima_menstruacion') : I18n.t('ajustes.me_vino')}</AjustesH1>
              <DatePicker
                mode="date"
                locale={I18n.current_short_locale}
                date={m(this.state.ciclo.me_vino).format('YYYY-MM-DD')}
                format="YYYY-MM-DD"
                confirmBtnText={I18n.t('ajustes.me_vino')}
                cancelBtnText={I18n.t('ajustes.cancelar')}
                showIcon={false}
                maxDate={m(this.state.me_vino.maxDate).format('YYYY-MM-DD')}
                onDateChange={date => this.cambiar_fecha('me_vino', date)}
                customStyles={{
                  dateInput: {
                    borderColor: '#ccc',
                    borderWidth: 1,
                    padding: 8,
                    borderRadius: 5
                  },
                  dateText: {
                    fontSize: 16,
                    fontFamily: 'Raleway-Medium'
                  }
                }}
              />
            </View>

            {this.state.ajustes.primera_vez ? null
              : <View style={STYLES.ajustes_seccion}>
                <AjustesH1>{I18n.t('ajustes.se_me_fue')}</AjustesH1>

                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <DatePicker
                    mode="date"
                    locale={I18n.current_short_locale}
                    date={m(this.state.ciclo.se_me_fue).format('YYYY-MM-DD')}
                    format="YYYY-MM-DD"
                    confirmBtnText={I18n.t('ajustes.se_me_fue')}
                    cancelBtnText={I18n.t('ajustes.cancelar')}
                    showIcon={false}
                    minDate={m(this.state.se_me_fue.minDate).format('YYYY-MM-DD')}
                    maxDate={m(this.state.se_me_fue.maxDate).format('YYYY-MM-DD')}
                    onDateChange={date => this.cambiar_fecha('se_me_fue', date)}
                    customStyles={{
                      dateInput: {
                        borderColor: '#ccc',
                        borderWidth: 1,
                        padding: 8,
                        borderRadius: 5
                      },
                      dateText: {
                        fontSize: 16,
                        fontFamily: 'Raleway-Medium'
                      }
                    }}
                  />
                </View>

                <Paragraph center><Small>
                  {PseudoMarkdown.render(this.props.fase, I18n.t('ajustes.se_me_fue_ayuda'))}
                </Small></Paragraph>
              </View>}

            {this.state.ajustes.primera_vez ? null :
              <View style={STYLES.ajustes_seccion}>
                <H1>{I18n.t('ajustes.proximo_ciclo')}</H1>
                <Paragraph center>
                  {I18n.t('ajustes.proximo_estimado')}
                  {m(this.state.ciclo.fin_del_ciclo).add(1, 'days').format(I18n.t('ajustes.proximo_estimado_format'))}
                </Paragraph>
              </View>}

            {this.state.ajustes.primera_vez ? null
              : <View style={STYLES.ajustes_seccion}>
                <H1>{I18n.t('ajustes.tu_usuaria')}</H1>
                {Helper.registrada()
                  ? <View>
                      <Paragraph center>{I18n.t('ajustes.hola')} {this.state.usuaria.email}</Paragraph>
                      <Paragraph center>{I18n.t('ajustes.backup')}</Paragraph>
                      <View style={{ flex: 1, alignItems: 'center' }}>
                        <Icon.Button name="key"
                          backgroundColor="grey"
                          style={{ justifyContent: 'center' }}
                          onPress={() => null}>
                          {this.state.usuaria.password_para_backup}
                        </Icon.Button>
                      </View>
                    </View>
                  : <View>
                      <Paragraph center>{I18n.t('ajustes.sin_registrarse')}</Paragraph>
                      <Icon.Button name="sign-in"
                        backgroundColor="grey"
                        onPress={() => Actions.pedir_consentimiento({
                          fase: this.state.fase,
                          volver_a: 'ajustes',
                          ciclo: this.state.ciclo
                        })}>
                          {I18n.t('ajustes.registrarme')}
                       </Icon.Button>
                    </View>}
              </View>}

            <View style={STYLES.ajustes_seccion}>
              <AjustesH1>{I18n.t('ajustes.duracion_promedio')}</AjustesH1>
              <ModalSelector
                data={this.duraciones()}
                initValue={this.duracion()}
                cancelText={I18n.t('ajustes.cancelar')}
                style={{ height: 35 }}
                optionTextStyle={{ fontFamily: 'Raleway-Medium', color: 'black' }}
                cancelTextStyle={{ fontFamily: 'Raleway-Medium' }}
                selectTextStyle={{ fontFamily: 'Raleway-Medium', color: 'black' }}
                onChange={option => this.cambiar_duracion_promedio(option.key) } />

              {this.state.ajustes.primera_vez ? null
                : <Paragraph center>
                  <Small>
                    {I18n.t('ajustes.promedio_actual', { promedio: this.state.promedio })}
                  </Small>
                </Paragraph>}
            </View>

            <View style={STYLES.ajustes_seccion}>
              <AjustesH1>{I18n.t('ajustes.notificaciones')}</AjustesH1>
              <ModalSelector
                data={this.frecuencias()}
                initValue={this.frecuencia()}
                cancelText={I18n.t('ajustes.cancelar')}
                style={{ height: 35 }}
                cancelTextStyle={{ fontFamily: 'Raleway-Medium' }}
                optionTextStyle={{ fontFamily: 'Raleway-Medium', color: 'black' }}
                selectTextStyle={{ fontFamily: 'Raleway-Medium', color: 'black' }}
                onChange={option => this.guardar_ajuste('frecuencia_notificaciones', option.key) } />
            </View>

            {this.state.ajustes.primera_vez
              ? <View style={STYLES.ajustes_seccion}>
                <Icon.Button
                  onPress={() => Actions.pop() }
                  backgroundColor="#00e3ac"
                  name="check">
                  <Text style={STYLES.boton}>
                    {I18n.t('ajustes.empezar')}
                  </Text>
                </Icon.Button>
              </View>
              : null }
          </View>
        </ScrollView>
      </Cajon>
    )
  }
}
