'use strict'

import { MENSTRUACION_PROMEDIO } from './const'

import realm, { Ciclo } from './models'
import m from 'moment'

import Toast from 'react-native-root-toast'

import { I18n } from './i18n'

/*
 * Esta clase contiene métodos estáticos para realizar acciones
 * repetitivas, obtener modelos únicos, etc.
 */
export class Helper {
  /*
   * La duración máxima soportada por un ciclo
   */
  static duracion_maxima () {
    return 35 + MENSTRUACION_PROMEDIO
  }

  /*
   * Detecta si un objeto de la base de datos está sincronizado,
   * comparando la fecha de actualización con la de sincronización.
   *
   * @param [Realm.Object]
   */
  static synced (object) {
    /*
     * XXX Esto está acá para forzar la sincronización por un error en
     * el backend.  Si la fecha de sincronización es anterior a
     * 2019-06-04, forzar la sincronización.
    */
    const sync_before = m('2019-06-04 00:00:00').toDate().getTime()
    if (object.synced_at.getTime() <= sync_before) return false

    return (object.updated_at.getTime() <= object.synced_at.getTime())
  }

  /*
   * Devuelve el comienzo del día presente
   *
   * @return [Date]
   */
  static today () {
    return m().startOf('day').toDate()
  }

  /*
   * Devuelve el fin del día actual
   *
   * @return [Date]
   */
  static end_of_today () {
    return m().endOf('day').toDate()
  }

  /*
   * Detecta si la usuaria se pasó 10 días del fin del ciclo y
   * deberíamos sugerirle que reconfigure la app
   *
   * TODO: Mover a Espiral
   *
   * @param [Ciclo]
   * @return [Bool]
   */
  static mostrar_reconfiguracion (ciclo) {
    const hoy = this.today()
    const ajustes = this.ajustes()

    return (m(ciclo.fin_del_ciclo).diff(m(hoy), 'days') > 10 &&
            m(ajustes.reconfiguracion).diff(m(hoy), 'days') > 0)
  }

  /*
   * Detecta si la usuaria se excedió del máximo de días permitidos y
   * deberíamos notificarle.
   *
   * TODO: Mover a Espiral o Ajustes?
   *
   * @param [Ciclo]
   * @return [Bool]
   */
  static mostrar_exceso (ciclo) {
    const ajustes = this.ajustes()

    return (ajustes.primera_vez && ciclo.dia > this.duracion_maxima())
  }

  /*
   * Guarda cambios en Ajustes individualmente, opcionalmente mostrando
   * una notificación.
   */
  static guardar_ajuste (llave, valor, toast = true) {
    const ajustes = this.ajustes()

    realm.write(() => ajustes[llave] = valor)

    if (toast) { Toast.show(I18n.t('helper.cambios_guardados')) }
  }

  /*
   * Los consentimientos son modelos únicos, este método crea uno si no
   * existen o trae el último disponible.
   *
   * XXX: Estamos unificando esto en Usuaria
   */
  static consentimiento () {
    const consentimientos = realm.objects('Consentimiento')

    if (consentimientos.length === 0) {
      realm.write(() => {
        realm.create('Consentimiento', {
          id: Date.now(),
          estado: false,
          fecha: new Date()
        })
      })
    }

    return consentimientos[consentimientos.length - 1]
  }

  /*
   * El backup es un modelo único que sigue el estado de cuándo se
   * realizan los backups
   */
  static backup () {
    const backups = realm.objects('Backup')

    if (backups.length === 0) { realm.write(() => realm.create('Backup', { primera_vez: true })) }

    return backups[backups.length - 1]
  }

  /*
   * La usuaria es un modelo único que lleva el estado de registro de la
   * cuenta en el backend
   *
   * TODO: Usar UUID como identificador
   */
  static usuaria () {
    const usuarias = realm.objects('Usuaria')

    if (usuarias.length === 0) {
      realm.write(() => {
        realm.create('Usuaria', { id: Date.now(), remote_id: undefined })
      })
    }

    return usuarias[usuarias.length - 1]
  }

  /*
   * Solo podemos sincronizar si la usuaria dio consentimiento y está
   * registrada correctamente.
   *
   * TODO: Deprecar consentimiento en favor de usuaria
   */
  static registrada () {
    const consentimiento = this.consentimiento()
    const usuaria = this.usuaria()

    // 'no' significa que rechazó la pantalla de registro
    return (consentimiento.estado && usuaria.remote_id !== 'no')
  }

  /*
   * Muestra la pantalla de consentimiento
   */
  static pedir_consentimiento () {
    return (this.usuaria().remote_id === '')
  }

  /*
   * Ajustes es un modelo único que tiene la configuración de la app
   */
  static ajustes () {
    let ajustes = realm.objects('Ajustes')

    if (ajustes.length === 0) {
      realm.write(() => {
        realm.create('Ajustes', {
          frecuencia_notificaciones: 'cada_3_dias',
          reconfiguracion: new Date(),
          bienvenida: 'no'
        })
      })
    }

    return ajustes[ajustes.length - 1]
  }

  /*
   * Obtener sitios recomendados activos
   */
  static recomendados () {
    return realm.objects('Recomendado').filtered('until == 0 || until <= $0', Date.now())
  }

  /*
   * Crea un ciclo temporal en base a una fecha de inicio data
   *
   * @param [Date|String] la fecha de inicio
   */
  static crear_ciclo (me_vino) {
    // XXX: Hacemos esto porque la documentación de Realm no es clara
    // sobre el valor de salida de write()
    let ciclo

    me_vino = m(me_vino).startOf('day').toDate()

    realm.write(() => {
      ciclo = realm.create('Ciclo', {
        id: Date.now(),
        activo: true,
        me_vino: me_vino,
        fin_del_ciclo: this.fin_del_ciclo_estimado(me_vino),
        se_me_fue: this.se_me_fue_estimada(me_vino),
        se_me_fue_temporal: true
      })
    })

    return ciclo
  }

  /*
   * Obtiene el ciclo actual, si no existe ninguno, se lo crea con la
   * fecha de hoy.
   */
  static ciclo_actual () {
    const ciclos = realm.objects('Ciclo').filtered('activo == $0', true)

    if (ciclos.length === 0) this.crear_ciclo(this.today())

    return ciclos[ciclos.length - 1]
  }

  /*
   * Encuentra un ciclo para una fecha determinada
   */
  static ciclo_para_fecha(fecha) {
    const ciclos = realm.objects('Ciclo')
      .filtered('me_vino <= $0 && $0 <= fin_del_ciclo', fecha)

    return ciclos[ciclos.length - 1]
  }

  /*
   * Obtiene el modelo de fase para el ciclo
   */
  static fase_actual (ciclo) {
    return realm.objectForPrimaryKey('Fase', ciclo.calcular_fase())
  }

  /*
   * Calcula la fecha en que termina la menstruación, se resta 1 día a
   * la duración promedio para calcular días completos.
   */
  static se_me_fue_estimada (fecha) {
    const estimacion = Ciclo.duracion_menstrual_estimada_segun_duracion(this.ajustes().duracion_promedio) - 1;

    return m(fecha).add(estimacion, 'days').toDate()
  }

  /*
   * Calcula la fecha de fin de ciclo en base al promedio informado
   *
   * XXX: Incorporar el promedio de ciclos cuando haya X ciclos
   * registrados.
   */
  static fin_del_ciclo_estimado (fecha) {
    return m(fecha)
      .add(this.ajustes().duracion_promedio - 1, 'days')
      .toDate()
  }
}
