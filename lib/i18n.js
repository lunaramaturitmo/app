import * as RNLocalize from 'react-native-localize'
import { Logger } from './logger'

/*
 * Obtiene las traducciones de archivos JSON
 */
export class I18n {
  static available_locales = ['es','pt','en'];
  static strings;

  /*
   * Obtiene el idioma actual
   */
  static get current_locale () {
    const best = RNLocalize.findBestAvailableLanguage(this.available_locales)

    if (best) return best.languageTag.split('-')[0]

    return this.available_locales[0]
  }

  /*
   * Traer la version corta del idioma
   */
  static get current_short_locale () {
    // Algunos iPhones informan en_AR como castellano argentino...
    if (I18n.current_locale === 'en_AR') {
      return 'es'
    } else {
      return I18n.current_locale.split('_')[0]
    }
  }

  /*
   * Cargar los archivos según el idioma.
   *
   * XXX: https://github.com/facebook/react-native/issues/6391
   */
  static load () {
    switch (I18n.current_short_locale) {
      case 'es':
        I18n.strings = require('../assets/es.json')
        break
      case 'en':
        I18n.strings = require('../assets/en.json')
        break
      case 'pt':
        I18n.strings = require('../assets/pt.json')
        break
      // Inglés es el idioma por defecto
      default:
        I18n.strings = require('../assets/en.json')
    }
  }

  /*
   * Obtiene la traducción, con valores de reemplazo
   *
   * Intenta encontrar el valor recursivamente o devuelve translation
   * missing
   */
  static t (string, replacements = {}) {
    let t

    try {
      // Encuentra el valor recursivamente
      t = string.split('.').reduce((v, k) => v[k], I18n.strings)
    } catch (e) {
      t = `${I18n.current_short_locale}.${string} translation missing`
    }

    if (typeof t === 'string') t = [t]
    if (!Array.isArray(t)) return t
    if (typeof t[0] === 'object') return t

    // Reemplazar las %{plantillas} por los valores enviados en el
    // objeto de reemplazos y eliminar las no encontradas
    t = t.map(_t => {
      return Object.keys(replacements).reduce((i, k) => {
        const r = new RegExp(`%{${k}}`, 'g')
        return i.replace(r, replacements[k])
      }, _t).replace(/%{\w+}/, '')
    })

    return (t.length > 1) ? t : t[0]
  }
}
