import { PANEL } from './const'

import PropTypes from 'prop-types'
import React, { Component } from 'react'

import { ScrollView, View, ImageBackground } from 'react-native'
import { Actions } from 'react-native-router-flux'

import Icon from 'react-native-vector-icons/FontAwesome'
import ModalSelector from 'react-native-modal-selector'
import Toast from 'react-native-root-toast'

import { STYLES, WIDTH } from './styles'
import { Paragraph, Bold, Resaltado, H1, H2 } from './pseudo_html'
import { Cajon } from './cajon'
import { Logger } from './logger'
import { I18n } from './i18n'
import { PseudoMarkdown } from './pseudo_markdown'

/*
 * Envía el registro de la app al backend.  La usuaria no tiene que
 * estar registrada para esto.
 */
export class Reporte extends Component {
  static propTypes = { fase: PropTypes.object.isRequired }

  constructor () {
    super()
    this.state = { mensaje: null, titulo: null }
  }

  /*
   * Envía el reporte de error
   */
  async enviar_log () {
    Toast.show('Enviando...')
    if (await Logger.send(this.state.titulo)) {
      Toast.show(I18n.t('reporte.enviado'))
    } else {
      Toast.show(I18n.t('reporte.error'))
    }
  }

  render () {
    const i18n = I18n.t('reporte.razones')
    const data = Object.keys(i18n).map(k => ({ key: k, label: i18n[k] }))

    return (
      <Cajon back={true}>
        <ScrollView contentContainerStyle={[STYLES.non_view_container, {flex: 1}]}>
          <View style={STYLES.aspecto_body}>
            {I18n.t('reporte.texto').map((parrafo, i) => {
              const render = PseudoMarkdown.render(this.props.fase, parrafo)
              return (<Paragraph key={`parrafo_${i}`}>{render}</Paragraph>)
            })}

            <ModalSelector
              data={data}
              initValue={I18n.t('reporte.contanos')}
              cancelText={I18n.t('reporte.cancelar')}
              style={{ marginBottom: 10, height: 50, marginRight: 10, marginLeft: 10 }}
              onChange={option => this.setState({ titulo: option.label })}
              cancelTextStyle={{ fontFamily: 'Raleway-Medium' }}
              optionTextStyle={{ fontFamily: 'Raleway-Medium', color: 'black' }}
              selectTextStyle={{ fontFamily: 'Raleway-Medium', color: 'black' }}
            />

            <Icon.Button
              onPress={() => this.enviar_log()}
              name="bug"
              style={{ width: WIDTH * 0.8 }}
              backgroundColor={this.props.fase.color}>
              {I18n.t('reporte.enviar')}
            </Icon.Button>
          </View>
        </ScrollView>
      </Cajon>
    )
  }
}
