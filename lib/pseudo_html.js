/*
 * Elementos textuales similares a HTML
 */
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Text } from 'react-native'

import { STYLES } from './styles'

/*
 * Un salto de línea
 */
export class Br extends Component {
  render () {
    return (<Text>{'\n'}</Text>)
  }
}

/*
 * Título de fase
 */
export class FaseH1 extends Component {
  static propTypes = { children: PropTypes.any.isRequired }

  render () {
    return (
      <Text style={STYLES.fase_h1}>{this.props.children}</Text>
    )
  }
}

/*
 * Título de fase de segundo nivel
 */
export class FaseH2 extends Component {
  static propTypes = { children: PropTypes.any.isRequired }

  render () {
    return (
      <Text style={STYLES.fase_h2}>{this.props.children}</Text>
    )
  }
}

/*
 * Título de fase de tercer nivel
 */
export class FaseH3 extends Component {
  static propTypes = { children: PropTypes.any.isRequired }

  render () {
    return (
      <Text style={STYLES.fase_h3}>{this.props.children}</Text>
    )
  }
}

/*
 * Título de ajustes de primer nivel
 */
export class AjustesH1 extends Component {
  static propTypes = { children: PropTypes.any.isRequired }

  render () {
    return (
      <Text style={STYLES.ajustes_h1}>{this.props.children}</Text>
    )
  }
}

// Genera un título de primer nivel
export class H1 extends Component {
  static propTypes = { children: PropTypes.any.isRequired }

  render () {
    return (
      <Text style={STYLES.h1}>{this.props.children}</Text>
    )
  }
}

// Genera un título de segundo nivel
export class H2 extends Component {
  static propTypes = { children: PropTypes.any.isRequired }

  render () {
    return (
      <Text style={STYLES.h2}>{this.props.children}</Text>
    )
  }
}

/*
 * Un párrafo
 */
export class Paragraph extends Component {
  static propTypes = {
    style: PropTypes.object,
    center: PropTypes.bool,
    children: PropTypes.any.isRequired
  }

  static defaultProps = { style: {}, center: false }

  render () {
    return (
      <Text
        style={[
          STYLES.p,
          { textAlign: (this.props.center ? 'center' : 'left') },
          this.props.style
        ]}>{this.props.children}</Text>
    )
  }
}

/*
 * Texto más pequeño
 */
export class Small extends Component {
  static propTypes = { children: PropTypes.any.isRequired }

  render () {
    return (
      <Text style={STYLES.small}>{this.props.children}</Text>
    )
  }
}

/*
 * Fuente más intensa
 */
export class Bold extends Component {
  static propTypes = { children: PropTypes.any.isRequired }

  render () {
    return (
      <Text style={STYLES.bold}>{this.props.children}</Text>
    )
  }
}

/*
 * Cambiar el color
 */
export class Color extends Component {
  static propTypes = {
    color: PropTypes.string.isRequired,
    children: PropTypes.any.isRequired
  }

  render () {
    return (
      <Text style={{ color: this.props.color }}>{this.props.children}</Text>
    )
  }
}

/*
 * Resaltado con color
 */
export class Resaltado extends Component {
  static propTypes = {
    color: PropTypes.string.isRequired,
    foreground: PropTypes.bool
  };

  static defaultProps = {
   foreground: false
  }

  constructor (props) {
    super(props)
    this.state = { style: { backgroundColor: this.props.color } }

    if (this.props.foreground) this.state.style.color = 'white'
  }

  render () {
    return (
      <Text style={this.state.style}>{this.props.children}</Text>
    )
  }
}

/*
 * Un botón con texto dentro
 */
export class BotonText extends Component {
  static propTypes = {
   style: PropTypes.object,
   children: PropTypes.any.isRequired
  }

  static defaultProps = { style: {} }

  constructor (props) {
    super(props)

    this.state = { style: this.props.style }
  }

  UNSAFE_componentWillReceiveProps (next) {
    this.setState({ style: next.style })
  }

  render () {
    return (<Text style={[STYLES.boton, this.state.style]}>
      {this.props.children}
    </Text>)
  }
}
