import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { ScrollView, View, Image } from 'react-native'
import Swiper from 'react-native-swiper'
import analytics from '@react-native-firebase/analytics'

import { Cajon } from './cajon'
import { STYLES, WIDTH } from './styles'
import { PANEL } from './const'
import { Slide, SlideParagraph } from './slide'
import { LunaNueva, LunaCreciente, LunaLlena, LunaMenguante, Agua, Aire, Fuego, Tierra } from './svg_elements'
import { Color, H1, H2, Br, Paragraph } from './pseudo_html'
import { I18n } from './i18n'
import { PseudoMarkdown } from './pseudo_markdown'
import { Helper } from './helper'

/*
 * El Aspecto es una vista con portada y diapositivas
 */
export class Aspecto extends Component {
  /*
   * Props.
   *
   * aspecto: una instancia de Aspecto
   */
  static propTypes = {
    aspecto: PropTypes.object.isRequired
  }

  /*
   * Iniciar la vista
   */
  constructor (props) {
    super(props)

    this.state = { aspecto: this.props.aspecto }

    // XXX: No confiamos en las relaciones de Realm :/
    if (this.props.aspecto.fase) {
      this.state.fase = this.props.aspecto.fase[0]
    } else {
      this.state.fase = Helper.fase_actual(Helper.ciclo_actual())
    }

    analytics().logEvent('lunar_aspecto_open', { id: this.props.aspecto.id })
  }

  /*
   * Dibujar la vista
   */
  render () {
    let luna, imagen, elemento
    // Elegir la luna y la imagen de portada
    switch (this.state.fase.luna) {
      case I18n.t('lunas.nueva'):
        luna = <LunaNueva scale="1" color={this.state.fase.color} />
        imagen = {uri: 'https://panel.lunarcomunidad.com/ios/fase_1.png'}
        break
      case I18n.t('lunas.creciente'):
        luna = <LunaCreciente scale="1" color={this.state.fase.color} />
        imagen = {uri: 'https://panel.lunarcomunidad.com/ios/fase_2.png'}
        break
      case I18n.t('lunas.llena'):
        luna = <LunaLlena scale="1" color={this.state.fase.color} />
        imagen = {uri: 'https://panel.lunarcomunidad.com/ios/fase_3.png'}
        break
      case I18n.t('lunas.menguante'):
        luna = <LunaMenguante scale="1" color={this.state.fase.color} />
        imagen = {uri: 'https://panel.lunarcomunidad.com/ios/fase_4.png'}
        break
    }

    // El elemento actual traducido
    switch (this.state.aspecto.elemento) {
      case I18n.t('aspectos.agua'):
        elemento = (<Agua fase={this.state.fase} fill/>)
        break
      case I18n.t('aspectos.aire'):
        elemento = (<Aire fase={this.state.fase} fill/>)
        break
      case I18n.t('aspectos.fuego'):
        elemento = (<Fuego fase={this.state.fase} fill/>)
        break
      case I18n.t('aspectos.tierra'):
        elemento = (<Tierra fase={this.state.fase} fill/>)
        break
    }

    // Las diapositivas a mostrar, la primera es la portada
    const slides = [
      (<View key="slide_cover" style={{ flex: 1, alignItems: 'flex-start', paddingTop: 40, paddingLeft: 10, paddingRight: 10, justifyContent: 'space-between' }}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <View style={{ flex: 1 }}>
            <Color color={this.state.fase.color}>
              <H1>{this.state.fase.etapa.toUpperCase()}</H1><Br/>
              <H2>{I18n.t('slide.luna').toUpperCase()} {this.state.fase.luna.toUpperCase()}</H2>
            </Color>
          </View>

          {luna}
        </View>

        <View style={{ justifyContent: 'center', alignItems: 'center', height: WIDTH - 20 }}>
          {this.state.fase.imagen.startsWith(PANEL)
          ?
            <Image
              style={{ width: WIDTH - 20, height: WIDTH - 20 }}
              resizeMode="contain"
              source={{ uri: this.state.fase.imagen }}/>
          :
            <Image
                resizeMode="contain"
                source={imagen}
                style={{ width: WIDTH - 20, height: WIDTH - 20 }} />
          }
        </View>

        <View></View>
      </View>),
      (<View key="slide_elemento" style={{ flex: 1, alignItems: 'flex-start', paddingTop: 40, paddingLeft: 10, paddingRight: 10, justifyContent: 'flex-start' }}>
        <View style={{ flexDirection: 'row' }}>
          {elemento}

          <Color color={this.state.fase.color}>
            <H1>{this.state.aspecto.elemento.toUpperCase()} </H1><Br/>
            <H2>{this.state.aspecto.sub_titulo}</H2>
          </Color>
        </View>

        <ScrollView contentContainerStyle={{ padding: 20, paddingTop: 40, paddingBottom: 40, alignItems: 'center', justifyContent: 'center' }}>
          {this.state.fase.contenido.split('\n').map((t, i) => {
            return (<Paragraph key={`texto-${i}`}>{t}</Paragraph>)
          })}
        </ScrollView>
      </View>)]

    // Agregar las diapositivas ordenadas por número de orden creciente
    this.state.aspecto.slides.sorted('order', false).forEach(slide => {
      let contenido

      if (slide.contenido.length > 0) {
        contenido = PseudoMarkdown.render(this.state.fase, slide.contenido)
      }

      slides.push(<SlideParagraph
        key={`slide_${slide.id}`}
        elemento={elemento}
        aspecto={this.state.aspecto}
        imagen={slide.imagen}
        fase={this.state.fase}>
        {contenido ? <Paragraph>{contenido}</Paragraph> : null}
      </SlideParagraph>)
    })

    return (<Cajon back={true}>
      <Swiper
        key={slides.length + 2}
        horizontal={true}
        loop={false}
        activeDotColor={this.state.fase.color}
        paginationStyle={{ backgroundColor: '#f0ede9', height: 40, bottom: 0 }}
        style={STYLES.swiper}>

        {slides}
      </Swiper>
    </Cajon>)
  }
}
