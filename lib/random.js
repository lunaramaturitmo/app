import isaac from 'isaac'

export class Random {
  /*
   * Obtiene una contraseña aleatoria de un largo especificado
   * Usa ISAAC como fuente de pseudo-aleatoriedad
   */
  static generar_password (length = 16) {
    const p = 'abcdefghijklmnopqrstuvwxyz0123456789'
    const pw = []

    for (let i = 0; i < length; i++) { pw.push(p.charAt(Math.floor(isaac.random() * p.length))) }

    return pw.join('')
  }
}
