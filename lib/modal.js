import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { View, ImageBackground, Modal } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import Icon from 'react-native-vector-icons/FontAwesome'

import { STYLES, WIDTH } from './styles'
import { BotonText } from './pseudo_html'
import { I18n } from './i18n'

EStyleSheet.build({ $rem: WIDTH > 340 ? 18 : 16 })

/*
 * Muestra una vista a pantalla completa que se superpone a la vista
 * actual.  Se puede cerrar y pasarle el contenido como props.
 */
export class LunarModal extends Component {
  /*
   * show: mostrar u ocultar
   * children: contenido
   * onPress: función al cerrar
   * fase: la fase actual
   */
  static propTypes = {
   show: PropTypes.bool.isRequired,
   children: PropTypes.element.isRequired,
   onPress: PropTypes.func,
   fase: PropTypes.object.isRequired
  };

  render () {
    return (
      <Modal
        animationType={'slide'}
        transparent={false}
        visible={this.props.show}
        onRequestClose={() => true}>

        <View style={STYLES.container}>
          <ImageBackground
            resizeMode="contain"
            source={{uri: 'https://panel.lunarcomunidad.com/ios/espiral_transparente.png'}}
            style={{ width: WIDTH * 0.8, height: WIDTH * 0.8 }}>

            <View style={STYLES.aspecto_body}>
              {this.props.children}

              {this.props.onPress
                ? <Icon.Button
                  name="close"
                  backgroundColor={this.props.fase.color}
                  onPress={() => this.props.onPress()}>
                  <BotonText>{I18n.t('modal.cerrar')}</BotonText>
                </Icon.Button>
                : null}
            </View>
          </ImageBackground>
        </View>
      </Modal>)
  }
}
