import { PANEL } from './const'

import { Dimensions, Platform, StatusBar } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'

/*
 * Dimensiones de la app
 */
export const WIDTH = Dimensions.get('window').width
export const HEIGHT = Dimensions.get('window').height

/*
 * La hoja de estilos de la app
 *
 * TODO: Traer todos los estilos sueltos acá
 * TODO: Emprolijar mirando la app
 */
export const STYLES = EStyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#f0ede9',
    paddingTop: 40
  },
  swiper: {
    backgroundColor: '#f0ede9'
  },
  slide: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 40
  },
  slide_cover: {
    marginLeft: WIDTH * 0.05,
    marginRight: WIDTH * 0.05,
    flex: 1,
    backgroundColor: 'transparent'
  },
  main: {
    paddingBottom: 20
  },
  non_view_container: {
    backgroundColor: '#f0ede9',
    paddingTop: 40
  },
  nota_column: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingTop: 15
  },
  nota_row: {
    width: WIDTH * 0.9,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end'
  },
  nota_lunas: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  nota_input: {
    fontSize: '1rem',
    lineHeight: '1.5rem',
    padding: 10,
    width: WIDTH * 0.9,
    textAlignVertical: 'top',
    borderWidth: 1,
    marginBottom: 10
  },
  nota_preview: {
    flex: 1,
    margin: 1,
    height: 120,
    borderWidth: 1,
    borderColor: 'lightgrey',
    padding: 5,
    backgroundColor: '#f0ede9'
  },
  nota_grid: {
    flex: 1,
    backgroundColor: '#f0ede9',
    paddingTop: 40
  },
  sensacion: {
    backgroundColor: 'pink',
    width: WIDTH / 2,
    margin: 5,
  },
  centrar: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center'
  },
  boton: {
    color: 'white',
    fontSize: '0.9rem',
    fontFamily: 'Raleway-Light'
  },
  menu_item: {
    paddingTop: 5,
    paddingBottom: 5,
    fontSize: '0.9rem',
    fontFamily: 'Raleway-Light'
  },
  aspecto_elemento: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: WIDTH * 0.9,
    marginTop: 10
  },
  aspecto_titulo: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: 10,
    marginBottom: 0,
    flex: 1
  },
  aspecto_image: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 10
  },
  aspecto_body: {
    marginLeft: WIDTH * 0.1,
    marginRight: WIDTH * 0.1,
    flex: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  aspecto_copete: {
    marginBottom: 10,
    borderBottomWidth: 2
  },
  barra_de_aspectos_exterior: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    flex: 1
  },
  barra_de_aspectos_titulo: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 15
  },
  ajustes_h1: {
    fontSize: '1rem',
    fontFamily: 'Raleway',
    marginBottom: 3
  },
  ajustes_seccion: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10
  },
  fase_h1: {
    fontSize: '1.2rem',
    fontFamily: 'Raleway-ExtraBold',
    fontWeight: 'bold',
    marginBottom: 3
  },
  fase_h2: {
    fontSize: '1.2rem',
    fontFamily: 'Raleway-Medium',
    fontStyle: 'italic',
    marginBottom: 3
  },
  fase_h3: {
    fontSize: '1rem',
    fontFamily: 'Raleway-Light',
    marginBottom: 10
  },
  h1: {
    fontSize: '1.2rem',
    fontFamily: 'Raleway-Medium',
    marginTop: HEIGHT * 0.03
  },
  h2: {
    fontSize: '1.2rem',
    fontFamily: 'Raleway-Light',
    fontStyle: 'italic'
  },
  p: {
    fontSize: '1rem',
    fontFamily: 'Roboto-Light',
    marginBottom: 10
  },
  small: {
    fontSize: '0.8rem'
  },
  bold: {
    fontFamily: 'Roboto-Bold',
    fontWeight: 'bold'
  },
  center: {
    alignItems: 'center'
  },
  dolor_view: {
    position: 'relative',
    backgroundColor: 'pink',
    borderRadius: 50,
    padding: 5
  },
  dolor: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  dolor_text: {
    fontFamily: 'Roboto-Light',
    alignItems: 'center',
    justifyContent: 'center',
    opacity: 1
  },
  android_safe_area: {
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  }
})
