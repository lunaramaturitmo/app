'use strict'

import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Platform, ScrollView, View, TouchableOpacity, Text, TextInput, Keyboard, KeyboardAvoidingView } from 'react-native'
import { Actions } from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/FontAwesome'
import Toast from 'react-native-root-toast'
import m from 'moment'

import realm from './models'
import { Cajon } from './cajon'
import { Helper } from './helper'
import { STYLES, HEIGHT } from './styles'
import { Guardar, Lapiz, LunaNueva, LunaCreciente, LunaLlena, LunaMenguante } from './svg_elements'
import { Color, FaseH1 } from './pseudo_html'
import { Logger } from './logger'
import { I18n } from './i18n'

/*
 * La nota se puede crear o editar
 */
export class Nota extends Component {
  static propTypes = {
  id: PropTypes.number,
  ciclo: PropTypes.object,
  date: PropTypes.string
  };

  static defaultProps = {
   date: m(Helper.today()).format('YYYY-MM-DD')
  }

  /*
   * Hay varias formas de llegar a la nota:
   *
   * * Especificando el ID.  Si la nota existe, la forma más segura de
   * llegar es a través de su ID.
   *
   * * Especificando la fecha.  La nota no existe, pero la queremos
   * crear en una fecha determinada.
   *
   * * Especificando el ciclo.  La nota no existe, pero la queremos
   * crear dentro de un ciclo determinado.
   */
  constructor (props) {
    Logger.log('Nota.constructor', 'Viendo nota')
    super(props)

    let nota, ciclo, fase
    let date = m(this.props.date).toDate()

    // Si hay una ID, traer la nota
    if (this.props.id) {
      nota = realm.objectForPrimaryKey('Nota', this.props.id)
    // Intentar traer la nota por el día
    } else {
      nota = realm.objects('Nota').filtered('created_at == $0', date)[0]
    }

    if (nota) {
      ciclo = nota.ciclo
      fase = realm.objectForPrimaryKey('Fase',
        ciclo.calcular_fase(nota.dia))
      date = nota.created_at
    } else {
      // La nota no existe, necesitamos pasar el ciclo y opcionalmente el
      // día
      ciclo = this.props.ciclo
      // La fecha por defecto es hoy en YYYY-MM-DD
      fase = realm.objectForPrimaryKey('Fase', ciclo.calcular_fase_segun_fecha(date))
    }

    this.state = {
      text: (nota) ? nota.texto : '',
      text_height: HEIGHT * 0.6,
      ciclo: ciclo,
      nota: nota,
      fase: fase,
      date: date
    }

    Logger.log('Nota.constructor.ciclo', ciclo)
    Logger.log('Nota.constructor.nota', nota)
  }

  /*
   * Guarda la nota, creando o actualizando
   */
  guardar_nota () {
    Logger.log('Nota.guardar_nota', 'Guardando')

    const attrs = {
      texto: this.state.text,
      updated_at: Helper.today(),
      ciclo: this.state.ciclo
    }

    // Si estamos actualizando o creando
    if (this.state.nota) {
      attrs.id = this.state.nota.id
    } else {
      attrs.id = Date.now()
      attrs.created_at = this.state.date
    }

    Logger.log('Nota.guardar_nota', attrs)

    const state = {}
    realm.write(() => state.nota = realm.create('Nota', attrs, 'all'))

    Toast.show(I18n.t('nota.guardada'))
    this.setState(state)
  }

  /*
   * Para que la nota se ajuste junto con el teclado, registramos
   * eventos.
   */
  UNSAFE_componentWillMount () {
    this.on_keyboard_show_listener = Keyboard.addListener('keyboardDidShow', this.on_keyboard_show.bind(this))
    this.on_keyboard_hide_listener = Keyboard.addListener('keyboardDidHide', this.on_keyboard_hide.bind(this))
  }

  /*
   * Eliminar los eventos al salir de la nota
   */
  componentWillUnmount () {
    this.on_keyboard_show_listener.remove()
    this.on_keyboard_hide_listener.remove()
  }

  /*
   * Ajustar el alto de la nota al ocultar el teclado
   */
  on_keyboard_hide () {
    this.setState({ text_height: HEIGHT * 0.6 })
  }

  /*
   * Ajustar el alto de la nota al mostrar el teclado
   */
  on_keyboard_show () {
    this.setState({ text_height: HEIGHT * 0.3 })
  }

  render () {
    const fase = this.state.fase
    const luna_nueva_color = (fase.id === 1) ? fase.color : 'black'
    const luna_creciente_color = (fase.id === 2) ? fase.color : 'black'
    const luna_llena_color = (fase.id === 3) ? fase.color : 'black'
    const luna_menguante_color = (fase.id === 4) ? fase.color : 'black'

    return (
      <Cajon back={true}>
        <ScrollView
          keyboardDismissMode="interactive"
          ref={ref => this.scrollView = ref}
          contentContainerStyle={[STYLES.non_view_container, {flex: 1}]}>

          <View style={[STYLES.nota_column, { flexGrow: 1 }]}>
            <View style={STYLES.nota_row}>
              <View>
                <Lapiz color={fase.color}/>

                <Color color={fase.color}>
                  <FaseH1>{I18n.t('nota.fase').toUpperCase()} {fase.id}</FaseH1>
                </Color>
              </View>

              <View style={STYLES.nota_lunas}>
                <LunaNueva scale="0.6" size="20" color={luna_nueva_color}/>
                <LunaCreciente scale="0.6" size="20" color={luna_creciente_color}/>
                <LunaLlena scale="0.6" size="20" color={luna_llena_color}/>
                <LunaMenguante scale="0.6" size="20" color={luna_menguante_color}/>
              </View>
            </View>

            <KeyboardAvoidingView behavior="padding" style={STYLES.center}>
              <TextInput
                multiline={true}
                placeholder={I18n.t('nota.placeholder')}
                placeholderTextColor="grey"
                value={this.state.text}
                selectionColor={fase.color}
                underlineColorAndroid="#f0ede9"
                onChangeText={text => this.setState({ text: text || '' })}
                style={[STYLES.nota_input, { borderColor: fase.color, height: this.state.text_height }]}/>

              <TouchableOpacity onPress={() => this.guardar_nota()} style={STYLES.center}>
                <Guardar scale="1"/>
                <Text style={[STYLES.p, STYLES.center, STYLES.small]}>{I18n.t('nota.guardar')}</Text>
              </TouchableOpacity>
            </KeyboardAvoidingView>
          </View>
        </ScrollView>
      </Cajon>
    )
  }
}
