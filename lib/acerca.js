import React, {Component} from 'react';
import {ScrollView, View, Image, Linking} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import {Paragraph, Small} from './pseudo_html';
import {Cajon} from './cajon';
import {STYLES, WIDTH} from './styles';
import {I18n} from './i18n';
import {PseudoMarkdown} from './pseudo_markdown';
import {VERSION} from './const';

/*
 * Acerca muestra un texto explicando qué es Lunar
 */
export class Acerca extends Component {
  /*
   * Dibujar la vista
   */
  render() {
    return (
      <Cajon back={true}>
        <ScrollView contentContainerStyle={STYLES.non_view_container}>
          <View
            style={{
              flexGrow: 1,
              flexDirection: 'row',
              justifyContent: 'center',
            }}>
            <Image
              source={{uri: 'https://panel.lunarcomunidad.com/ios/logo.png'}}
              style={{width: 100, height: 100}}
              resizeMode="contain"
            />
          </View>

          <View style={STYLES.aspecto_body}>
            <View style={{marginBottom: 10}}>
              <Icon.Button
                name="facebook"
                backgroundColor="#3b5998"
                style={{width: WIDTH / 2}}
                onPress={() =>
                  Linking.openURL('https://www.facebook.com/soyapplunar')
                }>
                aplicacionlunar
              </Icon.Button>
            </View>

            <View style={{marginBottom: 10}}>
              <Icon.Button
                name="instagram"
                backgroundColor="#e4405f"
                style={{width: WIDTH / 2}}
                onPress={() =>
                  Linking.openURL('https://www.instagram.com/applunar/')
                }>
                @applunar
              </Icon.Button>
            </View>

            <View style={{marginBottom: 10}}>
              <Paragraph><Small>Lunar v{VERSION}</Small></Paragraph>
            </View>

            {I18n.t('acerca').map((parrafo, i) => {
              const render = PseudoMarkdown.render(this.props.fase, parrafo);
              return <Paragraph key={`parrafo_${i}`}>{render}</Paragraph>;
            })}
          </View>
        </ScrollView>
      </Cajon>
    );
  }
}
