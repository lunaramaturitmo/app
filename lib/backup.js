import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { ScrollView, View, Image } from 'react-native'
import { Actions } from 'react-native-router-flux'

import Icon from 'react-native-vector-icons/FontAwesome'
import Toast from 'react-native-root-toast'

import m from 'moment'

import { Paragraph, H1, Small } from './pseudo_html'
import { Cajon } from './cajon'
import { STYLES } from './styles'

import { Helper } from './helper'
import { Backend } from './backend'

import { I18n } from './i18n'
import { PseudoMarkdown } from './pseudo_markdown'
import { LunarModal } from './modal'

/*
 * Vista de Backups
 */
export class Backup extends Component {
  static propTypes = {
    fase: PropTypes.object.isRequired,
    ciclo: PropTypes.object.isRequired
  }

  constructor (props) {
    super(props)

    this.state = {
      fase: this.props.fase,
      ciclo: this.props.ciclo,
      usuaria: Helper.usuaria(),
      backup: Helper.backup(),
      confirmacion: false,
      consentimiento: Helper.consentimiento()
    }
  }

  /*
   * Forzar la creación de un backup
   */
  backup () {
    Toast.show(I18n.t('backup.iniciando'))
    Backend.backup(true,
      () => {
        Toast.show(I18n.t('backup.exito'))
        this.setState({ backup: Helper.backup() })
      },
      () => { Toast.show(I18n.t('backup.error')) })
  }

  /*
   * Cambiar el estado de un valor booleano, útil para modales
   *
   * @param [String] atributo del estado a cambiar
   */
  toggle (cosa) {
    const s = {}

    s[cosa] = !this.state[cosa]

    this.setState(s)
  }

  render () {
    const replacements = {
      email: this.state.usuaria.email,
      password_para_backup: this.state.usuaria.password_para_backup,
      ultimo: this.state.backup.cuenta > 0 ? m(this.state.backup.ultimo).format(I18n.t('moment.datetime')) : I18n.t('backup.sin_backups')
    }

    const registrada = Helper.registrada()
    const texto = registrada ? 'backup.texto' : 'backup.texto_sin_registro'

    return (<Cajon back={true}>
      <LunarModal
        show={this.state.confirmacion}
        fase={this.state.fase}
        onPress={() => this.toggle('confirmacion')}>
        <View style={{ marginBottom: 20 }}>
          <Paragraph center>
            {I18n.t('ajustes.estas_segura')}
          </Paragraph>

          <Icon.Button name="sign-out"
            backgroundColor={this.state.fase.color}
            onPress={() => {
              this.toggle('confirmacion')
              Backend.retirar_consentimiento(this.state.usuaria, this.state.consentimiento)
            }}>
            {I18n.t('ajustes.yep')}
          </Icon.Button>
        </View>
      </LunarModal>

      <ScrollView contentContainerStyle={[STYLES.non_view_container, STYLES.main, { flex: 1, justifyContent: 'flex-start', alignItems: 'center', paddingLeft: 20, paddingRight: 20 }]}>
        <Image
          source={{uri: 'https://panel.lunarcomunidad.com/ios/logo.png'}}
          style={{ width: 100, height: 100 }}
          resizeMode="contain" />

        <H1>{I18n.t('backup.titulo')}</H1>

        {I18n.t(texto, replacements).map((parrafo, i) => {
          const render = PseudoMarkdown.render(this.state.fase, parrafo)
          return (<Paragraph center key={`parrafo_${i}`}>{render}</Paragraph>)
        })}

        {registrada ?
          <View>
            <View style={{ marginBottom: 10 }}>
              <Icon.Button name="cloud-upload" onPress={() => this.backup()}>
                {I18n.t('backup.manual')}
              </Icon.Button>
            </View>

            <Icon.Button backgroundColor="grey" name="sign-out" onPress={() => this.toggle('confirmacion')}>
              {I18n.t('backup.retirar_consentimiento')}
            </Icon.Button>
          </View>
        :
          <Icon.Button name="sign-in"
            onPress={() => Actions.pedir_consentimiento({
              fase: this.state.fase,
              volver_a: 'ajustes',
              ciclo: this.state.ciclo
            })}>
            {I18n.t('ajustes.registrarme')}
          </Icon.Button>
        }
      </ScrollView>
    </Cajon>)
  }
}
