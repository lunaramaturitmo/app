'use strict'

import { MENSTRUACION_PROMEDIO } from './const'

/*
 * Modelo de datos
 *
 * En este archivo se registran todos los datos a almacenar en la base
 * de datos interna de la app.  Con Realm se pueden hacer consultas
 * también.
 */

import * as Sentry from '@sentry/react-native'
import Realm from 'realm'
import m from 'moment'

import { Logger } from './logger'

/*
 * Ajustes es un modelo único donde se guardan las configuraciones
 *
 * duracion_promedio: La duración promedio del ciclo que informa la
 * usuaria al configurar la app.  En base a este valor se crean los
 * ciclos temporales.
 *
 * frecuencia_notificaciones: Cada cuantos días se envían
 * notificaciones.
 *
 * primera_vez: Si es la primera configuración.
 *
 * sincronizacion: La fecha de última sincronizacion.  Empezamos por
 * 2019-01-01 para sincronizar todo lo que haya disponible.
 */
class Ajustes extends Realm.Object {}
Ajustes.schema = {
  name: 'Ajustes',
  properties: {
    duracion_promedio: { type: 'int', default: 28 },
    frecuencia_notificaciones: 'string',
    primera_vez: { type: 'bool', default: true },
    reconfiguracion: 'date',
    sincronizacion: {
      type: 'date',
      default: m('2019-01-01').startOf('year').toDate()
    }
  }
}

/*
 * Mantiene el estado de los backups
 *
 * primera_vez: Si es el primer backup que se realiza, enviar
 * recordatorio de contraseña por correo.
 *
 * ultimo: La fecha del último backup realizado.  Por defecto es el día
 * anterior para forzar la creación de un backup.
 *
 * cuenta: Cantidad de backups realizados.
 */
class Backup extends Realm.Object {}
Backup.schema = {
  name: 'Backup',
  properties: {
    primera_vez: { type: 'bool', default: true },
    ultimo: {
      type: 'date',
      default: m().startOf('day').add(-1, 'days').toDate()
    },
    cuenta: { type: 'int', default: 0 }
  }
}

/*
 * Las notas son textos escritos por las usuarias.  Están asociadas a
 * ciclos específicos.
 *
 * ciclo: El ciclo al que pertenecen.
 *
 * created_at: Fecha de creación.
 *
 * updated_at: Fecha de última modificación.
 *
 * texto: El texto escrito
 */
class Nota extends Realm.Object {
  /*
   * El día de la nota en el ciclo
   */
  get dia () {
    return m(this.created_at).diff(m(this.ciclo.me_vino), 'days') + 1
  }
}
Nota.schema = {
  name: 'Nota',
  primaryKey: 'id',
  properties: {
    id: 'int',
    ciclo: 'Ciclo',
    created_at: 'date',
    updated_at: 'date',
    texto: 'string'
  }
}

/*
 * El ciclo representa un ciclo menstrual completo.  Los hitos son
 * fecha de comienzo, fin (el día anterior al ciclo siguiente) y fecha
 * de fin de la menstruación.  En base al ciclo se calculan todas las
 * fases.
 */
export class Ciclo extends Realm.Object {
  /*
   * La duración en días estimada para la menstruación, según la
   * duración del ciclo
   */
  static duracion_menstrual_estimada_segun_duracion (duracion) {
    switch (duracion) {
      case 20: return 4;
      case 21: return 5;
      case 22: return 6;
      case 23: return 7;
    }

    return 8;
  }

  /*
   * Duración total del ciclo en días.
   */
  get duracion () {
    return m(this.fin_del_ciclo).diff(this.me_vino, 'days') + 1
  }

  /*
   * Días completos desde el comienzo del ciclo
   */
  get dia () {
    return m().diff(m(this.me_vino), 'days') + 1
  }

  /*
   * Calcula la fecha a partir de un día del ciclo.  Se resta uno para
   * contar días completos.
   */
  date_para_dia (dia = null) {
    return m(this.me_vino).add((dia || this.dia) - 1, 'days').toDate()
  }

  /*
   * Nombre estable y sin traducir para la fase según el id
   */
  etapa_segun_fase (fase_id) {
    switch (fase_id) {
      case 1: return 'menstrual'
      case 2: return 'pre_ovulatoria'
      case 3: return 'ovulatoria'
      case 4: return 'pre_menstrual'
    }
  }

  /*
   * Devuelve la fecha para la etapa según el id de la fase, porque si
   * el nombre de la etapa está traducido no podemos convertirlo en un
   * método del ciclo al estilo ciclo[etapa]
   *
   * Lo único que no cambia es la fase.id
  */
  fecha_para_etapa_segun_fase (fase_id) {
    return this[this.etapa_segun_fase(fase_id)]
  }

  /*
   * Devuelve la duración de la etapa según el id de la fase
   */
  duracion_para_etapa_segun_fase (fase_id) {
    return this[`duracion_${this.etapa_segun_fase(fase_id)}`]
  }

  /*
   * Fecha de inicio de la etapa menstrual
   */
  get menstrual () {
    return this.me_vino;
  }

  /*
   * La etapa pre-ovulatoria empieza inmediatamente después que se fue
   *
   * XXX: La etapa pre ovulatoria es opcional, qué valor deberíamos
   * devolver para que sea lo correcto?
   */
  get pre_ovulatoria () {
    return m(this.se_me_fue).add(1, 'days').toDate()
  }

  /*
   * La etapa ovulatoria comienza 16 días completos antes del fin de
   * ciclo.
   */
  get ovulatoria () {
    return m(this.fin_del_ciclo).add(-16, 'days').toDate()
  }

  /*
   * La etapa pre menstrual comienza 11 días completos antes del fin del
   * ciclo.
   */
  get pre_menstrual () {
    return m(this.fin_del_ciclo).add(-11, 'days').toDate()
  }

  /*
   * Duración de la menstruación en días completos.
   */
  get duracion_menstrual () {
    return m(this.se_me_fue).diff(this.menstrual, 'days') + 1;
  }

  /*
   * Días que dura la etapa pre ovulatoria
   *
   * En algunos ciclos no hay etapa pre ovulatoria no existe y la
   * duración va a dar negativo.  Corregimos a 0.
   */
  get duracion_pre_ovulatoria () {
    const d = m(this.ovulatoria).diff(this.pre_ovulatoria, 'days')

    if (d < 0) return 0

    return d + 1
  }

  /*
   * Días que dura la etapa ovulatoria
   */
  get duracion_ovulatoria () {
    return m(this.pre_menstrual).diff(this.ovulatoria, 'days')
  }

  /*
   * Días que dura la etapa pre menstrual
   */
  get duracion_pre_menstrual () {
    return m(this.fin_del_ciclo).diff(this.pre_menstrual, 'days')
  }

  /*
   * Traer la fase según la fecha actual, usamos el fin del día para
   * calcular el día entero
   */
  calcular_fase_segun_fecha (fecha) {
    return this.calcular_fase(m(fecha).startOf('day').diff(m(this.me_vino), 'days') + 1)
  }

  /*
   * Devolver la fecha de comienzo de fase según la id
   *
   * XXX: La etapa pre ovulatoria es opcional, qué valor deberíamos
   * devolver para que sea lo correcto?
   */
  comienzo_de_fase (fase_id) {
    switch (fase_id) {
      case 1: return this.me_vino
      case 2: return this.pre_ovulatoria
      case 3: return this.ovulatoria
      case 4: return this.pre_menstrual
    }
  }

  /*
   * Devolver la fecha de fin de fase según la id
   *
   * XXX: La etapa pre ovulatoria es opcional, qué valor deberíamos
   * devolver para que sea lo correcto?
   */
  fin_de_fase (fase_id) {
    switch (fase_id) {
      case 1: return this.se_me_fue
      case 2: return m(this.ovulatoria).add(-1, 'days').toDate()
      case 3: return m(this.pre_menstrual).add(-1, 'days').toDate()
      case 4: return this.fin_del_ciclo
    }
  }

  /*
   * El día del ciclo en que termina la etapa menstrual
   */
  get dia_fin_menstrual () {
    return this.duracion_menstrual
  }

  /*
   * El día del ciclo en que termina la etapa pre ovulatoria
   *
   * XXX: La etapa pre ovulatoria es opcional, qué valor deberíamos
   * devolver para que sea lo correcto?
   */
  get dia_fin_pre_ovulatoria () {
    return m(this.ovulatoria).diff(m(this.me_vino), 'days') + 1;
  }

  /*
   * El día del ciclo en que termina la etapa ovulatoria
   */
  get dia_fin_ovulatoria () {
    return m(this.pre_menstrual).diff(m(this.me_vino), 'days') + 1
  }

  /*
   * El día del ciclo en que termina la etapa pre menstrual
   */
  get dia_fin_pre_menstrual () {
    return this.duracion
  }

  /*
   * Obtener la fase para el día del ciclo
   */
  calcular_fase (dia = null) {
    // Usar el día actual por defecto
    if (dia == null) dia = this.dia

    switch (true) {
      // Si el día está dentro de la etapa menstrual
      case (dia <= this.duracion_menstrual): return 1
      // O dentro de la etapa pre ovulatoria, siempre y cuando haya una
      case (this.duracion_pre_ovulatoria > 0 && dia <= this.dia_fin_pre_ovulatoria): return 2
      // O dentro de la etapa ovulatoria
      case (dia <= this.dia_fin_ovulatoria): return 3
    }

    return 4
  }
}

/*
 * id: un identificador único
 *
 * me_vino: fecha en que inicia el ciclo
 *
 * se_me_fue: fecha de fin de menstruación, por defecto me_vino + 8 días
 *
 * fin_del_ciclo: fecha de fin del ciclo, por defecto me_vino +
 * ajustes.duracion_promedio, luego ciclo_siguiente.me_vino - 1 día
 *
 * activo: true si este es el ciclo activo
 *
 * se_me_fue_temporal: true si el ciclo está en una menstruación
 * promedio
 *
 * notas: notas asociadas
 *
 * encuestas: encuestas asociadas
 *
 * remote_id: id en el backend, si está vacío es que no fue sincronizado
 * aun (TODO: Convertir todos los remote_id en UUIDs, de forma que las
 * usuarias puedan reclamar su información y no tenemos
 *
 * fase: la fase a la que pertenece el ciclo (XXX: ???)
 *
 * created_at: fecha de creación
 *
 * updated_at: fecha de última modificación
 *
 * synced_at: fecha de última sincronización
 */
Ciclo.schema = {
  name: 'Ciclo',
  primaryKey: 'id',
  properties: {
    id: 'int',
    me_vino: 'date',
    se_me_fue: 'date',
    fin_del_ciclo: 'date',
    activo: 'bool',
    se_me_fue_temporal: 'bool',
    notas: { type: 'linkingObjects', objectType: 'Nota', property: 'ciclo' },
    encuestas: { type: 'linkingObjects', objectType: 'Encuesta', property: 'ciclo' },
    remote_id: { type: 'string', default: '' },
    fase: { type: 'linkingObjects', objectType: 'Fase', property: 'ciclos' },
    created_at: { type: 'date', default: new Date() },
    updated_at: { type: 'date', default: new Date() },
    synced_at: { type: 'date', default: new Date() }
  }
}

/*
 * Las notificaciones son los textos disponibles para programar según la
 * frecuencia especificada en Ajustes.frecuencia_notificaciones
 *
 * id: identificador de la notificación, como String para que coincida
 * con la id solicitada por PushNotification
 *
 * etapa: etapa a la que pertenece
 *
 * texto: el texto de la notificación
 *
 * en_uso: true si está programada
 *
 * run_at: hora a la que debería programarse, por defecto 12:00 (XXX)
 */
class Notificacion extends Realm.Object { }
Notificacion.schema = {
  name: 'Notificacion',
  primaryKey: 'id',
  properties: {
    id: 'string',
    etapa: 'string',
    texto: 'string',
    en_uso: 'bool',
    run_at: 'string'
  }
}

/*
 * Registro de estados
 *
 * id: identificador único
 *
 * ciclo: ciclo al que pertenece
 *
 * como_te_sentis: array de estados (TODO: Convertir a constantes)
 *
 * sintomas: relación de síntomas con encuesta (TODO: convertir a un
 * array para simplificar y constante del modelo)
 *
 * dolores: relación de dolores con encuesta (TODO: idem anterior)
 *
 * nivel_de_sangrado: [-1 0 1]
 * sangrado_durante_o_despues_del_sexo: boolean
 * duracion_del_sangrado: [-1 0 1]
 * intensidad_del_dolor: [-1 0 1]
 * color_del_flujo_vaginal: #ffffff
 * cantidad_del_flujo_vaginal: [-1 0 1]
 * olor_del_flujo_vaginal: boolean
 * textura_del_flujo: [seco pegajoso cremoso elastico]
 *
 * productos_utilizados: (TODO: Convertir a constantes)
 *
 * remote_id: id en el backend (XXX)
 *
 * created_at: fecha de creación
 * updated_at: fecha de última modificación
 * synced_at: fecha de última sincronización
 */
class Encuesta extends Realm.Object {}
Encuesta.schema = {
  name: 'Encuesta',
  primaryKey: 'id',
  properties: {
    id: 'int',
    ciclo: 'Ciclo',
    como_te_sentis: 'string[]',
    sintomas: { type: 'list', objectType: 'Sintoma' },
    dolores: { type: 'list', objectType: 'Dolor' },
    nivel_de_sangrado: { type: 'int', default: 0 },
    sangrado_durante_o_despues_del_sexo: { type: 'bool', default: false },
    duracion_del_sangrado: { type: 'int', default: 0 },
    intensidad_del_dolor: { type: 'int', default: 0 },
    color_del_flujo_vaginal: { type: 'string', default: '' },
    cantidad_del_flujo_vaginal: { type: 'int', default: 0 },
    olor_del_flujo_vaginal: { type: 'bool', default: false },
    textura_del_flujo: { type: 'string', default: '' },
    productos_utilizados: 'string[]',
    pais: { type: 'string', default: '' },
    marca_vacuna: { type: 'string', default: '' },
    dosis: { type: 'string', default: '' },
    remote_id: { type: 'string', default: '' },
    created_at: { type: 'date', default: new Date() },
    updated_at: { type: 'date', default: new Date() },
    synced_at: { type: 'date', default: new Date() }
  }
}

/*
 * Síntomas
 *
 * TODO: Eliminar y convertir en array
 */
class Sintoma extends Realm.Object {}
Sintoma.schema = {
  name: 'Sintoma',
  properties: {
    encuesta: {
      type: 'linkingObjects',
      objectType: 'Encuesta',
      property: 'sintomas'
    },
    nombre: 'string'
  }
}

/*
 * Dolores
 *
 * TODO: Eliminar y convertir en array
 */
class Dolor extends Realm.Object {}
Dolor.schema = {
  name: 'Dolor',
  properties: {
    encuesta: {
      type: 'linkingObjects',
      objectType: 'Encuesta',
      property: 'dolores'
    },
    parte_del_cuerpo: 'string'
  }
}

/*
 * Registra el consentimiento de la usuaria
 *
 * TODO: Unificar en Usuaria ya que estamos registrando un solo
 * consentimiento.
 *
 * id: identificador
 * fecha: fecha en que se dio el consentimiento
 * estado: si se lo aceptó o rechazó
 * remote_id: sincronizacion (XXX: esto no se está sincronizando)
 */
class Consentimiento extends Realm.Object {}
Consentimiento.schema = {
  name: 'Consentimiento',
  primaryKey: 'id',
  properties: {
    id: 'int',
    fecha: 'date',
    estado: 'bool',
    remote_id: { type: 'string', default: '' }
  }
}

/*
 * Registro de Usuaria
 *
 * Es un modelo único donde registramos todos los cambios.  Siempre
 * tenemos que traer la última disponible, porque en algunos casos se
 * han creado dos y estábamos trabajando indistintamente.
 *
 * id: identificador
 * remote_id: identificador remoto (XXX)
 * email: dirección de correo que usó para el registro
 * registrada: fecha de registro
 * password: contraseña para la API
 * password_para_backup: contraseña para cifrar el backup
 */
class Usuaria extends Realm.Object {}
Usuaria.schema = {
  name: 'Usuaria',
  primaryKey: 'id',
  properties: {
    id: 'int',
    remote_id: { type: 'string', default: '' },
    email: { type: 'string', default: '' },
    registrada: { type: 'date', default: new Date() },
    password: { type: 'string', default: '' },
    password_para_backup: { type: 'string', default: '' }
  }
}

/*
 * Fases sincronizadas con la API, salvo el id suelen estar traducidas
 * al idioma local.
 *
 * Las fases son siempre cuatro y son los segmentos por los que pasa el
 * ciclo.
 *
 * id: el identificador de la Fase.  Los identificadores se asumen de 1
 * a 4 en el orden en que suceden dentro del ciclo, de forma que podamos
 * cruzar información.  Ver Ciclo.
 *
 * nombre: Nombre de la fase
 * etapa: Nombre de la etapa
 * color: El color
 * luna: La luna
 * contenido: Texto introductorio a la fase
 * aspectos: Lista de aspectos
 * ciclos: Ciclos dentro de esta fase (XXX: ???)
 */
class Fase extends Realm.Object {}
Fase.schema = {
  name: 'Fase',
  primaryKey: 'id',
  properties: {
    id: 'int',
    nombre: 'string',
    etapa: 'string',
    color: 'string',
    luna: 'string',
    contenido: 'string',
    aspectos: { type: 'list', objectType: 'Aspecto' },
    ciclos: { type: 'list', objectType: 'Ciclo' },
    imagen: 'string'
  }
}

/*
 * Los aspectos son los cuatro elementos que se relacionan con las
 * cuatro fases, en total 16.
 *
 * Los textos guardados en este modelo están traducidos al idioma de la
 * app.
 *
 * id: identificador, coincide con el id en la API
 * fase: Fase
 * elemento: Nombre del elemento, traducido
 * sub_titulo: Traducido
 * slides: Slides
 */
class Aspecto extends Realm.Object {}
Aspecto.schema = {
  name: 'Aspecto',
  primaryKey: 'id',
  properties: {
    id: 'int',
    fase: { type: 'linkingObjects',
      objectType: 'Fase',
      property: 'aspectos' },
    elemento: 'string',
    sub_titulo: 'string',
    slides: { type: 'list', objectType: 'Slide' }
  }
}

/*
 * Cada Aspecto tiene una cantidad indeterminada de Slides que se
 * sincronizan con la API
 *
 * Las Slides también están traducidas y no se ordenan por fecha sino
 * por número de orden.
 *
 * id: identificador
 * aspecto: Aspecto
 * contenido: Texto traducido
 * order: Posición en el aspecto
 * imagen: URL de una imagen
 */
class Slide extends Realm.Object {}
Slide.schema = {
  name: 'Slide',
  primaryKey: 'id',
  properties: {
    id: 'int',
    aspecto: { type: 'linkingObjects',
      objectType: 'Aspecto',
      property: 'slides' },
    contenido: 'string',
    order: 'int',
    imagen: 'string'
  }
}

/*
 * Sitios recomendados, aparecen en el cajón.
 */
class Recomendado extends Realm.Object {}
Recomendado.schema = {
  name: 'Recomendado',
  primaryKey: 'id',
  properties: {
    id: 'string',
    title: 'string',
    url: 'string',
    favicon_url: 'string',
    until: 'int'
  }
}

/*
 * Aquí se incorporan las migraciones.  Si se hace un cambio en un
 * modelo o se agrega uno nuevo, hay que incrementar schemaVersion.  Si
 * se agrega un modelo también hay que agregarlo en schema.
 *
 * Si una migración modifica datos, agregar un paso en migration que
 * modifica los datos de la versión anterior a la actual.  Usamos un
 * switch para que usuarias que están actualizando luego de varias
 * versiones puedan aplicar todas las migraciones una tras otra.
 */
export default new Realm({
  schema: [Ajustes, Nota, Ciclo, Notificacion, Encuesta, Sintoma, Dolor, Consentimiento, Usuaria, Backup, Fase, Aspecto, Slide, Recomendado],
  schemaVersion: 81,
  migration: function (oldRealm, newRealm) {
    switch (oldRealm.schemaVersion) {
      case 16:
        let notas_sin_ciclo = newRealm.objects('Nota').filtered('ciclo == $0', null)

        for (let i = 0; i < notas_sin_ciclo.length; i++) {
          notas_sin_ciclo[i].ciclo = newRealm.objects('Ciclo')[0]
        }
      case 42:
        newRealm.objects('Encuesta').forEach(encuesta => {
          encuesta.sangrado_durante_o_despues_del_sexo = oldRealm.objectForPrimaryKey('Encuesta', encuesta.id).sangrado_antes_o_despues_del_sexo
        })
      case 50:
        // Eliminar las notificaciones en el formato anterior de id con
        // SHA1
        newRealm.delete(newRealm.objects('Notificacion'))
      case 69:
        newRealm.objects('Encuesta').forEach(encuesta => {
          // Actualizar el color de las encuestas
          switch (encuesta.color_del_flujo_vaginal) {
            case '#cce97b':
              encuesta.color_del_flujo_vaginal = '#dcf18f'
              break
            case '#eb667a':
              encuesta.color_del_flujo_vaginal = '#f19f96'
              break
            default:
          }

          // Convertir la sensación en un array
          encuesta.como_te_sentis = [ oldRealm.objectForPrimaryKey('Encuesta', encuesta.id).como_te_sentis ]
        })
      // Cambiar el backup a primera_vez: true
      case 72:
        newRealm.objects('Backup').forEach(backup => {
          backup.primera_vez = true
        })
      // Setear el valor de ciclo.synced_at para forzar la actualización
      // del backend
      case 74:
        newRealm.objects('Ciclo').forEach(ciclo => {
          ciclo.synced_at = m('2019-01-01 00:00:00').toDate()
        })
      // Corregir el cierre del ciclo a la duración máxima
      case 75:
        try {
          const ciclos = newRealm.objects('Ciclo').sorted('id')

          ciclos.forEach((ciclo, i) => {
            if (ciclo.activo) return
            if (ciclo.duracion >= (35 + MENSTRUACION_PROMEDIO)) return

            const fin_del_ciclo = m(ciclos[i+1].me_vino).add(-1, 'days').toDate()

            Logger.log('Ciclos', 'Corrigiendo')
            Logger.log('Ciclos', ciclo.fin_del_ciclo.toString())
            Logger.log('Ciclos', fin_del_ciclo.toString())

            ciclo.fin_del_ciclo = fin_del_ciclo
            ciclo.updated_at = new Date
          })
        } catch(e) {
          Sentry.captureException(e)
        }
      default:
    }
  } })
