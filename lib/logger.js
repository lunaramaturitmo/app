import { TEST, API } from './const'

import FileSystem from 'react-native-fs'
import m from 'moment'

const LOGFILE = FileSystem.DocumentDirectoryPath + '/lunar.log'

/*
 * Los atributos de los objetos que no queremos filtrar, como el
 * contenido de las notas.  También atributos recursivos, como las notas
 * o los ciclos.
 */
const ANONYMIZED_KEYS = [ 'notas', 'ciclo', 'texto', 'data', 'encuestas',
  'password', 'password_para_backup', 'fase', 'aspectos', 'ciclos',
  'bigText', 'ticker', 'usuaria' ]

/*
 * Lleva un registro en un archivo y lo envía al servidor
 */
export class Logger {
  static async log (view, message) {
    const date = m().format('YYYY-MM-DD HH:mm:ss')

    // quitar información importante, clonando el objeto para no
    // eliminarle datos más adelante
    const obj = {}

    if (typeof message === 'object') {
      Object.keys(message).forEach(key => {
        if (ANONYMIZED_KEYS.includes(key)) return

        obj[key] = message[key]
      })

      message = obj
    }

    if (TEST) console.log(view, message)

    await FileSystem.appendFile(LOGFILE,
      `${date}\t[${view}]\t${JSON.stringify(message)}\n`)
  }

  /*
   * Eliminar el archivo
   */
  static async delete () {
    return await FileSystem.unlink(LOGFILE)
  }

  /*
   * Elimina el archivo si supera una cantidad en bytes
   */
  static async delete_if_too_big () {
    const exists = await FileSystem.exists(LOGFILE)

    if (!exists) return

    const stat = await FileSystem.stat(LOGFILE)

    // 1MB
    if (parseInt(stat.size) > (1024 * 1024)) Logger.delete()
  }

  /*
   * Enviarlo al backend
   */
  static async send (titulo = 'Lunar.log') {
    Logger.log('Logger', `Enviando log ${LOGFILE}`)

    const content = await FileSystem.readFile(LOGFILE)
    return await fetch(`${API}/v1/reportes`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Host: 'api.lunarcomunidad.com'
        },
        body: JSON.stringify({ reporte: { log: content, tipo: titulo } })
      })
      .then(r => r.json())
      .then(r => {
        Logger.log('Logger.enviar_reporte', `Guardado ${r.data.id}`)
        return true
      })
      .catch(error => {
        Logger.log('Logger.enviar_reporte', error)
        return false
      })
  }
}
