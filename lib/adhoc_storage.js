import { AsyncStorage } from 'react-native'
import { Logger } from './logger'

/*
 * Realiza una acción solo una vez
 */
export class AdhocStorage {
  static getset(key, callback) {
    AsyncStorage.getItem(key).then(x => {
      if (x !== null) return

      callback()

      AsyncStorage.setItem(key, (new Date).toString())
                  .then(x => Logger.log('AdhocStorage.success', x))
                  .catch(e => Logger.log('AdhocStorage.set_error', e))
    }).catch(e => {
      Logger.log('AdhocStorage.error', e)
    })
  }
}
