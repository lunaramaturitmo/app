# Lanzar una versión

Cambiar la versión en `package.json`:

```json
  "version": "1.3.5", // <= Cambiar aquí
```

## Android

Es necesario tener una copia de las llaves privadas para poder firmar la
aplicación y que las usuarias puedan actualizarla sin tener que
desinstalar y volver a instalar con otra firma.

Correr el compilador y esperar:

```bash
make all
```

Ingresar a la [consola de desarrolladora de
Android](https://play.google.com/apps/publish/).

Generar una versión nueva en Lunar > Gestión de versiones > Versiones de
la aplicación.

En Segmento abierto > Editar la versión, subir los archivos creados en
`android/app/build/outputs/apk/release/`:

```
app-armeabi-v7a-release.apk # Para dispositivos ARM
app-x86-release.apk # Para dispositivos Intel/AMD
```

La compilación está separada en dos partes para que el código específico
de cada procesador no ocupe espacio innecesario en el dispositivo.  La
app es más liviana de descargar e instalar.  También hay una versión
"universal" para ambas arquitecturas llamada
`app-universal-release.apk`.

Luego de subir los archivos, agregar las notas de la versión y utilizar
los botones Revisar e Iniciar lanzamiento Beta

## iOS

Asegurarse de instalar los parches:

```bash
make patches
```

En XCode, cambiar Product > Scheme > Edit scheme y cambiar Lunar a
`Release` en `Build configuration`.

Volver a Product y utilizar la opción `Archive`.  Al finalizar el
proceso XCode ofrece subir los archivos .ipa generados a la App Store.
