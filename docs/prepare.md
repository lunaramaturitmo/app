# Preparar el sistema para trabajar con Lunar

Lunar es desarrollada en GNU/Linux y MacOS.

## Preparar el entorno Android

Instalar `gradle`

```bash
brew install gradle # Mac
sudo pacman -Sy gradle # Archlinux
sudo apt install gradle # Debian/Ubuntu
```

Instalar [Android Studio](https://developer.android.com/studio/) ([paso
a paso](https://developer.android.com/studio/install).  También es
posible instalar el SDK y las plataformas específicas manualmente,
dependiendo de la distribución.

Desde el gestor de Android Studio, instalar la plataforma versión 29 y
la SDK versión 29.0.2.  Es posible que algunas dependencias soliciten
28.0.3 también.


## Preparar el entorno iOS

Es necesario contar con una computadora Mac en versión Mojave, un
dispositivo iPhone/iPad y una cuenta de Apple.

Instalar XCode 11.6 o superior desde la App Store.  También se [pueden
descargar versiones
específicas](https://developer.apple.com/download/more/).

Instalar [Homebrew](https://brew.sh) para gestionar paquetes de software
en Mac.

Dentro de XCode, en Preferencias > Cuentas, agregar la Apple ID.  Sin
esto aun podemos correr la app en el simulador pero no compilar la app
para publicarla en la App Store.

Es necesario registrar un dispositivo iPhone, si no lo tenemos a mano se
puede averiguar el [UDID](https://udid.io) y cargarlo en
<https://developer.apple.com>.


## Descargar el código

Instalar [NVM](https://github.com/creationix/nvm#readme) para gestionar
la versión de Node

```bash
# Seguir los pasos de instalación del sitio de NVM o instalarlo usando
# el gestor de paquetes de la distribución

brew install nvm # Mac
# Con GNU/Linux, seguir los pasos de instalación desde el sitio de NVM
...
# Seguir los pasos para incorporar nvm al ~/.bash_profile sugeridos por
# la instalación

```

* Instalar Yarn para gestionar las dependencias de JavaScript

```bash
# Alguna de estas opciones según el sistema operativo y distribución
brew install yarn # Mac
sudo pacman -Sy yarn # Archlinux/Parabola/etc
sudo apt install yarn # Debian/Ubuntu/etc
```

* Instalar `git` si no estaba ya instalado

```bash
# Igual que con yarn
```

* Obtener el código fuente clonando el repositorio con `git`

```bash
# Clonar
git clone git@0xacab.org:lunaramaturitmo/app.git
# Ingresar al directorio donde está el código, eso es necesario cada vez
# que vayamos a trabajar con él
cd app
# Actualizar el código si hubo cambios
git pull
# Enviar nuestros cambios
git push
```

* Instalar la versión de `node` solicitada por el repositorio:

```bash
nvm use # Activar la versión necesaria
nvm install v12.16.1 # Instalar la versión necesaria (puede cambiar)
```

* Correr `yarn install` para instalar las dependencias de JavaScript

  **NOTA:** Es posible usar `npm` pero nos ha dado problemas en el
  pasado, preferimos `yarn`.

```bash
yarn
```

* Instalar los parches con modificaciones a las dependencias (necesario
  siempre luego de correr `yarn`)

```bash
make patches -B
```

## Correr en el emulador o dispositivo

### Android

Es necesario [conectar un
dispositivo](https://facebook.github.io/react-native/docs/running-on-device)
con opciones de desarrollo habilitadas o desde Android Studio instalar
una simulador de Android (en nuestra experiencia pesado y lento).

(Siempre dentro del directorio `app/`)

```bash
./node_modules/.bin/react-native run-android
make dev
```

### iOS

Desde XCode, cambiar el objetivo de la compilación a la versión de
iPhone que queramos (arriba a la izquierda, al lado de un botón de
play).

Correr la simulación desde XCode utilizando el botón de play arriba a la
izquierda o manualmente:

```bash
./node_modules/.bin/react-native run-ios
```
