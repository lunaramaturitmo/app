# Cifrado

El cifrado se realiza de la siguiente forma:

* Se genera una contraseña aleatoria de 8 caracteres alfanuméricos en
  minúscula.

* La contraseña se hashea con PBKDF2 usando una sal de 8 caracteres
  aleatorios alfanuméricos en minúscula.  Esto permite que la llave de
  cifrado y otra información necesaria se genere a partir de una
  contraseña recordable, pero sea "caro" en términos de recursos,
  generar todas las posibles combinaciones.

* El hash PBKDF2 se hashea a SHA512 para obtener los 512b necesarios
  para la llave y otros componentes del cifrado.

* Los primeros 256 bits del hash SHA512 se utilizan como llave simétrica
  AES.

* Los siguientes 128 bits como valor de inicialización (IV)

* Los bits restantes se utilizan como datos de autenticación (AD), de
  forma que el backup quede invalidado si fue manipulado o mal
  descargado.

* Los valores de la base de datos se convierten a una string JSON y se
  cifran con AES-GCM-256 utilizando los atributos generados a partir de
  la contraseña.

* El texto cifrado junto con la sal se envían al servidor.  La sal es
  información pública y permite recuperar el hash PBKDF2.

* El servidor nunca recibe la contraseña.  Las usuarias deben tomar nota
  de la contraseña por su cuenta.
