SHELL := /bin/bash
.DEFAULT_GOAL := help

# Copiar el archivo de configuración y avisar cuando hay que
# actualizarlo.
.env: .env.example
	@test -f $@ || cp -v $< $@
	@test -f $@ && echo "Revisa $@ para actualizarlo con respecto a $<"
	@test -f $@ && diff -auN --color $@ $<

# La ubicación del APK una vez compilado.  Debería haber
apk_release := android/app/build/outputs/apk/release/app-armeabi-v7a-release.apk

version ?= $(shell jq -r .version < package.json)
major ?= $(shell echo $(version) | cut -d . -f 1,2)
sed := $(shell which gsed 2>/dev/null || which sed 2>/dev/null)
envsubst := $(shell which genvsubst 2>/dev/null || which envsubst 2>/dev/null)
env ?= development
env_file := .env

ifeq ($(env),production)
env_file := .env.production
endif

development_platform := $(shell uname -s)
ifeq ($(development_platform),Darwin)
osx := true
endif
ifeq ($(development_platform),Linux)
linux := true
endif

include $(env_file)

export

# Es necesario que estén acá arriba para poder generar los locales
locales_yaml := $(wildcard locales/*.yml)
locales_json := $(patsubst locales/%.yml,assets/%.json,$(locales_yaml))
updates_url  := https://panel.lunarcomunidad.com/updates/

help: always ## Ayuda
	@echo -e "Lunar\n"
	@echo -e "Uso: make TAREA\n"
	@echo -e "Tareas:\n"
	@grep -E "^[a-z\-]+:.*##" Makefile | sed -re "s/(.*):.*##(.*)/\1;\2/" | column -s ";" -t

# Algunos paquetes que utilizamos necesitan parches.
patches: sjcl check-box ## Aplicar parches

# Al editar los idiomas en locales/, correr esta tarea para actualizar
# los archivos JSON.
locales: $(locales_json) ## Generar los archivos de idiomas

# Para subir el número de versión solo es necesario editarlo en el
# archivo package.json.  Luego correr esta tarea para asociar la versión
# a todas las plataformas.
bump-version: ## Configurar el número de versión en Android e iOS a partir de package.json
	$(sed) -re 's/^(\s+versionCode) .*/\1 $(subst .,,$(version))/' -i android/app/build.gradle
	$(sed) -re 's/^(\s+versionName) ".*"/\1 "$(version)"/' -i android/app/build.gradle
	$(sed) -re 's/(android:versionCode)=".*"/\1="$(subst .,,$(version))"/' -i android/app/src/main/AndroidManifest.xml
	$(sed) -re 's/(android:versionName)=".*"/\1="$(version)"/' -i android/app/src/main/AndroidManifest.xml
	$(sed) -re 's,(<key>CFBundleShortVersionString</key><string)>.*(</string>),\1>$(version)\2,' -i ios/Lunar/Info.plist
	$(sed) -re 's,$(updates_url)[^/]+/android-index.json,$(updates_url)$(major)/android-index.json,' -i android/app/src/main/AndroidManifest.xml
	$(sed) -re 's,$(updates_url)[^/]+/ios-index.json,$(updates_url)$(major)/ios-index.json,' -i ios/Lunar/Supporting/Expo.plist

android-clean: ## Eliminar archivos temporales de Android
	rm -rf android/app/build

android-dev: patches development android-secrets ## Configurar e iniciar el entorno de desarrollo
	npm start

android-icons: $(icons) ## Genera los íconos para Android

android-log: ## Mostrar el log de Android usando logcat
	adb logcat | grep --line-buffered -E 'MessageQueue|React' # | ccze -A

android-release: bump-version android-secrets production yarn patches $(apk_release) ## Compila la versión de lanzamiento
	@echo -e "\a"

android-run: ## Iniciar Android en emulador o dispositivo
	$(sed) -re 's/android:allowBackup="false"/android:allowBackup="true"/' -i android/app/src/main/AndroidManifest.xml
	./node_modules/.bin/react-native run-android

android-secrets: $(android_secrets) ## Verifica que tengamos todos los secretos para Android

android-shake: ## Sacudir el dispositivo para abrir el menú de React Native
	adb shell input keyevent 82

# Dependiendo del entorno de trabajo, instalar Yarn automáticamente o
# recomendarlo.
yarn: ## Instalar las dependencias de React Native
ifdef osx
	which yarn || brew install yarn
endif
ifdef linux
	@which yarn >/dev/null || echo "No te olvides de instalar Yarn en tu distribución"
endif
	yarn

development: ## Cambiar a modo desarrollo
	$(MAKE) lib/const.js env=development

production: ## Cambiar a modo producción
	$(MAKE) lib/const.js env=production

pod-install: Gemfile.lock ./vendor ## Instalar CocoaPods
	cd ios && bundle exec pod install

ios-match: ./vendor ## Instalar los certificados de AppStore
	bundle exec fastlane match appstore

ios-prepare: ./vendor pod-install ios-secrets ios-match ## Preparar el entorno de desarrollo en iOS
	which gsed || brew install gnu-sed
	which jq || brew install jq 

ios-release: production yarn patches ios-secrets ## Compilar versión de lanzamiento para iOS
	bundle exec fastlane ios release ; echo -e "\a"

ios-run: development ## Iniciar iOS en emulador o dispositivo
	./node_modules/.bin/react-native run-ios

ios-secrets: $(ios_secrets) ## Verifica que tengamos todos los secretos para iOS

ota-release: dist/$(major)/android-index.json dist/$(major)/ios-index.json ## Compilar las actualizaciones automáticas

# Sincronizar primero los archivos grandes y a lo último eliminar las
# actualizaciones.
ota-sync: ## Subir las actualizaciones automáticas al servidor
	ssh root@panel.lunarcomunidad.com mkdir -p /srv/deploy/srv/http/panel.lunarcomunidad.com/public/updates/$(major)/assets/ /srv/deploy/srv/http/panel.lunarcomunidad.com/public/updates/$(major)/bundles/
	rsync -av --chown=1000:82 dist/$(major)/assets/ root@panel.lunarcomunidad.com:/srv/deploy/srv/http/panel.lunarcomunidad.com/public/updates/$(major)/assets/
	rsync -av --chown=1000:82 dist/$(major)/bundles/ root@panel.lunarcomunidad.com:/srv/deploy/srv/http/panel.lunarcomunidad.com/public/updates/$(major)/bundles/
	rsync -av --chown=1000:82 --delete-after dist/$(major)/ root@panel.lunarcomunidad.com:/srv/deploy/srv/http/panel.lunarcomunidad.com/public/updates/$(major)/

fastlane: ./vendor

sjcl: ## Compilar cifrado con los módulos que necesitamos
	cd node_modules/sjcl && ./configure \
		--with-aes --with-sha512 --with-gcm --with-pbkdf2 --compress=none
	cd node_modules/sjcl && make
	# Remover crypto.randomBytes
	patch -Np 0 -r - -i sjcl.patch

check-box: ## Aplicar los parches para react-native-checkbox
	patch -Np 0 -r - -i check-box.patch || :

# Compilar la APK para lanzamiento solo si cambia alguno de los archivos
# del código fuente.
sources := $(wildcard *.js lib/*.js lib/*/*.js assets/*.json assets/*.png)
sources += android/app/build.gradle package.json
sources := $(filter-out lib/const.js,$(sources))
$(apk_release): $(sources)
	$(sed) -re 's/android:allowBackup="true"/android:allowBackup="false"/' -i android/app/src/main/AndroidManifest.xml
	cd android ; ./gradlew assembleRelease --stacktrace
	ls -halSr $(dir $@)

# Genera íconos para todas las versiones requeridas por Android.
resolutions := hdpi mdpi xhdpi xxhdpi
hdpi   := 72
mdpi   := 48
xhdpi  := 96
xxhdpi := 144
android_icon_dir := android/app/src/main/res/mipmap-
icons := $(patsubst %,$(android_icon_dir)%/ic_launcher.png,$(resolutions))
$(icons): $(android_icon_dir)%/ic_launcher.png: logo.svg
	convert -strip -background none -resize $($*)x$($*) $< $@

# Generar archivos en base a plantillas simples
%: %.in
	@echo "Configurando $@ en entorno $(env)"
	$(envsubst) < $< > $@
	touch $<

# Convertir los archivos de idiomas en YAML a JSON.
assets/%.json: locales/%.yml
	ruby -r yaml -r json -e "puts JSON.dump(YAML.safe_load(File.read('$<')))" > $@

# Instalar las dependencias de Ruby cuando cambia el archivo
Gemfile.lock ./vendor: Gemfile 
	which bundler && gem install bundler
	bundle install --path=./vendor

# Estos archivos no se comparten en el repositorio, sino manualmente por
# otrxs desarrolladorxs.
ios_secrets := ios/sentry.properties ios/Lunar/GoogleService-Info.plist
android_secrets := android/app/la-inventoria.keystore android/app/google-services.json android/sentry.properties
$(ios_secrets) $(android_secrets): always
	@test -f "$@" || echo "Pedile el archivo $@ a otre desarrolladore de Lunar"
	@test -f "$@"

# Compilar las actualizaciones automáticas con expo-updates y
# pre-compilarlas.
dist/$(major)/android-index.json dist/$(major)/ios-index.json: $(sources)
	mkdir -p $(dir $@)
	find $(dir $@) -type f -name "*.br" -or -name "*.gz" -print0 | xargs -r0 rm
	./node_modules/.bin/expo export --output-dir $(dir $@) --dump-sourcemap -p https://panel.lunarcomunidad.com/updates/$(major) --force
	find $(dir $@) -type f | while read file ; do gzip -9k "$$file" ; brotli -9k "$$file"; done

snapshots/%.ab:
	install -d $(dir $@)
	adb backup -f $@ -apk com.comunidadlunar

# Las tareas que tengan que correr siempre dependen de esta.
always:
.PHONY: phony
