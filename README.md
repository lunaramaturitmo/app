# Lunar

## Documentación

Ver el directorio `docs/` dentro de este repositorio.


Lunar se libera bajo la [licencia
Atribución/Reconocimiento-NoComercial-SinDerivados 4.0
Internacional](LICENSE.md)
